#include "restoreversion_command.h"

void RestoreVersionCommand::undo() {
	if(NotesManager::getInstance().isActive(prev->getState()->getId()))
		NotesManager::getInstance().restoreVersion(prev);
}

void RestoreVersionCommand::redo() {
	if(NotesManager::getInstance().isActive(prev->getState()->getId()))
		NotesManager::getInstance().restoreVersion(toRestore);
}
