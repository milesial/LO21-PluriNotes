#include "tasklist_widget.h"
#include "ui_tasklist_widget.h"
#include "notes_manager.h"

TaskListWidget::TaskListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskListWidget)
{
	ui->setupUi(this);
	
	update();
	
	connect(ui->treeWidget, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(itemPressed(QTreeWidgetItem*)));
	connect(ui->checkFinished, SIGNAL(toggled(bool)), this, SLOT(update()));
	connect(ui->checkProgress, SIGNAL(toggled(bool)), this, SLOT(update()));
	connect(ui->checkWaiting, SIGNAL(toggled(bool)), this, SLOT(update()));
}

TaskListWidget::~TaskListWidget()
{
	delete ui;
}

void TaskListWidget::update() {
	const QString dateFormat = "dd'/'MM'/'yy";
	QLocale locale  = QLocale(QLocale::English, QLocale::UnitedStates);
	ui->treeWidget->clear();
	NotesManager& nm = NotesManager::getInstance();
	Task *t;
	for(auto it = nm.beginActive(); it != nm.endActive(); it++) {
		t = dynamic_cast<Task*>(*it);
		if(t) {
			if(t->getState() == Task::FINISHED && !ui->checkFinished->isChecked())
				continue;
			if(t->getState() == Task::WAITING && !ui->checkWaiting->isChecked())
				continue;
			if(t->getState() == Task::IN_PROGRESS && !ui->checkProgress->isChecked())
				continue;
			
			
			
			
			QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
			item->setText(0, QString::fromStdString(t->getTitle()));
			if(t->getPriority() != 0) {
				item->setText(1, QString::number(t->getPriority()));
				item->setTextAlignment(1, Qt::AlignCenter);
				
			}
			if(t->getDeadline() != 0) {
				QDateTime date = QDateTime::fromTime_t(t->getDeadline());
				item->setText(2, locale.toString(date, dateFormat));
				item->setTextAlignment(2, Qt::AlignRight);
			}
			item->setData(0, Qt::UserRole, QString::fromStdString(t->getId()));
			QColor c;
			if(t->getState() == Task::WAITING) {
				c = QColor::fromRgb(255, 187, 92);
			} else if(t->getState() == Task::FINISHED) {
				c = QColor::fromRgb(148, 255, 140);
			} else {
				c = QColor::fromRgb(162, 218, 255);
			}
			//ui->treeWidget->setStyleSheet("selection-background-color: rgba(0,0,0,0);");
			item->setBackgroundColor(0, c);
			item->setBackgroundColor(1, c);
			item->setBackgroundColor(2, c);
		}
	}
}


void TaskListWidget::itemPressed(QTreeWidgetItem* i) {
	ui->treeWidget->clearFocus();
	emit taskClicked(i->data(0, Qt::UserRole).toString());
}
