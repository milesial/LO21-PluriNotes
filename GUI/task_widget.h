#ifndef TASK_WIDGET_H
#define TASK_WIDGET_H

#include <QWidget>
#include <QDateTime>
#include "note_widget.h"


namespace Ui {
class TaskWidget;
}

class Task;
/*!
 * \brief La classe TaskWidget est un QWidget qui affiche une TaskNote.
 */
class TaskWidget : public NoteWidget
{
	Q_OBJECT
protected:
	/*!
	 * \brief Met à jour les champs pour afficher la Task.
	 * \param task La tache à afficher.
	 */
	void setupFields(const Task *task);
	
public:
	/*!
	 * \brief Construit un TaskWidget qui affiche une Task avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de la Task associé.
	 * \param task La Task à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit TaskWidget(Task *task, QWidget *parent = 0);
	/*!
	 * \brief Construit un TaskWidget qui affiche une TaskN en mode lecture seule.
	 * \param task La Task à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit TaskWidget(const Task *task, QWidget *parent = 0);
	~TaskWidget();

private:
	Ui::TaskWidget *ui;

private slots:
	void toggleTaskPriority(bool b);
	void setTaskPriority(int p);
	void toggleTaskDeadline(bool b);
	void setTaskDeadline(QDateTime s);
	void setTaskAction();
	void setTaskState(QString s);

};
#endif // TASK_WIDGET_H
