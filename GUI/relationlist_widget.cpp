#include "relationlist_widget.h"
#include "ui_relationlist_widget.h"
#include "noteslist_widget.h"
RelationListWidget::RelationListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RelationListWidget)
{
    ui->setupUi(this);
    connect(ui->treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(itemPressed(QTreeWidgetItem*)));
}

RelationListWidget::~RelationListWidget()
{
    delete ui;
}

QTreeWidgetItem* RelationListWidget::addRoot(std::string name){
    QString qname = QString::fromStdString(name);
    QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidget);
    itm->setText(0,qname);
    ui->treeWidget->addTopLevelItem(itm);
    return itm;
}

QTreeWidgetItem* RelationListWidget::addChild(QTreeWidgetItem *parent, std::string id){
    QString qid = QString::fromStdString(id);
    QTreeWidgetItem *itm = new QTreeWidgetItem();
    itm->setText(0,qid);
    parent->addChild(itm);
    return itm;
}

void RelationListWidget::update(QString id){
    std::string ids = id.toStdString();
    NotesManager& nm = NotesManager::getInstance();
    RelationsManager& rm = nm.getRelationsManager();
    std::vector<Relation*> rel = rm.getRelations();
    Relation* ref = rm.getReference();
    ui->treeWidget->clear();
    ui->treeWidget->setHeaderLabel("Note's relations");
    QTreeWidgetItem *itm = addRoot("Ascending");
    QTreeWidgetItem *itm2;
    std::vector<std::string> pere;
    pere.push_back(ids);
    std::vector<Couple*> tmp;
    for(unsigned int i=0; i<rel.size(); i++){
        tmp = rel[i]->getAscendants(ids);
        if(tmp.size() != 0){
            itm2 = addChild(itm, rel[i]->getTitle());
            itm2->setBackgroundColor(0, Qt::cyan);
            if(rel[i]->isOriented()){
                for(unsigned int j=0; j<tmp.size();j++)
                    affichAcs(pere, itm2, tmp[j]->getNoteX(), rel[i]);
            }
            else {
                for(unsigned int j=0; j<tmp.size();j++){
                    if(tmp[j]->getNoteX() == ids)
                        affichAcs(pere, itm2, tmp[j]->getNoteY(), rel[i]);
                    else
                        affichAcs(pere, itm2, tmp[j]->getNoteX(), rel[i]);
                }
            }
        }
    }
    tmp = ref->getAscendants(ids);
    if(tmp.size() != 0){
        itm2 = addChild(itm, ref->getTitle());
        for(unsigned int j=0; j<tmp.size();j++)
            affichAcs(pere, itm2, tmp[j]->getNoteX(), ref);
    }

    itm = addRoot("Descending");
    for(unsigned int i=0; i<rel.size(); i++){
        tmp = rel[i]->getDescendants(ids);
        if(tmp.size() != 0){
            itm2 = addChild(itm, rel[i]->getTitle());
            itm2->setBackgroundColor(0, Qt::cyan);
            if(rel[i]->isOriented()){
                for(unsigned int j=0; j<tmp.size();j++)
                    affichDesc(pere, itm2, tmp[j]->getNoteY(), rel[i]);
            }
            else {
                for(unsigned int j=0; j<tmp.size();j++){
                    if(tmp[j]->getNoteX() == ids)
                        affichDesc(pere, itm2, tmp[j]->getNoteY(), rel[i]);
                    else
                        affichDesc(pere, itm2, tmp[j]->getNoteX(), rel[i]);
                }
            }
        }
    }
    tmp = ref->getDescendants(ids);
    if(tmp.size() != 0){
        itm2 = addChild(itm, ref->getTitle());
        for(unsigned int j=0; j<tmp.size();j++)
            affichDesc(pere, itm2, tmp[j]->getNoteY(), ref);
    }
}

QTreeWidget *RelationListWidget::getTree() const {
    return ui->treeWidget;
}

void RelationListWidget::clear() const {
    ui->treeWidget->clear();
}

void RelationListWidget::affichDesc(std::vector<std::string> pere, QTreeWidgetItem* ici, std::string aAffich, Relation* rel){
    for(unsigned int i=0; i<pere.size();i++){
        if(pere[i] == aAffich){ //cycle
            addChild(ici, aAffich);
            return;
        }
    }
    std::vector<Couple*> fils = rel->getDescendants(aAffich);
    if(fils.size()==0){ //plus de fils
        addChild(ici, aAffich);
        return;
    }
    else{               //il reste des fils et il n'y a pas de cycle
        if(rel->isOriented()){
            QTreeWidgetItem* tmp = addChild(ici, aAffich);
            pere.push_back(aAffich);
            for(unsigned int i=0;i<fils.size();i++)
                affichDesc(pere, tmp, fils[i]->getNoteY(), rel);
            pere.pop_back();
        }
        else{           //relation non orienté
            QTreeWidgetItem* tmp = addChild(ici, aAffich);
            pere.push_back(aAffich);
            for(unsigned int i=0;i<fils.size();i++){
                if(fils[i]->getNoteX() == aAffich)
                    affichDesc(pere, tmp, fils[i]->getNoteY(), rel);
                else
                    affichDesc(pere, tmp, fils[i]->getNoteX(), rel);
                pere.pop_back();
            }
        }
    }
}

void RelationListWidget::affichAcs(std::vector<std::string> pere, QTreeWidgetItem* ici, std::string aAffich, Relation* rel){
    for(unsigned int i=0; i<pere.size();i++){
        if(pere[i] == aAffich){ //cycle
            addChild(ici, aAffich);
            return;
        }
    }
    std::vector<Couple*> fils = rel->getAscendants(aAffich);
    if(fils.size()==0){ //plus de fils
        addChild(ici, aAffich);
        return;
    }
    else{               //il reste des fils et il n'y a pas de cycle
        if(rel->isOriented()){
            QTreeWidgetItem* tmp = addChild(ici, aAffich);
            pere.push_back(aAffich);
            for(unsigned int i=0;i<fils.size();i++)
                affichAcs(pere, tmp, fils[i]->getNoteX(), rel);
            pere.pop_back();
        }
        else{           //relation non orienté
            QTreeWidgetItem* tmp = addChild(ici, aAffich);
            pere.push_back(aAffich);
            for(unsigned int i=0;i<fils.size();i++){
                if(fils[i]->getNoteX() == aAffich)
                    affichAcs(pere, tmp, fils[i]->getNoteY(), rel);
                else
                    affichAcs(pere, tmp, fils[i]->getNoteX(), rel);
                pere.pop_back();
            }
        }
    }
}

void RelationListWidget::itemPressed(QTreeWidgetItem* i) {
    ui->treeWidget->clearFocus();
    emit noteClicked(i->text(0));
}
