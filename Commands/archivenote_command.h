#ifndef ARCHIVENOTECOMMAND_H
#define ARCHIVENOTECOMMAND_H

#include <QUndoCommand>
/*!
 * \brief La classe ArchiveNoteCommand gère la commande d'envoi d'une note active vers les archives, et son annulation.
 */
class ArchiveNoteCommand : public QUndoCommand
{
private:
	std::string id;
public:
	/*!
	 * \brief Construit une commande qui se charge d'archiver une Note active donnée.
	 * \param id L'identifiant de la note active à archiver.
	 * \param parent La commande mère, si il y en a une.
	 */
	ArchiveNoteCommand(const std::string& id, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Archive note "+id), parent), id(id) {}
	/*!
	 * \brief Passe la Note en Note active.
	 */
	void undo();
	/*!
	 * \brief Passe la Note en Note archivée.
	 */
	void redo();
};

#endif // ARCHIVENOTECOMMAND_H
