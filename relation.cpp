#include "relation.h"
#include "notes_exception.h"
#include "notes_manager.h"

#include <QDomElement>
#include <QDebug>

QDomDocumentFragment Couple::toXml() const {
	
	QDomDocument doc;

	QDomDocumentFragment f = doc.createDocumentFragment();
	
	QDomElement e = doc.createElement("couple");

	QDomElement n = doc.createElement("noteX");
	n.appendChild(doc.createTextNode(QString::fromStdString(noteX)));
	e.appendChild(n);

	n = doc.createElement("noteY");
	n.appendChild(doc.createTextNode(QString::fromStdString(noteY)));
	e.appendChild(n);

	n = doc.createElement("label");
	n.appendChild(doc.createTextNode(QString::fromStdString(label)));
	e.appendChild(n);

	f.appendChild(e);
	return f;
}

Couple Couple::fromXml(const QDomElement& e) {
	if(e.tagName() != "couple" || 
	   e.firstChildElement("noteX").isNull() || 
	   e.firstChildElement("noteY").isNull())
	   throw BadXmlException();
	
	std::string nx =e.firstChildElement("noteX").text().toStdString();
	std::string ny = e.firstChildElement("noteY").text().toStdString();
	std::string l = e.firstChildElement("label").text().toStdString();
	
	return Couple(nx, ny, l);
}


QDomDocumentFragment Relation::toXml() const {
	QDomDocument doc;
	
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	QDomElement e = doc.createElement("relation");

	
	
	QDomElement n;
	n = doc.createElement("title");
	n.appendChild(doc.createTextNode(QString::fromStdString(this->title)));
	e.appendChild(n);

	n = doc.createElement("description");
	n.appendChild(doc.createTextNode(QString::fromStdString(this->description)));
	e.appendChild(n);

	n = doc.createElement("oriented");
	n.appendChild(doc.createTextNode(this->oriented?"true":"false"));
	e.appendChild(n);
	
	n = doc.createElement("couples");
	
	for(auto it = enRel.begin(); it != enRel.end(); it++) {
		n.appendChild((*it)->toXml());
	}
	
	e.appendChild(n);
	
	f.appendChild(e);

	return f;
}

Relation* Relation::fromXml(const QDomElement& e) {
	if(e.tagName() != "relation" || 
	   e.firstChildElement("title").isNull() || 
	   e.firstChildElement("oriented").isNull())
		throw BadXmlException();
	
	
	std::string title = e.firstChildElement("title").text().toStdString();
	std::string desc = e.firstChildElement("description").text().toStdString();
	bool oriented = e.firstChildElement("oriented").text()=="true"?true:false;
	
	
	Relation *rel = new Relation(title, desc, oriented);
	
	QDomElement c = e.firstChildElement("couples").firstChildElement();
	if(!c.isNull()) {
		rel->addCouple(Couple::fromXml(c));
		do {
			c = c.nextSiblingElement();
			if(c.isNull())
				break;

			rel->addCouple(Couple::fromXml(c));
		} while (c != e.firstChildElement("couples").lastChildElement());
	}
	return rel;
}

void Relation::addCouple(Couple c) {
	for(auto it = enRel.begin(); it != enRel.end(); it++) {
		if(((*it)->getNoteX() == c.getNoteX() && (*it)->getNoteY() == c.getNoteY())
		   || (!oriented && ((*it)->getNoteX() == c.getNoteY() && (*it)->getNoteY() == c.getNoteX())))
			throw NotesException("Couple already exists");
	}
	this->enRel.push_back(new Couple(c));
}

QDomDocumentFragment RelationsManager::toXml() const {
	
	
	QDomDocument doc;
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	QDomElement e = doc.createElement("relationsmanager");
	
	QDomElement n;

	n = doc.createElement("relations");
	for(auto it = relations.begin(); it != relations.end(); it++) {
		n.appendChild((*it)->toXml());
	}
	e.appendChild(n);
	
	n = doc.createElement("reference");
	n.appendChild(reference->toXml());
	
	e.appendChild(n);
	
	f.appendChild(e);
	
	return f;
	
}
void RelationsManager::fromXml(const QDomElement& e) {
	if(e.tagName() != "relationsmanager" || 
	   e.firstChildElement("relations").isNull() || 
	   e.firstChildElement("reference").isNull())
		throw BadXmlException();
	
	reference = Relation::fromXml(e.firstChildElement("reference").firstChildElement());
	relations.clear();
	
	QDomElement c = e.firstChildElement("relations").firstChildElement();
	
	if(!c.isNull()) {
		
		relations.push_back(Relation::fromXml(c));
		do {
			c = c.nextSiblingElement();
			if(c.isNull())
				break;
			relations.push_back(Relation::fromXml(c));
		} while (c != e.firstChildElement("relations").lastChildElement());
		
	}
}



Relation::~Relation() {
	for(auto it = enRel.begin(); it != enRel.end(); it++) {
        delete *it;
    }
	enRel.clear();
}

void Relation::removeCouple(Couple c) {
	for(auto it = enRel.begin(); it != enRel.end(); it++) {
		if((*it)->getNoteX() == c.getNoteX() && (*it)->getNoteY() == c.getNoteY()) {
			enRel.erase(it);
			return;
		}
	}
}

Couple* Relation::getCouple(std::string label) const{
    for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if ((*it)->getLabel() == label)
            return (*it);
    }
   
	throw NotesException("Couple with label '"+label+"' not found in relation '"+this->title+"' !");
}

Couple* Relation::getCouple(const std::string &x, const std::string &y) const{
	for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if (((*it)->getNoteX() == x && (*it)->getNoteY() == y) ||
                (!oriented && (*it)->getNoteX() == y && (*it)->getNoteY() == x) )
            return *it;
    }
    throw NotesException("Couple not found in relation '"+this->title+"' !");
}

std::vector<Couple*> Relation::getOccurences(const std::string &n) const {
    std::vector<Couple*> occu;
    for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if ((*it)->getNoteX() == n || (*it)->getNoteY() == n)
            occu.push_back(*it);
    }
    return occu;
}

void Relation::removeOccurences(const std::string &n){
	for(auto it = enRel.begin(); it != enRel.end(); ) {
		if((*it)->getNoteX() == n || (*it)->getNoteY() == n)
		   it = enRel.erase(it);
		else
			it++;
	}
}

bool Relation::isInRelation(const std::string &n) const {
    for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if ((*it)->getNoteX() == n || (*it)->getNoteY() == n)
            return true;
    }
    return false;
}

std::vector<Couple*> Relation::getAscendants(const std::string& n) const {
    std::vector<Couple*> occu;
    for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if ((oriented == false && ((*it)->getNoteX() == n || (*it)->getNoteY() == n))|| (*it)->getNoteY() == n)
            occu.push_back(*it);
    }
    return occu;
}

std::vector<Couple*> Relation::getDescendants(const std::string &n) const {
    std::vector<Couple*> occu;
    for(auto it = enRel.begin(); it != enRel.end(); it++) {
        if ((oriented == false && ((*it)->getNoteX() == n || (*it)->getNoteY() == n)) || (*it)->getNoteX() == n)
            occu.push_back(*it);
    }
    return occu;
}


RelationsManager::~RelationsManager() {
	for(auto it = relations.begin(); it != relations.end(); it++) {
		delete *it;
	}
	relations.clear();
	delete reference;
}

bool RelationsManager::isInRelation(const std::string &n) const{
    for(auto it = relations.begin(); it != relations.end(); it++) {
        if((*it)->getOccurences(n).size() != 0)
            return true;
    }
    return false;
}
void RelationsManager::removeRelation(std::string titre){
    for(auto it = relations.begin(); it != relations.end(); ) {
        if((*it)->getTitle() == titre)
            it = relations.erase(it);
        else
			it++;
    }
}

void RelationsManager::removeOccurences(const std::string &n){
    for(auto it = relations.begin(); it != relations.end(); it++) {
        (*it)->removeOccurences(n);
    }
	reference->removeOccurences(n);
}

Relation* RelationsManager::getRelation(const std::string &title) {
	for(auto it = relations.begin(); it != relations.end(); it++) {
		if((*it)->getTitle() == title)
			return *it;
	}
	
	throw NotesException("Relation not found : "+title);
}

void RelationsManager::clear() {
	for(auto it = relations.begin(); it != relations.end(); it++) {
		delete *it;
	}
	relations.clear();
	delete reference;
	reference = new Relation("Reference","Relation between notes that reference each other");
}
