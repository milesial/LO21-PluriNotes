#ifndef NEW_NOTE_DIALOG_H
#define NEW_NOTE_DIALOG_H

#include <QDialog>
#include "note.h"

namespace Ui {
class NewNoteDialog;
}
/*!
 * \brief La classe NewNoteDialog est une fenêtre qui demande l'ID, le titre et le type de note à créer.
 * La note voulue est alors créée et ajoutée au NotesManager.
 * La fenêtre vérifie si l'ID est déjà pris, et affiche un message si c'est le cas.
 */
class NewNoteDialog : public QDialog
{
	Q_OBJECT
private slots:
	void createNote();
	void syncEdits(QString s);
signals:
	//! Signal est émis lorsque le bouton OK est cliqué, il passe l'ID de la note en paramètre
	void noteCreated(QString);
public:
	/*!
	 * \brief Construit une nouvelle fenetre de création de Note.
	 * \param parent Le Qwidget parent, si il existe.
	 */
	explicit NewNoteDialog(QWidget *parent = 0);
	~NewNoteDialog();

private:
	Ui::NewNoteDialog *ui;
};

#endif // NEW_NOTE_DIALOG_H
