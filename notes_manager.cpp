#include "notes_manager.h"
#include "note.h"
#include "notes_exception.h"
#include <stdlib.h>

#include <QFile>
#include <QXmlStreamWriter>
#include <QDir>
#include <QDebug>

#include "memento.h"

NotesManager::NotesManager()
{
	
	//factories used to load from XML
	factories["Article"] = new ArticleFactory();
	factories["Task"] = new TaskFactory();
	factories["Image"] = new ImageNoteFactory();
	factories["Video"] = new VideoNoteFactory();
	factories["Audio"] = new AudioNoteFactory();
	
}

NotesManager::~NotesManager() {
	//delete all pointers
	for(auto it = activeNotes.begin(); it != activeNotes.end(); it++)
		delete *it;
	
	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++)
		delete *it;
	
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++)
		delete *it;
	
	for(auto it = activeVersions.begin(); it != activeVersions.end(); it++)
		delete it->second;
	
	for(auto it = factories.begin(); it != factories.end(); it++)
		delete it->second;
}


bool NotesManager::isArchived(const std::string& id) {
	for(auto it = beginArchived(); it != endArchived(); it++) {
		if((*it)->getId() == id)
			return true;
	}
	return false;
}

bool NotesManager::isActive(const std::string& id) {
	for(auto it = beginActive(); it != endActive(); it++) {
		if((*it)->getId() == id)
			return true;
	}
	return false;
}

bool NotesManager::isDeleted(const std::string& id) {
	for(auto it = beginDeleted(); it != endDeleted(); it++) {
		if((*it)->getId() == id)
			return true;
	}
	return false;
}

const VersionNote* NotesManager::getVersion(const std::string& id) {
	for(auto it = activeVersions.begin(); it != activeVersions.end(); it++) {
		if(it->second->getId() == id)
			return it->second;
	}

	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++) {
		if((*it)->getId() == id)
			return *it;
	}
	
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		if((*it)->getId() == id)
			return *it;
	}
	
	throw IdNotFoundException(id);
}

VersionNote* NotesManager::getActiveVersion(const std::string& id) {
	for(auto it = activeVersions.begin(); it != activeVersions.end(); it++) {
		if(it->second->getId() == id)
			return it->second;
	}
	throw IdNotFoundException(id);
}

const Note* NotesManager::deleteNote(const std::string& id) {
	
	for(auto it = activeNotes.begin(); it != activeNotes.end(); it++) {
		if((*it)->getId() == id) {
			save(id);
			Note *n = *it;
			activeNotes.erase(it);
			if(relations.getReference()->getAscendants(id).size() == 0) {
				//add to trash bin
				deletedNotes.push_back(activeVersions.at(id));
				activeVersions.erase(activeVersions.find(id));
				delete n;
				return getDeletedNote(id);
			} else {
				archivedNotes.push_back(activeVersions.at(id));
				activeVersions.erase(activeVersions.find(id));
				delete n;
				return getArchivedNote(id);
			}
			
		}
	}
	
	throw IdNotFoundException(id);
}

const Note* NotesManager::setArchived(const std::string& id) {
	for(auto it = activeNotes.begin(); it != activeNotes.end(); it++) {
		if((*it)->getId() == id) {
			save(id);
			Note *n = *it;
			activeNotes.erase(it);
			archivedNotes.push_back(activeVersions.at(id));
			activeVersions.erase(activeVersions.find(id));
			delete n;
			return getArchivedNote(id);
		}
	}
	
	throw IdNotFoundException(id);
}

Memento* NotesManager::save(const std::string& id) {
	checkReferences(id);
	
	for(auto it = activeNotes.begin(); it != activeNotes.end(); it++) {
		if((*it)->getId() == id) {
			Memento *m = new Memento(*it);
			activeVersions.at(id)->addVersion(m);
			return m;
		}
	}
	throw IdNotFoundException(id);
	
}

void NotesManager::checkReferences(const std::string& id) {
	//supp tous les descendants dans ref
	std::vector<Couple*> l = relations.getReference()->getDescendants(id);
	for(auto it = l.begin(); it != l.end(); it++) {
		relations.getReference()->removeCouple(*(*it));
	}
	
	//check references
	QRegExp rx("\\\\ref\\{([ a-zA-Z0-9_-]*)\\}");
	QString s = QString::fromStdString(getNote(id)->toString());
	int pos = 0;
	while ((pos = rx.indexIn(s, pos)) != -1) {
	    QString id2 = rx.cap(1);
		if(isActive(id2.toStdString()) || isArchived(id2.toStdString())) {
			try {
				relations.getReference()->addCouple(Couple(id,id2.toStdString()));
			} catch(NotesException& e) {
				//whatever
			}
		}
		pos += rx.matchedLength();
	}
}

void NotesManager::clear() {
	for(auto it = activeNotes.begin(); it != activeNotes.end(); it++) {
		delete *it;
	}
	activeNotes.clear();
	
	for(auto it = activeVersions.begin(); it != activeVersions.end(); it++) {
		delete it->second;
	}
	activeVersions.clear();
	
	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++) {
		delete *it;
	}
	archivedNotes.clear();
	
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		delete *it;
	}
	deletedNotes.clear();
	
	relations.clear();
}

void NotesManager::deleteFromBin(const std::string& id) {
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		if((*it)->getId() == id) {
			relations.removeOccurences(id);
			const VersionNote *v = *it;
			deletedNotes.erase(it);
			delete v;
			return;
		}
	}
	
	throw IdNotFoundException(id);
}

void NotesManager::deleteFromArchive(const std::string& id) {
	if(relations.getReference()->getAscendants(id).size() > 0)
		throw NotesException("Can't delete the note '"+id+"', still has references");
	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++) {
		if((*it)->getId() == id) {
			relations.removeOccurences(id);
			const VersionNote *v = *it;
			archivedNotes.erase(it);
			delete v;
			return;
		}
	}
	
	throw IdNotFoundException(id);
}

void NotesManager::clearBin() {
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		relations.removeOccurences((*it)->getLast()->getState()->getId());
		delete *it;
	}
	deletedNotes.clear();
}

Note* NotesManager::restoreFromBin(const std::string& id) {
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		if((*it)->getId() == id) {
			const VersionNote *v = *it;
			activeVersions[id] = new VersionNote(*v);
			activeNotes.push_back(v->getLast()->getState()->clone());
			deletedNotes.erase(it);
			return getActiveNote(id);
		}
	}
	
	throw IdNotFoundException(id);
}

Note* NotesManager::restoreFromArchive(const std::string& id) {
	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++) {
		if((*it)->getId() == id) {
			const VersionNote *v = *it;
			activeVersions[id] = new VersionNote(*v);
			activeNotes.push_back(v->getLast()->getState()->clone());
			archivedNotes.erase(it);
			return getActiveNote(id);
		}
	}
	
	throw IdNotFoundException(id);
}

Note* NotesManager::restore(const std::string& id) {
	if(isDeleted(id))
		return restoreFromBin(id);
	if(isArchived(id))
		return restoreFromArchive(id);
	
	throw IdNotFoundException(id);
}

Note* NotesManager::getActiveNote(const std::string& id) {
	for(auto it = beginActive(); it != endActive(); it++) {
		if((*it)->getId() == id)
			return *it;
	}
	
	throw IdNotFoundException(id);
}

const Note* NotesManager::getArchivedNote(const std::string& id) {
	for(auto it = beginArchived(); it != endArchived(); it++) {
		if((*it)->getId() == id)
			return *it;
	}
	
	throw IdNotFoundException(id);
}

const Note* NotesManager::getDeletedNote(const std::string& id) {
	
	for(auto it = beginDeleted(); it != endDeleted(); it++) {
		if((*it)->getId() == id)
			return *it;
	}
	
	throw IdNotFoundException(id);
}


const Note* NotesManager::getNote(const std::string& id) {
	try {
		return getActiveNote(id);
	} catch(IdNotFoundException& e) {
		try {
			return getDeletedNote(id);
		} catch(IdNotFoundException& e) {
			return getArchivedNote(id);
		}
	}
}

void NotesManager::saveToFile(const std::string& filename) {
	
	QString filepath = QString::fromStdString(filename);
	
	QDomDocument doc("plurinotes");
	QDomElement root = doc.createElement("plurinotes");
	
	
	//NOTES
	QDomElement n = doc.createElement("notes");
	
	//Actives
	QDomElement na = doc.createElement("active");
	for(auto it = activeVersions.begin(); it != activeVersions.end(); it++) {
		na.appendChild((*it).second->toXml());
	}
	n.appendChild(na);
	
	//Archived
	na = doc.createElement("archived");
	for(auto it = archivedNotes.begin(); it != archivedNotes.end(); it++) {
		na.appendChild((*it)->toXml());
	}
	n.appendChild(na);
	
	//Deleted
	na = doc.createElement("deleted");
	for(auto it = deletedNotes.begin(); it != deletedNotes.end(); it++) {
		na.appendChild((*it)->toXml());
	}
	n.appendChild(na);
	
	root.appendChild(n);
	
	//RELATIONS
	root.appendChild(relations.toXml());
	
	doc.appendChild(root);
	
	
	//write to file
	QFile f(filepath);
	if(f.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream stream(&f);
		doc.save(stream, QDomNode::CDATASectionNode);
	} else {
		throw NotesException("Can't write to file "+filepath.toStdString());
	}
	
	f.close();
}


void NotesManager::loadFromFile(const std::string& filename) {
	
	//Opens file and converts to XML doc
	QDomDocument doc;
	QFile file(QString::fromStdString(filename));
		
	if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
		throw NotesException("Can't open file "+filename);
	
	if(!doc.setContent(&file)) {
		file.close();
		throw NotesException("Can't parse file "+filename);
	}
		
	file.close();
	
	//root <plurinotes>
	QDomElement root = doc.firstChildElement();
		
	//node <notes>
	QDomElement notes = root.firstChildElement("notes");
	
	QDomElement version, first;
	NoteFactory *fact;
	std::string id;
	QDomNodeList l;
	VersionNote *v;
	
	//versions from node <active>
	QDomNodeList active = notes.firstChildElement("active").childNodes();
	
	
	
	for(int i = 0; i < active.size(); i++) {
		fact = nullptr;
		version = active.at(i).toElement();
		
		first = version.firstChildElement();
		
		//check whith wich factory we must build the note
		for(auto it = factories.begin(); it != factories.end(); it++) {
			if((it->second->isXmlValid(first))) {
				fact = it->second;
				break;
			}
		}
		
		if(!fact)
			throw NotesException("Can't parse note : no factory suites the note");
		
		//list of notes in the version
		l = version.childNodes();
		id = version.attribute("id").toStdString();
		v = new VersionNote(id);
		
		//ajoute à l'envers
		for(int n = l.size()-1; n >= 0 ; n--) {
			Note *nn = fact->fromXml(l.at(n).toElement());
			v->addVersion(new Memento(nn), true);
		}
		
		activeNotes.push_back(v->getLast()->getState()->clone());
		activeVersions[id] = v;
	}
	
	
	
	//versions from node <archived>
	QDomNodeList archived = notes.firstChildElement("archived").childNodes();
	
	
	
	for(int i = 0; i < archived.size(); i++) {
		fact = nullptr;
		version = archived.at(i).toElement();
		
		first = version.firstChildElement();
		
		//check whith wich factory we must build the note
		for(auto it = factories.begin(); it != factories.end(); it++) {
			if((it->second->isXmlValid(first))) {
				fact = it->second;
				break;
			}
		}
		
		if(!fact)
			throw NotesException("Can't parse note : no factory suites the note");
		
		//list of notes in the version
		l = version.childNodes();
		id = version.attribute("id").toStdString();
		v = new VersionNote(id);
		
		//ajoute à l'envers
		for(int n = l.size()-1; n >= 0 ; n--) {
			Note *nn = fact->fromXml(l.at(i).toElement());
			v->addVersion(new Memento(nn), true);
		}
		
		archivedNotes.push_back(v);
	}
	
	
	//versions from node <deleted>
	QDomNodeList deleted = notes.firstChildElement("deleted").childNodes();
	
	
	
	for(int i = 0; i < deleted.size(); i++) {
		fact = nullptr;
		version = deleted.at(i).toElement();
		
		first = version.firstChildElement();
		
		//check whith wich factory we must build the note
		for(auto it = factories.begin(); it != factories.end(); it++) {
			if((it->second->isXmlValid(first))) {
				fact = it->second;
				break;
			}
		}
		
		if(!fact)
			throw NotesException("Can't parse note : no factory suites the note");
		
		//list of notes in the version
		l = version.childNodes();
		id = version.attribute("id").toStdString();
		v = new VersionNote(id);
		
		//ajoute à l'envers
		for(int n = l.size()-1; n >= 0 ; n--) {
			Note *nn = fact->fromXml(l.at(i).toElement());
			v->addVersion(new Memento(nn), true);
		}
		
		deletedNotes.push_back(v);
	}
	
	
	relations.fromXml(root.firstChildElement("relationsmanager"));	
}

Note* NotesManager::restoreVersion(const Memento* m) {
	std::string id = m->getState()->getId();
	
	Note *n = m->getState()->clone();
	
	
	for(auto it = beginActive(); it != endActive(); it++) {
		if((*it)->getId() == id) {
			delete *it;
			activeNotes.erase(it);
			activeNotes.push_back(n);
			checkReferences(id);
			return n;
		}
	}
	
	throw IdNotFoundException(id);
	
	
}


void NotesManager::saveAll() {
	for(auto it = beginActive(); it != endActive(); it++) {
		save(*it);
	}
}

void NotesManager::addNewNote(Note* n) {
	if(hasNote(n->getId()))
		throw IdTakenException(n->getId());
	
	activeNotes.push_back(n);
	
	VersionNote *v = new VersionNote(n->getId());
	activeVersions[n->getId()] = v;
	//Memento *m = new Memento(n);
	//activeVersions.at(n->getId())->addVersion(m);
}

bool NotesManager::isSaved(const std::string& id) {
	return getVersion(id)->getSize()>0 && (getActiveNote(id)->getDateModified() == getVersion(id)->getLast()->getState()->getDateModified());
}
