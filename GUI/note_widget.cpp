#include <QDateTime>
#include <QTextEdit>
#include <iterator>
#include <QList>
#include <QLabel>

#include "note_widget.h"
#include "ui_note_widget.h"
#include "note.h"



NoteWidget::NoteWidget(Note *note, QWidget *parent) :
    QWidget(parent),
    note(note),
    id(note->getId()),
    readonly(false),
    ui(new Ui::NoteWidget)
{
	ui->setupUi(this);

	setupFields(note);
	connect(ui->titleLabel, SIGNAL(textChanged(QString)), this, SLOT(setNoteTitle(QString)));
	connect(this, SIGNAL(noteModified(QString)), this, SLOT(updateDateField()));

}


NoteWidget::NoteWidget(const Note *note, QWidget *parent) :
    QWidget(parent),
    note(nullptr),
    id(note->getId()),
    readonly(true),
    ui(new Ui::NoteWidget)
{
	ui->setupUi(this);
	
	setupFields(note);
	ui->titleLabel->setReadOnly(true);

}

void NoteWidget::setupFields(const Note* note) {
	const QString dateFormat = "d MMM yy - hh:mm";

	//pour avoir les mois en anglais (ce soucis du détail !)
	QLocale locale  = QLocale(QLocale::English, QLocale::UnitedStates);

	ui->idLabel->setText(QString::fromStdString(note->getId()));
	ui->titleLabel->setText(QString::fromStdString(note->getTitle()));

	QDateTime date = QDateTime::fromTime_t(note->getDateCreated());
	ui->createdLabel->setText(locale.toString(date, dateFormat));

	date = QDateTime::fromTime_t(note->getDateModified());
	ui->modifiedLabel->setText(locale.toString(date, dateFormat));
	
}

void NoteWidget::updateDateField() {
	if(this->note) {
		const QString dateFormat = "d MMM yy - hh:mm";
		QLocale locale  = QLocale(QLocale::English, QLocale::UnitedStates);
		QDateTime date = QDateTime::fromTime_t(note->getDateModified());
		ui->modifiedLabel->setText(locale.toString(date, dateFormat));
	}
}

NoteWidget::~NoteWidget()
{
	delete ui;
}

QWidget* NoteWidget::getContainer() {
	return ui->container;
}

void NoteWidget::setNoteTitle(QString s) {
	note->setTitle(s.toStdString());
	emit noteModified(s);
}
