#ifndef ADD_COUPLE_DIALOG_H
#define ADD_COUPLE_DIALOG_H

#include <QDialog>
#include "relation.h"

namespace Ui {
class AddCoupleDialog;
}
/*!
 * \brief La classe AddCoupleDialog est une fenêtre qui permet de créer un nouveau Couple dans une Relation.
 * Le Couple est créée à partir de 2 notes existantes (actives ou archivées) et d'un label.
 * Il n'est pas ajouté au NotesManager, mais passé en paramètre du signal.
 * 
 */
class AddCoupleDialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit AddCoupleDialog(QWidget *parent = 0);
	~AddCoupleDialog();
	
private:
	Ui::AddCoupleDialog *ui;
	
signals:
	//! Signal émis lorsque le bouton OK est cliqué.
	void coupleCreated(Couple);
	
private slots:
	void createCouple();
};

#endif // ADD_COUPLE_DIALOG_H
