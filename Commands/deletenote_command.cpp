#include "deletenote_command.h"
#include "notes_manager.h"

void DeleteNoteCommand::undo() {
	if(NotesManager::getInstance().isDeleted(id))
		NotesManager::getInstance().restore(id);
}

void DeleteNoteCommand::redo() {
	if(NotesManager::getInstance().isActive(id))
		NotesManager::getInstance().deleteNote(id);
}
