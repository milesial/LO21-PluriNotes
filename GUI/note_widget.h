#ifndef NOTE_WIDGET_H
#define NOTE_WIDGET_H

#include <QWidget>


namespace Ui {
class NoteWidget;
}


class Note;
/*!
 * \brief La classe NoteWidget est un QWidget qui affiche le contenu d'un note.
 * Si la note passée dans le constructeur est constante, le widget passe en mode lecture seulement,
 * sinon, tous les champs si modifiables et la note associée est mise à jour en fonction.
 */
class NoteWidget : public QWidget
{
	Q_OBJECT
public:
	/*!
	 * \brief Construit un NoteWidget qui affiche une Note avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de la Note associé.
	 * \param note La Note à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit NoteWidget(Note *note, QWidget *parent = 0);	
	/*!
	 * \brief Construit un NoteWidget qui affiche une Note en mode lecture seule.
	 * \param note La Note à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit NoteWidget(const Note *note, QWidget *parent = 0);
	~NoteWidget();
	
	//! @return Si le widget est en mode lecture seule ou non.
	bool isReadOnly() const { return readonly; }
	//! Retourne l'identifiant de la Note associée
	std::string getId() const { return id; }
protected:
	//! La Note associée (seulement si les champs sont modifiables)
	Note *note;
	//! L'identifiant de la note associée
	std::string id;
	//! Récupère le widget central vide qui permet aux classe spécialisées d'étendre l'interface graphique
	virtual QWidget* getContainer();
	//! Initialise les champs depuis la Note passée en paramètre
	virtual void setupFields(const Note* note);
	
	
private:
	bool readonly;
	Ui::NoteWidget *ui;
	
	
private slots:
	void setNoteTitle(QString s);
	void updateDateField();
signals:
	//! Signal émis à chaque fois qu'un champ est modifié. Si le champ contient du texte, ce texte est passé en 
	//! paramètre (afin de vérifier les références)
	void noteModified(QString);
};

#endif // NOTE_WIDGET_H
