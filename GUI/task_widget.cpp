#include "task_widget.h"
#include "ui_task_widget.h"
#include "note.h"

#include <QDateTime>

TaskWidget::TaskWidget(Task *task, QWidget *parent) :
	NoteWidget(task, parent),
    ui(new Ui::TaskWidget)
{
	ui->setupUi(getContainer());

	setupFields(task);
	
	connect(ui->priorityCheck, SIGNAL(clicked(bool)), this, SLOT(toggleTaskPriority(bool)));
	connect(ui->deadlineCheck, SIGNAL(clicked(bool)), this, SLOT(toggleTaskDeadline(bool)));
	connect(ui->prioritySpin, SIGNAL(valueChanged(int)), this, SLOT(setTaskPriority(int)));
	connect(ui->deadlineEdit, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(setTaskDeadline(QDateTime)));
	connect(ui->actionEdit, SIGNAL(textChanged()), this, SLOT(setTaskAction()));
	connect(ui->stateList, SIGNAL(currentIndexChanged(QString)), this, SLOT(setTaskState(QString)));
}

TaskWidget::TaskWidget(const Task *task, QWidget *parent) :
	NoteWidget(task, parent),
    ui(new Ui::TaskWidget)
{
	ui->setupUi(getContainer());

	setupFields(task);	
	
	ui->deadlineEdit->setReadOnly(true);
	ui->actionEdit->setReadOnly(true);
	ui->prioritySpin->setReadOnly(true);
	ui->priorityCheck->setDisabled(true);
	ui->deadlineCheck->setDisabled(true);
	ui->stateList->setDisabled(true);
	
}


void TaskWidget::setupFields(const Task *task) {
	
	connect(ui->priorityCheck, SIGNAL(toggled(bool)), ui->prioritySpin, SLOT(setEnabled(bool)));
	connect(ui->deadlineCheck, SIGNAL(toggled(bool)), ui->deadlineEdit, SLOT(setEnabled(bool)));
	
	ui->actionEdit->setHtml(QString::fromStdString(task->getAction()));
	ui->priorityCheck->setChecked(task->hasPriority());
	

	if(task->hasPriority())
		ui->prioritySpin->setValue(task->getPriority());

	ui->deadlineCheck->setChecked(task->hasDeadline());

	if(task->hasDeadline())
		ui->deadlineEdit->setDateTime(QDateTime::fromTime_t(task->getDeadline()));

	

	ui->deadlineEdit->setDateTime(QDateTime::currentDateTime());
	ui->deadlineEdit->setMinimumDateTime(QDateTime::currentDateTime());

	QStringList list=(QStringList()<<"Waiting"<<"In progress"<<"Finished");
	ui->stateList->addItems(list);
}

TaskWidget::~TaskWidget()
{
	delete ui;
}

void TaskWidget::setTaskPriority(int p) {
	static_cast<Task*>(note)->setPriority(p);
	emit noteModified("");
}

void TaskWidget::setTaskDeadline(QDateTime s) {
	static_cast<Task*>(note)->setDeadline(s.toTime_t());
	emit noteModified("");
}

void TaskWidget::setTaskAction() {
	static_cast<Task*>(note)->setAction(ui->actionEdit->toHtml().toStdString());
	emit noteModified(ui->actionEdit->toPlainText());
}

void TaskWidget::setTaskState(QString s) {
	Task* t = static_cast<Task*>(note);
	
	if(s == "Waiting")
		t->setState(Task::WAITING);
	else if(s== "In progress")
		t->setState(Task::IN_PROGRESS);
	else if(s == "Finished")
		t->setState(Task::FINISHED);
	
	emit noteModified("");
}

void TaskWidget::toggleTaskPriority(bool b) {
	if(b) {
		static_cast<Task*>(note)->setPriority(ui->prioritySpin->value());
	} else {
		static_cast<Task*>(note)->setPriority(0);
	}
	emit noteModified("");
}

void TaskWidget::toggleTaskDeadline(bool b) {
	if(b) {
		time_t t = ui->deadlineEdit->dateTime().toTime_t();
		static_cast<Task*>(note)->setDeadline(t);
	} else {
		static_cast<Task*>(note)->setDeadline(0);
	}
	emit noteModified("");
}

