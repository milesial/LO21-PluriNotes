#ifndef RELATION_H
#define RELATION_H
#include "note.h"

/*!
 * \brief A Couple est un ensemble de deux notes, décrit par un label
 */
class Couple
{
    std::string label; //!<Label caracteristique d'une note
    const std::string noteX, noteY; //!< Les ids des deux notes formant le couple
public:

    /*!
     * \brief Construit un couple a partir de 2 id et un label (optionnel)
     * \param x L id de la note X
     * \param y L id de la note y
     * \param lb Le label peut etre omis
     */
    Couple( const std::string& x,  const std::string& y, std::string lb=""):
        label(lb), noteX(x), noteY(y) {}


    //!
    //! \brief Accesseur de la classe
    //! \return Label
    //!
    std::string getLabel() const {return label;}

    //!
    //! \brief getNoteX Accesseur
    //! \return L'id noteX
    //!
    std::string getNoteX() const {return noteX;}

    //!
    //! \brief getNoteY Accesseur
    //! \return l'id noteY.
    //!
    std::string getNoteY() const {return noteY;}

    /*!
     * \brief Mutateur de la classe, modifie le label
     * \param label La chaine de caracteres qui va prendre la place du label de l'objet
     */
    void setLabel(const std::string& label) {
        this->label = label;
    }

    //!
    //! \brief toXml Copie notre Couple au format .xml
    //! \return un element couple en xml
    //!
	QDomDocumentFragment toXml() const;

    //! Recupere un couple a partir d'un element XML
    //! \param e L'element xml
    //! \return Le couple une fois recupere
	static Couple fromXml(const QDomElement& e);
};

/*!
 * \brief Une relation est une relation au sens mathématique entre notes
 * \details Elle représente un ensemble de couples de notes, décrit par un titre et une description.
 * Elle peut être orientée ou non.
 */
class Relation{
    std::string title;  //!<Le titre caractéristique d'une relation
    std::string description; //!<un description breve
    bool oriented;  //!< Permet de savoir si la relation est orientée
    std::vector<Couple*> enRel; //!< Les couples en relation
public:
    /*!
     * \brief Construit une relation avec un titre, une description et un orientation (par defaut orienté)
     * \param t Le titre
     * \param d La description (par defaut "")
     * \param o orientation ou non (par defaut orienté)
     */
    Relation(std::string t, std::string d ="", bool o = true):
        title(t), description(d), oriented(o) {}
	~Relation();

    //!
    //! \brief getTitle Accesseur
    //! \return titre
    //!
    std::string getTitle() const {return title;}

    //!
    //! \brief getDescription Accesseur
    //! \return description
    //!
    std::string getDescription() const {return description;}

    //!
    //! \brief isOriented informe si la Relation est orientée
    //! \return le booleen qui indique si la relation est oreintée
    //!
    bool isOriented() const {return oriented;}
	
    //!
    //! \brief getCouples Accesseur
    //! \return tous les couples de la relation
    //!
    const std::vector<Couple*>& getCouples() const {return enRel;}
	
    //! \brief setTitle change le titre de la rel avec t
    //! \param t Le nouveau titre
    void setTitle(std::string t) {this->title = t;}

    //! \brief setDescription change la description
    //! \param d La nouvelle Description
    void setDescription(std::string d) {this->description = d;}

    //! \brief setOriented change l'orientation
    //! \param o La nouvelle orientation
    void setOriented(bool o) {this->oriented = o;}

    //! \brief addCouple ajoute un Couple à notre relation
    //! \param c Le couple à ajouter.
    void addCouple(Couple c);

    //! \brief removeCouple enleve un Couple c à notre relation
    //! \param c le couple à supprimer
	void removeCouple(Couple c);
	
    //! \brief getCouple Accesseur
    //! \param label le label de la note que l'on veut trouver
    //! \return Le couple dont le label est passé en argument
    Couple* getCouple(std::string label) const;

    //! \brief getCouple Accesseur
    //! \param x L'ID X du couple que l'on veut trouver
    //! \param y L'ID Y du couple que l'on veut trouver
    //! \return Le couple dont les IDs sont passés en argument
    Couple* getCouple( const std::string& x,  const std::string& y) const;
	
    //! \brief Accesseur
    //! \param n La note dont on veut les occurences
    //! \return un tableau de couple de la relation dans lequel il y a n
    std::vector<Couple*> getOccurences(const std::string& n) const;

    //! \brief Accesseur
    //! \param n La note dont on veut les acsendants direct
    //! \return les Acsendants direct dans la relation de la note dont l'id est n
	std::vector<Couple*> getAscendants(const std::string& n) const;

    //! \brief Accesseur
    //! \param n La note dont on veut les decsendants direct
    //! \return les Decsendants direct dans la relation de la note dont l'id est n
    std::vector<Couple*> getDescendants(const std::string& n) const;
	
    //! \brief removeOccurences supprime les couples de la relation dans lesquels sont impliqués n.
    //! \param n La note dont on veut supprimer les occurences
    void removeOccurences(const std::string& n);
	
    //! \brief isInRelation vérifie si la note apparaît dans un des couples de la relation
    //! \param n l'ID de la note a tester
    //! \return vrai si est en relation faux sinon
    bool isInRelation(const std::string& n) const;
	
    //!
    //! \brief toXml crée une copie de la relation en xml
    //! \return une copie de notre relation au format xml
    //!
	QDomDocumentFragment toXml() const;

    //!
    //! \brief fromXml recupere une Relation a partir d'un fichier .xml
    //! \param e L'element xml
    //! \return la relation recuperee
    //!
	static Relation* fromXml(const QDomElement& e);
};

/*!
 * \brief Le RelationsManager gère un ensemble de relations, en plus de la relation préexistente des références
 */
class RelationsManager{
    Relation* reference;    //!<La réference qui existe toujours
    std::vector<Relation*> relations;//!<Les relations à gerer
public:

    //!
    //! \brief RelationsManager crée la RelationManager et la reference associé
    //!
    RelationsManager() : reference(new Relation("Reference","Relation between notes that reference each other")) {}
	~RelationsManager();

    //! \brief addRelation ajoute une Relation au RelationManager.
    //! \param r La relation à ajouter.
    void addRelation(Relation* r) {relations.push_back(r);}
	
    //!
    //! \brief getRelations Accesseur
    //! \return la liste des relations (hors références)
    //!
    const std::vector<Relation*>& getRelations() const {return relations;}
	
    //!
    //! \brief getReference accesseur
    //! \return la référence
    //!
    Relation* getReference() {return reference;}
	
    //!
    //! \brief getRelation Accesseur
    //! \param title
    //! \return La relation dans RelationManager dont le titre est title
    //!
    Relation* getRelation(const std::string& title);
	
    //!
    //! \brief isInReference vérifie si la note est dans les références
    //! \param n l'ID de la note
    //! \return vrai si est presente, faux sinon
    //!
    bool isInReference(const std::string& n) const {return reference->isInRelation(n); }
	
    //!
    //! \brief isInRelation vérifie si la note est dans une relation (hors références)
    //! \param n l'Id de la note
    //! \return vrai si est presente , faux sinon
    //!
    bool isInRelation(const std::string& n) const;
	
    //!
    //! \brief removeRelation supprime une relation dont le titre est titre
    //! \param titre
    //!
    void removeRelation(std::string titre);
	
    //!
    //! \brief removeOccurences supprime tous les couples dans lesquels la note apparaît, dans toutes les relations (sauf les références)
    //! \param n la note.
    //!
    void removeOccurences(const std::string& n);
	
    //!
    //! \brief clear supprime toutes les relations de notre RelationManager et vide Reference
    //!
	void clear();
	
    //!
    //! \brief toXml crée une copie de notre RelationManager en xml
    //! \return  renvoie la copie
    //!
	QDomDocumentFragment toXml() const;

    //!
    //! \brief fromXml récupere le RelationManager contenu dans un element xml
    //! \param e L'element xml qui stocke (normalement un RM)
    //!
	void fromXml(const QDomElement& e);
};

#endif // RELATION_H
