#ifndef NEW_RELATION_DIALOG_H
#define NEW_RELATION_DIALOG_H

#include "relation.h"
#include <QDialog>

namespace Ui {
class NewRelationDialog;
}
/*!
 * \brief La classe NewRelationDialog est une fenêtre permettant de créer une nouvelle Relation.
 * L'utilisateur entre le titre de la relation, sa description et si elle est orientée ou non.
 * La fenêtre vérifie si une relation avec le même titre existe, et affiche un message si c'est le cas.
 * Attention : la relation n'est pas ajoutée au NotesManager, mais passé en paramètre du signal.
 */
class NewRelationDialog : public QDialog
{
	Q_OBJECT
	
public:
	/*!
	 * \brief Construit une nouvelle fenetre de création de Relation.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit NewRelationDialog(QWidget *parent = 0);
	~NewRelationDialog();
	
private:
	Ui::NewRelationDialog *ui;
	
private slots:
	void createRelation();
	void checkTitle();
	
signals:
	//! Signal émis lorsque le bouton OK est cliqué, il passe en paramètre la nouvelle Relation créée.
	void relationCreated(Relation*);
};

#endif // NEW_RELATION_DIALOG_H
