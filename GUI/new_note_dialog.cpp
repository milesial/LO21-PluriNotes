#include <iostream>
#include <QPushButton>
#include <QRadioButton>

#include "new_note_dialog.h"
#include "ui_new_note_dialog.h"
#include "note_factory.h"
#include "notes_manager.h"


NewNoteDialog::NewNoteDialog(QWidget *parent) :
    QDialog(parent),
	ui(new Ui::NewNoteDialog) {


	ui->setupUi(this);

	this->setFixedSize(this->size());

	QLineEdit* idEdit = ui->idEdit;
	idEdit->setFocus();
	QRegExp rx("[ A-Za-z0-9_-]*");
	QValidator *validator = new QRegExpValidator(rx, this);
	idEdit->setValidator(validator);


	//SIGNALS SLOTS
	connect(idEdit, SIGNAL(textChanged(QString)),
			ui->titleEdit, SLOT(setText(QString)));

	connect(idEdit, SIGNAL(textChanged(QString)),
			this, SLOT(syncEdits(QString)));

	connect(this, SIGNAL(accepted()), this, SLOT(createNote()));

	QSizePolicy sp =  ui->idError->sizePolicy();
	sp.setRetainSizeWhenHidden(true);
	ui->idError->setSizePolicy(sp);
	ui->idError->hide();
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);


	const std::map<std::string, NoteFactory*>& map = NotesManager::getInstance().getFactories();
	for(auto it = map.begin(); it != map.end(); it++) {
		QRadioButton *b = new QRadioButton(this);
		b->setText(QString::fromStdString(it->first));
		b->setAccessibleName(QString::fromStdString(it->first));
		ui->radioLayout->addWidget(b);
	}
	findChildren<QRadioButton*>().first()->setChecked(true);
	
}

NewNoteDialog::~NewNoteDialog() {
	delete ui;
}

void NewNoteDialog::createNote() {
	QList<QRadioButton*> l = findChildren<QRadioButton*>();
	
	for(int i = 0; i < l.size(); i++) {
		if(l.at(i)->isChecked()) {
			NoteFactory* n = NotesManager::getInstance().getFactories().at(l.at(i)->text().toStdString());
			Note *note = n->create(ui->idEdit->text().toStdString());
			note->setTitle(ui->titleEdit->text().toStdString());
			emit noteCreated(QString::fromStdString(note->getId()));
			break;
		}
	}
}
void NewNoteDialog::syncEdits(QString s) {
	//to camel case, + transforms '_' and '-' to spaces
	QStringList parts = s.split(QRegExp("[-_\\s]+"), QString::SkipEmptyParts);
	for (int i=0; i<parts.size(); i++)
		parts[i].replace(0, 1, parts[i][0].toUpper());

	ui->titleEdit->setText(parts.join(" "));

	bool dis = false;


	//TODO : check si l'id existe
	if(NotesManager::getInstance().hasNote(s.toStdString()))
		dis = true;


	ui->idError->setVisible(dis);

	if(s == "") dis = true;
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(dis);

}
