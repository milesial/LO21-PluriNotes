#include "add_couple_dialog.h"
#include "ui_add_couple_dialog.h"

#include "notes_manager.h"
#include "relation.h"

AddCoupleDialog::AddCoupleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddCoupleDialog)
{
	ui->setupUi(this);
	
	NotesManager& nm = NotesManager::getInstance();
	
	for(auto it = nm.beginActive(); it != nm.endActive(); it++) {
		ui->noteXBox->addItem(QString::fromStdString((*it)->getId()));
		ui->noteYBox->addItem(QString::fromStdString((*it)->getId()));
	}
	
	for(auto it = nm.beginArchived(); it != nm.endArchived(); it++) {
		ui->noteXBox->addItem(QString::fromStdString((*it)->getId()));
		ui->noteYBox->addItem(QString::fromStdString((*it)->getId()));
	}
	
	connect(this, SIGNAL(accepted()), this, SLOT(createCouple()));
}

AddCoupleDialog::~AddCoupleDialog()
{
	delete ui;
}

void AddCoupleDialog::createCouple() {
	std::string x = ui->noteXBox->currentText().toStdString();
	std::string y = ui->noteYBox->currentText().toStdString();
	
	if(x != "" && y != "") {
		Couple c(x, y, ui->labelEdit->text().toStdString());
		emit coupleCreated(c);
	}
	
}
