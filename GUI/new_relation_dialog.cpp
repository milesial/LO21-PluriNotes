#include "new_relation_dialog.h"
#include "ui_new_relation_dialog.h"

#include "relation.h"
#include "notes_manager.h"

#include <QPushButton>

NewRelationDialog::NewRelationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewRelationDialog)
{
	ui->setupUi(this);
	
	this->setFixedSize(this->size());
	
	QRegExp rx("[ A-Za-z0-9_-]*");
	QValidator *validator = new QRegExpValidator(rx, this);
	ui->titleEdit->setValidator(validator);
	
	connect(ui->titleEdit, SIGNAL(textChanged(QString)), this, SLOT(checkTitle()));
	connect(this, SIGNAL(accepted()), this, SLOT(createRelation()));
	
	QSizePolicy sp =  ui->error->sizePolicy();
	sp.setRetainSizeWhenHidden(true);
	ui->error->setSizePolicy(sp);
	ui->error->hide();
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
}

NewRelationDialog::~NewRelationDialog()
{
	delete ui;
}

void NewRelationDialog::createRelation() {
	Relation* r = new Relation(ui->titleEdit->text().toStdString(),
	                           ui->descriptionEdit->toPlainText().toStdString(), 
	                           ui->checkBox->isChecked());
	emit relationCreated(r);
}

void NewRelationDialog::checkTitle() {
	QString t = ui->titleEdit->text();
	
	if(t == "") {
		ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
		return;
	}
	
	std::vector<Relation*> l = NotesManager::getInstance().getRelationsManager().getRelations();
	
	for(auto it=l.begin(); it != l.end(); it++) {
		if((*it)->getTitle() == t.toStdString()) {
			ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
			ui->error->show();
			return;
		}
	}
	
	ui->error->hide();
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(false);
}
