#ifndef NOTES_EXCEPTION
#define NOTES_EXCEPTION

#include <exception>
#include <iostream>
/*!
* \brief La classe NotesException permet de personnaliser nos exceptions.
* \details Elle herite de std::exception.
* Sera envoyé lors d'exception generale.
*/
class NotesException : public std::exception {
private:
    std::string msg; //!<Le message récupéré lors de l'exception

public:

    //!
    //! \brief NotesException construit un objet NotesException a partir d'un string
    //! \param msg le message transmit par l'exception (par defaut "unknown")
    //!
	NotesException(const std::string& msg="Unknown") : std::exception(), msg(msg) {
		this->msg.insert(0, std::string("NotesException: "));
	}
    //!
    //! \brief what permet de savoir le message/la nature de l'exception recue
    //! \return Les infos que l'on a sur l'exception (msg).
    //!
	 const char* what() const throw () {
		return msg.c_str();
	}
};

/*!
 * \brief La classe IdTakenException est une exception envoyée lorsque l'Id d'une note que l'on essaie de créer est deja utilisé.
 * \details Elle herite de NotesException.
 */
class IdTakenException : public NotesException {

public:

    //!
    //! \brief IdTakenException constructeur de l'exception.
    //! \param id L'ID deja existant
    //!
	IdTakenException(const std::string& id) :
		NotesException("ID '" + id +"' already taken !") {}
};

/*!
 * \brief la classe IdNotFoundException est une excpetion qui sera enoyé quand l'ID d'une note ne sera pas trouvé dans une relation ou autre.
 * \details Elle herite de NotesException.
 */
class IdNotFoundException : public NotesException {

public:
    //!
    //! \brief IdNotFoundException constructeur qui recupere l'ID non trouvé.
    //! \param id non trouvé
    //!
	IdNotFoundException(const std::string& id) :
		NotesException("Note with ID '" + id +"' not found !") {}
};
/*!
 * \brief La classe BadXmlException est une exception qui est appele lorsqu'on essaie d'extraire d'un element xml une classe qui n'est pas la bonne.
 * \details Elle herite de NotesException.
*/
class BadXmlException : public NotesException {

public:
    //!
    //! \brief BadXmlException constructeur qui appelle juste le constructeur de NotesException avec un message prédefini.
    //!
	BadXmlException() :
		NotesException("Invalid XML to build this object !") {}
};
#endif // NOTESEXCEPTION

