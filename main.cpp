#include <QApplication>
#include <QDebug>

#include <cstdlib>
#include <iostream>
#include <string>

#include "note_factory.h"
#include "relation.h"
#include "notes_manager.h"
#include "memento.h"
#include "GUI/new_note_dialog.h"
#include "GUI/noteslist_widget.h"
#include "GUI/notes_window.h"

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QXmlStreamWriter>

using namespace std;

/*
        __________________________
        < Projet LO21 - Plurinotes >       ______________________________________________
        --------------------------       < https://gitlab.utc.fr/milesial/LO21-PluriNotes >
          \                               ----------------------------------------------
           \                                \
                _____                        \
              .'/L|__`.                         /~\
             / =[_]O|` \                       |oo )
             |"+_____":|                       _\=/_
           __:='|____`-:__                    /     \
          ||[] ||====| []||                  //|/.\|\\
          ||[] | |=| | []||                 ||  \_/  ||
          |:||_|=|U| |_||:|                 || |\ /| ||
          |:|||]_=_ =[_||:|                  # \_ _/  #
          | |||] [_][]C|| |                    | | |
          | ||-'"""""`-|| |                    | | |
          /|\\_\_|_|_/_//|\                    []|[]
         |___|   /|\   |___|                   | | |
         `---'  |___|  `---'                  /_]_[_\
                `---'


*/


int start() {

    qDebug() << "\n - - - - - - PluriNotes - - - - - -\n\n";

	
	
    return 0;
}





/* * * * * * * * * * * * * * * * * * * * * * * * */
/*                                               */
/*                 FONCTION QT                   */
/*                                               */
/* * * * * * * * * * * * * * * * * * * * * * * * */


int main(int argc, char *argv[])
{

	
    QApplication a(argc, argv);
	
	/*ArticleFactory().create("idnote1");
	
	ArticleFactory().create("idnoe1");
	
	ArticleFactory().create("idno1");
	
	ArticleFactory().create("idne1");
	
	ArticleFactory().create("ite1");
	
	
	NotesManager::getInstance().deleteNote("idnote1");
*/
	NotesWindow f;
	f.show();
	
    return a.exec();
}
