#include "relationsmanager_widget.h"
#include "ui_relationsmanager_widget.h"

#include <QDesktopWidget>
#include <QShortcut>

#include "notes_manager.h"
#include "Commands/addcouple_command.h"
#include "Commands/addrelation_command.h"
#include "Commands/removecouple_command.h"
#include "Commands/removerelation_command.h"
#include "GUI/add_couple_dialog.h"
#include "Commands/removerelation_command.h"

#include "GUI/new_relation_dialog.h"

RelationsManagerWidget::RelationsManagerWidget(QUndoStack& stack, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RelationsManagerWidget),
    commands(stack)
{
	ui->setupUi(this);
	updateList();
	
	changeRelation("REFERENCES");
	
	connect(ui->relationCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(changeRelation(QString)));
	connect(ui->titleEdit, SIGNAL(textEdited(QString)), this, SLOT(titleChanged(QString)));
	connect(ui->descEdit, SIGNAL(textChanged()), this, SLOT(descChanged()));
	connect(ui->orientedCheck, SIGNAL(toggled(bool)), this, SLOT(orientedChanged(bool)));
	
	connect(ui->deleteRelButton, SIGNAL(pressed()), this, SLOT(removeRelation()));
	connect(ui->addRelButton, SIGNAL(pressed()), this, SLOT(createRelation()));
	connect(ui->addCoupleButton, SIGNAL(pressed()), this, SLOT(createCouple()));
	connect(ui->deleteCoupleButton, SIGNAL(pressed()), this, SLOT(removeCurrentCouple()));
	
	QRegExp rx("[ a-zA-Z_-]*");
	QValidator *validator = new QRegExpValidator(rx, this);
	ui->titleEdit->setValidator(validator);
	
	QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width()-this->width()) / 2;
    int y = (screenGeometry.height()-this->height()) / 2;
    this->move(x, y);
	
	QShortcut *s = new QShortcut(QKeySequence("Ctrl+Z"), this);
	connect(s, SIGNAL(activated()), this, SLOT(undo()));
	        
	QShortcut *s1 = new QShortcut(QKeySequence("Ctrl+Y"), this);
	connect(s1, SIGNAL(activated()), this, SLOT(redo()));
}

RelationsManagerWidget::~RelationsManagerWidget()
{
	delete ui;
}
Relation* RelationsManagerWidget::getRelation(QString s) {
	if(s == "REFERENCES") {
		return NotesManager::getInstance().getRelationsManager().getReference();
	} else {
		return NotesManager::getInstance().getRelationsManager().getRelation(s.toStdString());
	}
}

void RelationsManagerWidget::updateList() {
	NotesManager& nm = NotesManager::getInstance();
	
	std::vector<Relation*> rel = nm.getRelationsManager().getRelations();
	ui->relationCombo->clear();
	ui->relationCombo->addItem("REFERENCES");
	for(auto it = rel.begin(); it != rel.end(); it++) {
		ui->relationCombo->addItem(QString::fromStdString((*it)->getTitle()));
	}
}

void RelationsManagerWidget::updateCouples() {
	ui->treeWidget->clear();
	Relation* r = getRelation(ui->relationCombo->currentText());
	std::vector<Couple*> l = r->getCouples();
	for(auto it = l.begin(); it != l.end(); it++) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
		item->setText(0, QString::fromStdString((*it)->getNoteX()));
		item->setText(1, QString::fromStdString((*it)->getNoteY()));
		item->setText(2, QString::fromStdString((*it)->getLabel()));
	}
}

void RelationsManagerWidget::undo() {
	commands.undo();
	updateList();
}


void RelationsManagerWidget::redo() {
	commands.redo();
	updateList();
}

void RelationsManagerWidget::changeRelation(QString s) {
	ui->treeWidget->clear();
	if(s != "") {
		Relation* r = getRelation(s);
		ui->titleEdit->setText(QString::fromStdString(r->getTitle()));
		ui->descEdit->setText(QString::fromStdString(r->getDescription()));
		ui->orientedCheck->setChecked(r->isOriented());
		if(s == "REFERENCES") {
			ui->titleEdit->setDisabled(true);
			ui->descEdit->setDisabled(true);
			ui->deleteRelButton->setDisabled(true);
			ui->orientedCheck->setDisabled(true);
			ui->addCoupleButton->setDisabled(true);
			ui->deleteCoupleButton->setDisabled(true);
		} else {
			ui->titleEdit->setDisabled(false);
			ui->descEdit->setDisabled(false);
			ui->deleteRelButton->setDisabled(false);
			ui->orientedCheck->setDisabled(false);
			ui->addCoupleButton->setDisabled(false);
			ui->deleteCoupleButton->setDisabled(false);
		}
		updateCouples();
		
		
		
	}
	
	
}

void RelationsManagerWidget::titleChanged(QString s) {
	if(s=="") {
		s="_";
		ui->titleEdit->setText(s);
	}
	Relation *r = getRelation(ui->relationCombo->currentText());
	r->setTitle(s.toStdString());
	int i = ui->relationCombo->currentIndex();
	updateList();
	ui->relationCombo->setCurrentIndex(i);
	ui->titleEdit->setFocus();
}

void RelationsManagerWidget::descChanged() {
	Relation *r = getRelation(ui->relationCombo->currentText());
	r->setDescription(ui->descEdit->toPlainText().toStdString());
}

void RelationsManagerWidget::orientedChanged(bool b) {
	Relation *r = getRelation(ui->relationCombo->currentText());
	r->setOriented(b);
}




void RelationsManagerWidget::createRelation() {
	NewRelationDialog d;
	connect(&d, SIGNAL(relationCreated(Relation*)), this, SLOT(addNewRelation(Relation*)));
	d.exec();
}

void RelationsManagerWidget::addNewRelation(Relation* r) {
	commands.push(new AddRelationCommand(r));
	updateList();
	ui->relationCombo->setCurrentText(QString::fromStdString(r->getTitle()));
}

void RelationsManagerWidget::createCouple() {
	AddCoupleDialog d;
	connect(&d, SIGNAL(coupleCreated(Couple)), this, SLOT(addNewCouple(Couple)));
	d.exec();
}

void RelationsManagerWidget::addNewCouple(Couple c) {
	commands.push(new AddCoupleCommand(getRelation(ui->relationCombo->currentText()),c));
	updateCouples();
}

void RelationsManagerWidget::removeRelation() {
	Relation* r = NotesManager::getInstance().getRelationsManager().getRelation(ui->relationCombo->currentText().toStdString());
	commands.push(new RemoveRelationCommand(r));
	updateList();
}

void RelationsManagerWidget::removeCurrentCouple() {
	QList<QTreeWidgetItem*> l = ui->treeWidget->selectedItems();
	if(l.isEmpty())
		return;
	
	
	Relation *r = getRelation(ui->relationCombo->currentText());
	commands.push(new RemoveCoupleCommand(r, Couple(l.first()->text(0).toStdString(), l.first()->text(1).toStdString())));
	updateCouples();
}
