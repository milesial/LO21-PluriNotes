#-------------------------------------------------
#
# Project created by QtCreator 2017-05-18T01:29:55
#
#-------------------------------------------------

QT       += core gui
QT += xml
QT += multimedia
QT += multimediawidgets
CONFIG += c++
CONFIG += console
CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PluriNotes
TEMPLATE = app

SOURCES += main.cpp \
	GUI/new_note_dialog.cpp \
    GUI/note_widget.cpp \
    GUI/article_widget.cpp \
	note.cpp \
    GUI/task_widget.cpp \
    GUI/notes_window.cpp \
    GUI/medianote_widget.cpp \
    GUI/imagenote_widget.cpp \
    GUI/videonote_widget.cpp \
    GUI/audionote_widget.cpp \
    note_factory.cpp \
	notes_manager.cpp \
    relation.cpp \
    memento.cpp \
    Commands/deletenote_command.cpp \
    Commands/deleteversion_command.cpp \
    GUI/noteslist_widget.cpp \
    GUI/tasklist_widget.cpp \
    Commands/archivenote_command.cpp \
    Commands/restorearchive_command.cpp \
    Commands/restorefrombin_command.cpp \
    Commands/restoreversion_command.cpp \
    GUI/new_relation_dialog.cpp \
    Commands/addrelation_command.cpp \
    GUI/add_couple_dialog.cpp \
    Commands/addcouple_command.cpp \
    Commands/removerelation_command.cpp \
    Commands/removecouple_command.cpp \
    GUI/relationlist_widget.cpp \
    GUI/relationsmanager_widget.cpp

HEADERS  += \
	GUI/new_note_dialog.h \
    GUI/note_widget.h \
    GUI/article_widget.h \
	note.h \
    GUI/task_widget.h \
    GUI/notes_window.h \
    GUI/medianote_widget.h \
    GUI/imagenote_widget.h \
    GUI/videonote_widget.h \
    GUI/audionote_widget.h \
    notes_exception.h \
    note_factory.h \
	notes_manager.h \
    relation.h \
    memento.h \
    Commands/deletenote_command.h \
    Commands/deleteversion_command.h \
    GUI/noteslist_widget.h \
    GUI/tasklist_widget.h \
    Commands/archivenote_command.h \
    Commands/restorearchive_command.h \
    Commands/restorefrombin_command.h \
    Commands/restoreversion_command.h \
    GUI/new_relation_dialog.h \
    Commands/addrelation_command.h \
    GUI/add_couple_dialog.h \
    Commands/addcouple_command.h \
    Commands/removerelation_command.h \
    Commands/removecouple_command.h \
    GUI/relationlist_widget.h \
    GUI/relationsmanager_widget.h

FORMS    += \
	GUI/new_note_dialog.ui \
    GUI/note_widget.ui \
	GUI/article_widget.ui \
    GUI/task_widget.ui \
    GUI/notes_window.ui \
    GUI/medianote_widget.ui \
    GUI/imagenote_widget.ui \
    GUI/videonote_widget.ui \
    GUI/audionote_widget.ui \
	GUI/tasklist_widget.ui \
    GUI/noteslist_widget.ui \
    GUI/new_relation_dialog.ui \
    GUI/add_couple_dialog.ui \
    GUI/relationlist_widget.ui \
    GUI/relationsmanager_widget.ui

RESOURCES += \
    ressources.qrc

DISTFILES +=
