#ifndef MEDIANOTE_WIDGET_H
#define MEDIANOTE_WIDGET_H

#include <QWidget>
#include "note_widget.h"
class MediaNote;

namespace Ui {
class MediaNoteWidget;
}

/*!
 * \brief La classe abstraite MediaNoteWidget est un QWidget qui affiche le contenu d'une MediaNote.
 */
class MediaNoteWidget : public NoteWidget
{
	Q_OBJECT
	
private slots:
	void chooseFile();
	void setMediaNoteDescription();
	
public:
	/*!
	 * \brief Construit un MediaNoteWidget qui affiche une MediaNote avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de la MediaNote associé.
	 * \param mn La MediaNote à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit MediaNoteWidget(MediaNote *mn, QWidget *parent = 0);
	/*!
	 * \brief Construit un MediaNoteWidget qui affiche une MediaNote en mode lecture seule.
	 * \param mn La MediaNote à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit MediaNoteWidget(const MediaNote *mn, QWidget *parent = 0);
	~MediaNoteWidget();
	//! Joue le média.
	virtual void play()=0;
	//! Pause le média.
	virtual void stop()=0;
	//! @return Si le média est en lecture ou non.
	bool isPlaying() const { return playing; }
	
private:
	Ui::MediaNoteWidget *ui;
	
protected:
	//! @return Le widget vide central.
	QWidget* getContainer();
	//! L'état du media : en lecture ou non.
	bool playing;
	//! Le filtre des fichiers valides.
	QString nameFilter;
	//! Met à jour le widget pour afficher le nouveau média.
	virtual void onFileChange(QString s)=0;
	//! Initialise les champs du widget pour afficher une MediaNote.
	void setupFields(const MediaNote *mn);
};

#endif // MEDIANOTE_WIDGET_H
