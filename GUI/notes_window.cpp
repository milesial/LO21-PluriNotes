#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QSplitter>
#include <QShortcut>
#include <QDebug>

#include <cstdlib>
#include <iostream>
#include <string>

#include "notes_window.h"
#include "ui_notes_window.h"

#include "new_note_dialog.h"
#include "Commands/deletenote_command.h"
#include "Commands/archivenote_command.h"
#include "Commands/restorearchive_command.h"
#include "Commands/restorefrombin_command.h"
#include "Commands/deleteversion_command.h"
#include "Commands/restoreversion_command.h"
#include "Commands/addrelation_command.h"
#include "Commands/addcouple_command.h"
#include "GUI/new_relation_dialog.h"
#include "GUI/add_couple_dialog.h"
#include "GUI/relationsmanager_widget.h"


NotesWindow::NotesWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NotesWindow),
    manager(NotesManager::getInstance())
{
	

	ui->setupUi(this);
	
	deleteBinOnExit = false;
	
	//SPLITER ON THE LEFT (notes list + task list)
	QSplitter *s = new QSplitter(ui->centralwidget);
	s->setOrientation(Qt::Orientation::Vertical);
	s->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	list = new NotesListWidget(s);
	ui->leftLayout->addWidget(list);
	taskList = new TaskListWidget(s);
	s->addWidget(list);
	s->addWidget(taskList);
	s->setCollapsible(0, false);
	ui->leftLayout->addWidget(s);

	QSplitter *ss = new QSplitter(ui->centralwidget);
	ss->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
	ss->setOrientation(Qt::Orientation::Horizontal);
	ss->addWidget(ui->left);
	ss->addWidget(ui->center);
	ss->addWidget(ui->right);
	ss->setCollapsible(1, false);
	
	ui->centralwidget->layout()->addWidget(ss);
	
	relationList = new RelationListWidget(ui->relationList);
	
	//INIT BOTTOM BUTTONS
	ui->activeButtons->hide();
	ui->archivedButtons->hide();
	ui->deletedButton->hide();
	
	ui->restoreVersion->hide();
	ui->deleteVersion->hide();
	
	//CLICKS ON THE LEFT LISTS
	connect(list, SIGNAL(activeClicked(QString)), this, SLOT(openActive(QString)));
	connect(list, SIGNAL(archivedClicked(QString)), this, SLOT(openArchived(QString)));
	connect(list, SIGNAL(deletedClicked(QString)), this, SLOT(openDeleted(QString)));
	connect(taskList, SIGNAL(taskClicked(QString)), this, SLOT(openActive(QString)));
	connect(taskList, SIGNAL(taskClicked(QString)), list, SLOT(setSelected(QString)));
	
	//TAB CHANGE
	connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(changeListSelect(int)));
	connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
	
	//BOTTOM BUTTONS (SAVE, DELETE, ...)
	connect(ui->activeSave, SIGNAL(pressed()), this, SLOT(saveCurrent()));
	connect(ui->activeDelete, SIGNAL(pressed()), this, SLOT(deleteActive()));
	connect(ui->activeArchive, SIGNAL(pressed()), this, SLOT(archiveActive()));
	connect(ui->deletedDelete, SIGNAL(pressed()), this, SLOT(deleteDeleted()));
	connect(ui->deletedRestore, SIGNAL(pressed()), this, SLOT(restoreDeleted()));
	connect(ui->archivedRestore, SIGNAL(pressed()), this, SLOT(restoreArchived()));
	connect(ui->archivedDelete, SIGNAL(pressed()), this, SLOT(deleteArchived()));
	
	connect(ui->viewVersion, SIGNAL(pressed()), this, SLOT(openVersion()));
	connect(ui->deleteVersion, SIGNAL(pressed()), this, SLOT(deleteVersion()));
	connect(ui->restoreVersion, SIGNAL(pressed()), this, SLOT(restoreVersion()));
	
	
	//UNDO REDO
	connect(&commands, SIGNAL(indexChanged(int)), this, SLOT(updateUndoRedo(int)));
	
	//TOP MENU
	connect(ui->actionNew_note, SIGNAL(triggered(bool)), this, SLOT(createNote()));
	connect(ui->actionSave_Note, SIGNAL(triggered(bool)), this, SLOT(saveCurrent()));
	connect(ui->actionSave_all, SIGNAL(triggered(bool)), this, SLOT(saveAll()));
	connect(ui->actionClose_note, SIGNAL(triggered(bool)), this, SLOT(closeCurrent()));
	connect(ui->actionClose_all, SIGNAL(triggered(bool)), this, SLOT(closeAll()));
	connect(ui->actionLoad_project, SIGNAL(triggered(bool)), this, SLOT(loadFile()));
	connect(ui->actionSave_project, SIGNAL(triggered(bool)), this, SLOT(saveFile()));
	connect(ui->actionClose_project, SIGNAL(triggered(bool)), this, SLOT(closeProject()));
	connect(ui->actionUndo, SIGNAL(triggered(bool)), this, SLOT(undo()));
	connect(ui->actionRedo, SIGNAL(triggered(bool)), this, SLOT(redo()));
	connect(ui->actionRelations_Manager, SIGNAL(triggered(bool)), this, SLOT(openRelationsManager()));
	
	connect(relationList, SIGNAL(noteClicked(QString)), this, SLOT(openNote(QString)));
	
	//SHORTCUTS
	QShortcut *s1 = new QShortcut(QKeySequence("Ctrl+N"), this);
	connect(s1, SIGNAL(activated()), ui->actionNew_note, SLOT(trigger()));
	
	QShortcut *s2 = new QShortcut(QKeySequence("Ctrl+S"), this);
	connect(s2, SIGNAL(activated()), ui->actionSave_Note, SLOT(trigger()));
	
	QShortcut *s3 = new QShortcut(QKeySequence("Shift+Ctrl+S"), this);
	connect(s3, SIGNAL(activated()), ui->actionSave_all, SLOT(trigger()));
	
	QShortcut *s4 = new QShortcut(QKeySequence("Ctrl+W"), this);
	connect(s4, SIGNAL(activated()), ui->actionClose_note, SLOT(trigger()));
	
	QShortcut *s5 = new QShortcut(QKeySequence("Shift+Ctrl+W"), this);
	connect(s5, SIGNAL(activated()), ui->actionClose_all, SLOT(trigger()));
	
	QShortcut *s6 = new QShortcut(QKeySequence("Ctrl+X"), this);
	connect(s6, SIGNAL(activated()), ui->actionSave_project, SLOT(trigger()));
	
	QShortcut *s7 = new QShortcut(QKeySequence("Ctrl+O"), this);
	connect(s7, SIGNAL(activated()), ui->actionLoad_project, SLOT(trigger()));
	
	QShortcut *s8 = new QShortcut(QKeySequence("Ctrl+Z"), this);
	connect(s8, SIGNAL(activated()), ui->actionUndo, SLOT(trigger()));
	
	QShortcut *s9 = new QShortcut(QKeySequence("Ctrl+Y"), this);
	connect(s9, SIGNAL(activated()), ui->actionRedo, SLOT(trigger()));
	
	QShortcut *s10 = new QShortcut(QKeySequence("Ctrl+Q"), this);
	connect(s10, SIGNAL(activated()), ui->actionClose_project, SLOT(trigger()));
	
	
	
	QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width()-this->width()) / 2;
    int y = (screenGeometry.height()-this->height()) / 2;
    this->move(x, y);
	
	
	try {
		loadContext();
	} catch(NotesException& e) {
		QMessageBox msgBox;
		msgBox.setText("Error : Can't load context (first time running the app ?)");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}

//ON EXIT
NotesWindow::~NotesWindow()
{
	manager.saveAll();
	
	
	deleteBinOnExit = ui->actionClearOnExit->isChecked();
	
	saveContext();
	
	if(deleteBinOnExit)
		manager.clearBin();
	else {
		int n = manager.getDeletedCount();
		if(n > 0) {
			QMessageBox msgBox;
			msgBox.setText("There are "+QString::number(n)+" notes in the recycle bin.");
			msgBox.setInformativeText("Do you want to delete them forever ?");
			msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();
			if(ret == QMessageBox::Yes)
				manager.clearBin();
		}
	}
	
	
	
	if(manager.getActiveCount() > 0 || manager.getArchivedCount() > 0 || manager.getDeletedCount() > 0)
		saveFile();
	
	delete ui;
	
	
	
}

//CHANGE SELECTED ITEM ON THE LEFT ACCORDING TO SELECTED TAB
void NotesWindow::changeListSelect(int i) {
	if(ui->tabWidget->count() > 0) {
		QString id = QString::fromStdString(dynamic_cast<NoteWidget*>(ui->tabWidget->widget(i))->getId());
		if(manager.hasNote(id.toStdString())) {
			list->setSelected(id);
			updateVersionList(id);
		}
	} else {
		ui->versionTree->clear();
		relationList->clear();
	}
}

//CREATE A NEW NOTE DIALOG
void NotesWindow::createNote() {
	NewNoteDialog d;
	connect(&d, SIGNAL(noteCreated(QString)), this, SLOT(changeTabSelect(QString)));
	d.exec();
	taskList->update();
}

//CHANGE TAB SELECTION BASED ON ID
void NotesWindow::changeTabSelect(QString s) {
	openActive(s);
	list->update();
	list->setSelected(s);
	
}

//OPEN ACTIVE NOTE IN THE MIDDLE
void NotesWindow::openActive(QString id) {
	
	NoteWidget* w = manager.getActiveNote(id.toStdString())->getWidget();
	
	ui->activeButtons->show();
	ui->archivedButtons->hide();
	ui->deletedButton->hide();
	ui->restoreVersion->show();
	ui->deleteVersion->show();
	
	for(int i = 0; i < ui->tabWidget->count(); i++) {
		if(dynamic_cast<NoteWidget*>(ui->tabWidget->widget(i))->getId() == id.toStdString()) {
			ui->tabWidget->setCurrentIndex(i);
			ui->activeSave->setEnabled(manager.isActive(id.toStdString()) && !manager.isSaved(id.toStdString()));
			updateVersionList(id);
			relationList->update(id);
			return;
		}
	}
	
	connect(w, SIGNAL(noteModified(QString)), this, SLOT(enableSave()));
	connect(w, SIGNAL(noteModified(QString)), taskList, SLOT(update()));
	connect(w, SIGNAL(noteModified(QString)), list, SLOT(updateTitles()));
	
	//add new tab
	ui->tabWidget->addTab(w, id);
	ui->tabWidget->setCurrentWidget(w);
	if(manager.isActive(id.toStdString()) && !manager.isSaved(id.toStdString()))
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), ui->tabWidget->tabText(ui->tabWidget->currentIndex()) + " *");
	
	
	updateVersionList(id);
	relationList->update(id);
	
}


void NotesWindow::enableSave() {
	ui->activeSave->setDisabled(false);
	QString currText = ui->tabWidget->tabText(ui->tabWidget->currentIndex());
	if(!currText.endsWith(" *"))
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), currText + " *");
	
	ui->tabWidget->currentIndex();
}

void NotesWindow::saveCurrent() {
	if(ui->tabWidget->count()==0)
		return;
	
	
	QSet<QString> notRefedArchives;
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); it++) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0)
			notRefedArchives.insert(QString::fromStdString((*it)->getId()));
	}
	
	
	QString currText = ui->tabWidget->tabText(ui->tabWidget->currentIndex());
	if(currText.endsWith(" *")) {
		currText.chop(2);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), currText);
	}
	std::string id = (dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget()))->getId();
	if(!manager.isActive(id))
		return;
	
	manager.save(id);
	
	
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); ) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0
		   && !notRefedArchives.contains(QString::fromStdString((*it)->getId()))) {
			QMessageBox msgBox;
			msgBox.setText("The last references to the archived note '"+QString::fromStdString((*it)->getId())+"' has been deleted.");
			msgBox.setInformativeText("Do you want to delete the note ?");
			msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();
			if(ret == QMessageBox::Yes) {
				openArchived(QString::fromStdString((*it)->getId()));
				
				manager.deleteFromArchive((*it)->getId());
				updateTabs();
				list->update();
				
			} else
				it++;
			
		} else
			it++;
	}
	
	updateVersionList(QString::fromStdString(id));
	ui->activeSave->setDisabled(true);
	relationList->update(QString::fromStdString(id));
	
	
	
	
}

void NotesWindow::saveAll() {
	QSet<QString> notRefedArchives;
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); it++) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0)
			notRefedArchives.insert(QString::fromStdString((*it)->getId()));
	}
	
	manager.saveAll();
	for(int i = 0; i < ui->tabWidget->count(); i++) {
		QString currText = ui->tabWidget->tabText(i);
		if(currText.endsWith(" *")) {
			currText.chop(2);
			ui->tabWidget->setTabText(i, currText);
		}
	}
	
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); ) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0
		   && !notRefedArchives.contains(QString::fromStdString((*it)->getId()))) {
			QMessageBox msgBox;
			msgBox.setText("The last references to the archived note '"+QString::fromStdString((*it)->getId())+"' has been deleted.");
			msgBox.setInformativeText("Do you want to delete the note ?");
			msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();
			if(ret == QMessageBox::Yes) {
				openArchived(QString::fromStdString((*it)->getId()));
				
				manager.deleteFromArchive((*it)->getId());
				updateTabs();
				list->update();
				
			} else
				it++;
			
		} else
			it++;
	}
	
	
	if(ui->activeSave->isEnabled())
		ui->activeSave->setEnabled(false);
	if(ui->tabWidget->count() > 0) {
		std::string id = (dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget()))->getId();
		if(manager.isActive(id))
			updateVersionList(QString::fromStdString(id));
	}
	
	
	
	saveCurrent();
	
	
	
}


void NotesWindow::openArchived(QString id) {
	NoteWidget* w = manager.getArchivedNote(id.toStdString())->getWidget();
	
	ui->activeButtons->hide();
	ui->archivedButtons->show();
	ui->deletedButton->hide();
	ui->restoreVersion->hide();
	ui->deleteVersion->hide();
	
	for(int i = 0; i < ui->tabWidget->count(); i++) {
		if(ui->tabWidget->tabText(i) == id) {
			ui->tabWidget->setCurrentIndex(i);
			return;
		}
	}
	
	ui->tabWidget->addTab(w, id);
	ui->tabWidget->setCurrentWidget(w);
	
	
}


void NotesWindow::openDeleted(QString id) {
	
	NoteWidget* w = manager.getDeletedNote(id.toStdString())->getWidget();
	
	ui->activeButtons->hide();
	ui->archivedButtons->hide();
	ui->deletedButton->show();
	ui->restoreVersion->hide();
	ui->deleteVersion->hide();
	
	for(int i = 0; i < ui->tabWidget->count(); i++) {
		if(ui->tabWidget->tabText(i) == id) {
			ui->tabWidget->setCurrentIndex(i);
			return;
		}
	}
	
	ui->tabWidget->addTab(w, id);
	ui->tabWidget->setCurrentWidget(w);
	
	
}



void NotesWindow::closeTab(int i) {
	ui->tabWidget->removeTab(i);
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
	
}

void NotesWindow::closeCurrent() {
	if(ui->tabWidget->count()==0)
		return;
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
}

void NotesWindow::closeAll() {
	ui->tabWidget->clear();
	ui->activeButtons->hide();
	ui->archivedButtons->hide();
	ui->deletedButton->hide();
}


void NotesWindow::deleteActive() {
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	commands.push(new DeleteNoteCommand(id));
	
	list->update();
	list->setSelected(QString::fromStdString(id));
	taskList->update();
	
	if(manager.isArchived(id)) {
		QMessageBox msgBox;
		msgBox.setText("This note is references by other notes, it has been archived.");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
}


void NotesWindow::archiveActive() {
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	commands.push(new ArchiveNoteCommand(id));
	
	list->update();
	list->setSelected(QString::fromStdString(id));
	taskList->update();
}

void NotesWindow::deleteDeleted() {
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	QSet<QString> notRefedArchives;
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); it++) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0)
			notRefedArchives.insert(QString::fromStdString((*it)->getId()));
	}
	
	QMessageBox msgBox;
	msgBox.setText("The note '"+QString::fromStdString(id)+"' and all its relations will be deleted forever.");
	msgBox.setInformativeText("Do you want to continue ?");
	msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Cancel);
	int ret = msgBox.exec();
	if(ret == QMessageBox::Yes) {
		manager.deleteFromBin(id);
		ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
		list->update();
		
	}
	
	
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); ) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0
		   && !notRefedArchives.contains(QString::fromStdString((*it)->getId()))) {
			QMessageBox msgBox;
			msgBox.setText("The last references to the archived note '"+QString::fromStdString((*it)->getId())+"' has been deleted.");
			msgBox.setInformativeText("Do you want to delete the note ?");
			msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();
			if(ret == QMessageBox::Yes) {
				openArchived(QString::fromStdString((*it)->getId()));
				
				manager.deleteFromArchive((*it)->getId());
				updateTabs();
				list->update();
				
			} else
				it++;
			
		} else
			it++;
	}
	
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
}

void NotesWindow::restoreDeleted() {
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	commands.push(new RestoreFromBinCommand(id));
	
	list->update();
	list->setSelected(QString::fromStdString(id));
	taskList->update();
}

void NotesWindow::restoreArchived() {
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	commands.push(new RestoreArchiveCommand(id));
	
	list->update();
	list->setSelected(QString::fromStdString(id));
	taskList->update();
}

void NotesWindow::deleteArchived() {
	
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	
	QSet<QString> notRefedArchives;
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); it++) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0)
			notRefedArchives.insert(QString::fromStdString((*it)->getId()));
	}
	
	
	try {
		QMessageBox msgBox;
		msgBox.setText("The note '"+QString::fromStdString(id)+"' and all its relations will be deleted forever.");
		msgBox.setInformativeText("Do you want to continue ?");
		msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Yes);
		msgBox.setDefaultButton(QMessageBox::Cancel);
		int ret = msgBox.exec();
		if(ret == QMessageBox::Yes) {
			manager.deleteFromArchive(id);
			ui->tabWidget->removeTab(ui->tabWidget->currentIndex());	
			list->update();
			taskList->update();
		}
	} catch(NotesException& e) {
		QMessageBox msgBox;
		msgBox.setText("The note '"+QString::fromStdString(id)+"' still has references and can't be deleted.");
		msgBox.setStandardButtons(QMessageBox::Ok);
		msgBox.setDefaultButton(QMessageBox::Ok);
		msgBox.exec();
	}
	
	
	
	for(auto it = manager.beginArchived(); it != manager.endArchived(); ) {
		if(manager.getRelationsManager().getReference()->getAscendants((*it)->getId()).size() == 0
		   && !notRefedArchives.contains(QString::fromStdString((*it)->getId()))) {
			QMessageBox msgBox;
			msgBox.setText("The last references to the archived note '"+QString::fromStdString((*it)->getId())+"' has been deleted.");
			msgBox.setInformativeText("Do you want to delete the note ?");
			msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();
			if(ret == QMessageBox::Yes) {
				openArchived(QString::fromStdString((*it)->getId()));
				
				manager.deleteFromArchive((*it)->getId());
				updateTabs();
				list->update();
				
			} else
				it++;
			
		} else
			it++;
	}
	
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
	
	
	
}


void NotesWindow::loadFile() {
	QFileDialog dialog(this);
	dialog.setWindowTitle("Please select the project file");
	dialog.setNameFilter("XML files (*.xml)");
	dialog.setFileMode(QFileDialog::ExistingFile);
	if(dialog.exec()) {
		workspace = dialog.selectedFiles().at(0);
		manager.clear();
		commands.clear();
		manager.loadFromFile(workspace.toStdString());
		list->update();
		taskList->update();
		QString add = workspace;
		add.chop(4);
		this->setWindowTitle("PluriNotes - "+ add.split("/")[add.split("/").length()-1]);
	}
}

void NotesWindow::saveFile() {
	if(workspace == "") {
		QFileDialog dialog(this);
		dialog.setNameFilter("XML files (*.xml)");
		dialog.setFileMode(QFileDialog::AnyFile);
		dialog.setAcceptMode(QFileDialog::AcceptSave);
		dialog.setWindowTitle("Please choose where to save the project");
		if(dialog.exec()) {
			workspace = dialog.selectedFiles().at(0);
			if(!workspace.endsWith(".xml"))
				workspace += ".xml";
			
			saveAll();
			manager.saveToFile(workspace.toStdString());
			QString add = workspace;
			add.chop(4);
			this->setWindowTitle("PluriNotes - "+ add.split("/")[add.split("/").length()-1]);
		}
	} else {
		saveAll();
		manager.saveToFile(workspace.toStdString());
	}
	
	
}

 
void NotesWindow::loadContext() {
	QDomDocument doc;
	QFile file("context.xml");
		
	if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
		throw NotesException("Can't open context file ");
	
	if(!doc.setContent(&file)) {
		file.close();
		throw NotesException("Can't parse context file");
	}
		
	file.close();
	
	QDomElement root = doc.firstChildElement();
	
	workspace = root.firstChildElement("workspace").text();
	manager.clear();
	commands.clear();
	manager.loadFromFile(workspace.toStdString());
	list->update();
	taskList->update();
	QString add = workspace;
	add.chop(4);
	this->setWindowTitle("PluriNotes - "+ add.split("/")[add.split("/").length()-1]);
	
	deleteBinOnExit = root.firstChildElement("clearbin").text()=="true"?true:false;
	
	ui->actionClearOnExit->setChecked(deleteBinOnExit);
}

void NotesWindow::saveContext() {
	QDomDocument doc;
	
	QDomElement root = doc.createElement("context");
	
	QDomElement e = doc.createElement("workspace");
	e.appendChild(doc.createTextNode(workspace));
	root.appendChild(e);
	
	e = doc.createElement("clearbin");
	e.appendChild(doc.createTextNode(deleteBinOnExit?"true":"false"));
	root.appendChild(e);
	
	doc.appendChild(root);
	
	//write to file
	QFile f("context.xml");
	if(f.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream stream(&f);
		doc.save(stream, QDomNode::CDATASectionNode);
	} else {
		throw NotesException("Can't write save context");
	}
	
	f.close();
}

void NotesWindow::undo() {
	commands.undo();
	list->update();
	taskList->update();
	updateTabs();
	if(ui->tabWidget->count() > 0) {
		std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
		updateVersionList(QString::fromStdString(id));
		if(manager.isActive(id)) {
			ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
			openActive(QString::fromStdString(id));
		}
	}
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
}

void NotesWindow::redo() {
	commands.redo();
	list->update();
	taskList->update();
	updateTabs();
	if(ui->tabWidget->count() > 0) {
		std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
		updateVersionList(QString::fromStdString(id));
		if(manager.isActive(id)) {
			ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
			openActive(QString::fromStdString(id));
		}
	}
	if(ui->tabWidget->count() == 0) {
		ui->activeButtons->hide();
		ui->archivedButtons->hide();
		ui->deletedButton->hide();
	}
}

void NotesWindow::updateTabs() {
	for(int i = 0; i < ui->tabWidget->count(); i++) {
		NoteWidget *w = dynamic_cast<NoteWidget*>(ui->tabWidget->widget(i));
		std::string id = w->getId();
		if(!manager.hasNote(id) || 
		        (w->isReadOnly() && manager.isActive(id)) || 
		        (!w->isReadOnly() && manager.isArchived(id)) || 
		         (!w->isReadOnly() && manager.isDeleted(id))) {
			ui->tabWidget->removeTab(i);
		}
	}
}

void NotesWindow::updateUndoRedo(int) {
	if(commands.canUndo()) {
		ui->actionUndo->setDisabled(false);
		ui->actionUndo->setText("Undo '"+ commands.undoText() +"'");
	} else {
		ui->actionUndo->setDisabled(true);
		ui->actionUndo->setText("Undo");
	}
	
	if(commands.canRedo()) {
		ui->actionRedo->setDisabled(false);
		ui->actionRedo->setText("Redo '"+ commands.redoText() +"'");
	} else {
		ui->actionRedo->setDisabled(true);
		ui->actionRedo->setText("Redo");
	}
}

void NotesWindow::updateVersionList(QString id) {
	const QString dateFormat = "dd'/'MM'/'yy' - 'hh:mm:ss";
	QLocale locale  = QLocale(QLocale::English, QLocale::UnitedStates);
	
	ui->versionTree->clear();
	
	
	const VersionNote *vn = manager.getVersion(id.toStdString());
	std::vector<Memento*> l = vn->getList();
	for(int i = l.size()-1; i >= 0; i--) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->versionTree);
		item->setText(0, QString::number(i));
		QDateTime date = QDateTime::fromTime_t(l[i]->getState()->getDateModified());
		item->setText(1, locale.toString(date, dateFormat));
		item->setTextAlignment(0, Qt::AlignHCenter);
		item->setTextAlignment(1, Qt::AlignHCenter);
		ui->versionTree->addTopLevelItem(item);
	}
	
}

void NotesWindow::openVersion() {
	if(ui->versionTree->selectedItems().isEmpty())
		return;
	QTreeWidgetItem *i = ui->versionTree->currentItem();
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	NoteWidget* w = manager.getVersion(id)->getList()[i->text(0).toInt()]->getState()->getWidget();
	
	
	w->setWindowTitle(QString::fromStdString(id)+" - Version "+ i->text(0));
	QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width()-w->width()) / 2;
    int y = (screenGeometry.height()-w->height()) / 2;
    w->move(x, y);
	w->show();
}


void NotesWindow::deleteVersion() {
	if(ui->versionTree->selectedItems().isEmpty())
		return;
	QTreeWidgetItem *i = ui->versionTree->currentItem();
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	commands.push(new DeleteVersionCommand(manager.getVersion(id)->getList()[i->text(0).toInt()]));
	
	updateVersionList(QString::fromStdString(id));
}

void NotesWindow::restoreVersion() {
	if(ui->versionTree->selectedItems().isEmpty())
		return;
	QTreeWidgetItem *i = ui->versionTree->currentItem();
	std::string id = dynamic_cast<NoteWidget*>(ui->tabWidget->currentWidget())->getId();
	
	commands.push(new RestoreVersionCommand(manager.getVersion(id)->getList()[i->text(0).toInt()]));
	
	updateVersionList(QString::fromStdString(id));
	
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
	openActive(QString::fromStdString(id));
}


void NotesWindow::closeProject() {
	workspace = "";
	manager.clear();
	commands.clear();
	list->update();
	ui->tabWidget->clear();
	taskList->update();
	ui->versionTree->clear();
	ui->archivedButtons->hide();
	ui->activeButtons->hide();
	ui->deletedButton->hide();
	this->setWindowTitle("Plurinotes - New project");
}

void NotesWindow::openRelationsManager() {
	RelationsManagerWidget *w = new RelationsManagerWidget(commands);
	w->show();
}

void NotesWindow::openNote(QString id) {
	if(manager.isActive(id.toStdString()))
		openActive(id);
	else if(manager.isArchived(id.toStdString()))
		openArchived(id);
	else if(manager.isDeleted(id.toStdString()))
		openDeleted(id);
}
