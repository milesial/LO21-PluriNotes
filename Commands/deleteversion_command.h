#ifndef DELETEVERSIONCOMMAND_H
#define DELETEVERSIONCOMMAND_H

#include <QUndoCommand>
#include "memento.h"
/*!
 * \brief La classe DeleteVersionCommand gère la commande de suppression d'une version de note, et son annulation.
 */
class DeleteVersionCommand : public QUndoCommand
{
private:
	Memento* memento;
public:
	/*!
	 * \brief Construit une commande qui se charge de supprimer une version donnée d'une Note active.
	 * \param m Le Memento qui représente l'état de la Note à supprimer.
	 * \param parent La commande mère, si il y en a une.
	 */
	DeleteVersionCommand(Memento *m, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Delete version of  : "+m->getState()->getId()), parent), memento(m) {}
	/*!
	 * \brief Ajoute la version à la liste des versions de la Note.
	 */
	void undo();
	/*!
	 * \brief Supprime la version de la liste des versions de la Note.
	 */
	void redo();
};

#endif // DELETEVERSIONCOMMAND_H
