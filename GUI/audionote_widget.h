#ifndef AUDIONOTE_WIDGET_H
#define AUDIONOTE_WIDGET_H

#include <QWidget>
#include <QMediaPlayer>

#include "medianote_widget.h"

class AudioNote;

namespace Ui {
class AudioNoteWidget;
}
/*!
 * \brief La classe AudioNoteWidget permet d'afficher une AudioNote.
 * Elle permet de jouer et de mettre en pause un fichier audio (.mp3, .wav).
 */
class AudioNoteWidget : public MediaNoteWidget
{
	Q_OBJECT
	
public:
	/*!
	 * \brief Construit un AudioNoteWidget qui affiche une AudioNote avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de l'AudioNote associé.
	 * \param n L'AudioNote à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit AudioNoteWidget(AudioNote *n, QWidget *parent = 0);
	/*!
	 * \brief Construit un AudioNoteWidget qui affiche une AudioNote en mode lecture seule.
	 * \param n L'AudioNote à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit AudioNoteWidget(const AudioNote *n, QWidget *parent = 0);
	~AudioNoteWidget();
	
	/*!
	 * \brief Joue le fichier audio.
	 */
	void play();
	/*!
	 * \brief Pause le fichier audio.
	 */
	void stop();
	
private:
	Ui::AudioNoteWidget *ui;
	QMediaPlayer *player;
	
protected:
	/*!
	 * \brief Initialise tous les champs à partir d'une AudioNote.
	 * \param n L'AudioNote à afficher.
	 */
	void setupFields(const AudioNote *n);
	/*!
	 * \brief Rafraichit le widget pour afficher le nouveau fichier.
	 * \param s Chemin absolu du nouveau fichier.
	 */
	void onFileChange(QString s);
	
private slots:
	void playStop();
	void setSlider(qint32 i);
	void setTime(int i);
};

#endif // AUDIONOTE_WIDGET_H
