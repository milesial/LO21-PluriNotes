#include "note_factory.h"
#include "notes_exception.h"
#include "notes_manager.h"
#include <ctime>


/*
                    _   _       _      ______         _
                   | \ | |     | |     |  ___|       | |
                   |  \| | ___ | |_ ___| |_ __ _  ___| |_ ___  _ __ _   _
                   | . ` |/ _ \| __/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
                   | |\  | (_) | ||  __/ || (_| | (__| || (_) | |  | |_| |
                   \_| \_/\___/ \__\___\_| \__,_|\___|\__\___/|_|   \__, |
                                                                     __/ |
                                                                    |___/
*/


Note* NoteFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();

	QString id = e.firstChildElement("id").text();
	if(id == "") throw BadXmlException();

	QString title = e.firstChildElement("title").text();
	time_t dateCreated = e.firstChildElement("dateCreated").text().toInt();
	time_t dateModified = e.firstChildElement("dateModified").text().toInt();

	Note *n = new Note(id.toStdString());
	n->setTitle(title.toStdString());
	n->setDateCreated(dateCreated);
	n->setDateModified(dateModified);

	return n;
}

bool NoteFactory::isXmlValid(QDomElement e) {
	
	return    !e.firstChildElement("id").isNull()
		   && !e.firstChildElement("dateCreated").isNull()
		   && !e.firstChildElement("dateModified").isNull();
}


/*
                     ___       _   _      _     ______         _
                    / _ \     | | (_)    | |    |  ___|       | |
                   / /_\ \_ __| |_ _  ___| | ___| |_ __ _  ___| |_ ___  _ __ _   _
                   |  _  | '__| __| |/ __| |/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
                   | | | | |  | |_| | (__| |  __/ || (_| | (__| || (_) | |  | |_| |
                   \_| |_/_|   \__|_|\___|_|\___\_| \__,_|\___|\__\___/|_|   \__, |
                                                                              __/ |
                                                                             |___/

*/


Article* ArticleFactory::create(const std::string& id) {
	Article *a = new Article(id);

	NotesManager::getInstance().addNewNote(a);
	return a;
}

bool ArticleFactory::isXmlValid(QDomElement e) {
	return    NoteFactory::isXmlValid(e)
		   && e.tagName() == "article";

}

Article* ArticleFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();

	Note *n = NoteFactory::fromXml(e);
	time_t modif = n->getDateModified();
	Article *a = new Article(*n);

	QString text = e.firstChildElement("text").text();
	a->setText(text.toStdString());

	a->setDateModified(modif);
	return a;
}


/*
                  _____         _   ______         _
                 |_   _|       | |  |  ___|       | |
                   | | __ _ ___| | _| |_ __ _  ___| |_ ___  _ __ _   _
                   | |/ _` / __| |/ /  _/ _` |/ __| __/ _ \| '__| | | |
                   | | (_| \__ \   <| || (_| | (__| || (_) | |  | |_| |
                   \_/\__,_|___/_|\_\_| \__,_|\___|\__\___/|_|   \__, |
                                                                  __/ |
                                                                 |___/

*/


Task* TaskFactory::create(const std::string& id) {
	Task *t = new Task(id);
	NotesManager::getInstance().addNewNote(t);
	return t;
}

bool TaskFactory::isXmlValid(QDomElement e) {
	return    NoteFactory::isXmlValid(e)
		   && e.tagName() == "task"
		   && !e.firstChildElement("priority").isNull()
		   && !e.firstChildElement("deadline").isNull()
		   && !e.firstChildElement("state").isNull();
}

Task* TaskFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();

	Note *n = NoteFactory::fromXml(e);
	
	time_t modif = n->getDateModified();
	Task *t = new Task(*n);

	int priority = e.firstChildElement("priority").text().toInt();
	time_t deadline = e.firstChildElement("deadline").text().toInt();
	QString action = e.firstChildElement("action").text();
	int state = e.firstChildElement("state").text().toInt();

	t->setPriority(priority);
	t->setDeadline(deadline);
	t->setAction(action.toStdString());
	t->setState(Task::TaskState(state));
	t->setDateModified(modif);
	return t;
}

/*
                 ___  ___         _ _       _   _       _      ______         _
                 |  \/  |        | (_)     | \ | |     | |     |  ___|       | |
                 | .  . | ___  __| |_  __ _|  \| | ___ | |_ ___| |_ __ _  ___| |_ ___  _ __ _   _
                 | |\/| |/ _ \/ _` | |/ _` | . ` |/ _ \| __/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
                 | |  | |  __/ (_| | | (_| | |\  | (_) | ||  __/ || (_| | (__| || (_) | |  | |_| |
                 \_|  |_/\___|\__,_|_|\__,_\_| \_/\___/ \__\___\_| \__,_|\___|\__\___/|_|   \__, |
                                                                                             __/ |
                                                                                            |___/

*/


bool MediaNoteFactory::isXmlValid(QDomElement e) {
	return    NoteFactory::isXmlValid(e);
}

MediaNote* MediaNoteFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();

	Note *n = NoteFactory::fromXml(e);
	time_t modif = n->getDateModified();
	MediaNote *t = new MediaNote(*n);

	QString filename = e.firstChildElement("filename").text();
	QString description = e.firstChildElement("description").text();

	t->setFilename(filename.toStdString());
	t->setDescription(description.toStdString());
	t->setDateModified(modif);
	return t;
}


/*

               _____                           _   _       _      ______         _                   
              |_   _|                         | \ | |     | |     |  ___|       | |                  
                | | _ __ ___   __ _  __ _  ___|  \| | ___ | |_ ___| |_ __ _  ___| |_ ___  _ __ _   _ 
                | || '_ ` _ \ / _` |/ _` |/ _ \ . ` |/ _ \| __/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
               _| || | | | | | (_| | (_| |  __/ |\  | (_) | ||  __/ || (_| | (__| || (_) | |  | |_| |
               \___/_| |_| |_|\__,_|\__, |\___\_| \_/\___/ \__\___\_| \__,_|\___|\__\___/|_|   \__, |
                                     __/ |                                                      __/ |
                                    |___/                                                      |___/
*/






ImageNote* ImageNoteFactory::create(const std::string& id) {
	ImageNote *m = new ImageNote(id);
	NotesManager::getInstance().addNewNote(m);
	return m;
}

bool ImageNoteFactory::isXmlValid(QDomElement e) {
	return    MediaNoteFactory::isXmlValid(e)
	       && e.tagName() == "imagenote";
}

ImageNote* ImageNoteFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();
	
	MediaNote *t = MediaNoteFactory::fromXml(e);
	time_t modif = t->getDateModified();
	
	ImageNote *n = new ImageNote(*t);
	n->setFilename(t->getFilename());
	n->setDescription(t->getDescription());
	n->setDateModified(modif);
	

	return n;
}

/*
                ___            _ _       _   _       _      ______         _                   
               / _ \          | (_)     | \ | |     | |     |  ___|       | |                  
              / /_\ \_   _  __| |_  ___ |  \| | ___ | |_ ___| |_ __ _  ___| |_ ___  _ __ _   _ 
              |  _  | | | |/ _` | |/ _ \| . ` |/ _ \| __/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
              | | | | |_| | (_| | | (_) | |\  | (_) | ||  __/ || (_| | (__| || (_) | |  | |_| |
              \_| |_/\__,_|\__,_|_|\___/\_| \_/\___/ \__\___\_| \__,_|\___|\__\___/|_|   \__, |
                                                                                          __/ |
                                                                                         |___/

*/
AudioNote* AudioNoteFactory::create(const std::string& id) {
	AudioNote *m = new AudioNote(id);
	NotesManager::getInstance().addNewNote(m);
	return m;
}

bool AudioNoteFactory::isXmlValid(QDomElement e) {
	return    MediaNoteFactory::isXmlValid(e)
	       && e.tagName() == "audionote";
}

AudioNote* AudioNoteFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();
	
	MediaNote *t = MediaNoteFactory::fromXml(e);
	time_t modif = t->getDateModified();
	
	AudioNote *n = new AudioNote(*t);
	n->setFilename(t->getFilename());
	n->setDescription(t->getDescription());
	
	n->setDateModified(modif);

	return n;
}

/*
              _   _ _     _            _   _       _      ______         _                   
             | | | (_)   | |          | \ | |     | |     |  ___|       | |                  
             | | | |_  __| | ___  ___ |  \| | ___ | |_ ___| |_ __ _  ___| |_ ___  _ __ _   _ 
             | | | | |/ _` |/ _ \/ _ \| . ` |/ _ \| __/ _ \  _/ _` |/ __| __/ _ \| '__| | | |
             \ \_/ / | (_| |  __/ (_) | |\  | (_) | ||  __/ || (_| | (__| || (_) | |  | |_| |
              \___/|_|\__,_|\___|\___/\_| \_/\___/ \__\___\_| \__,_|\___|\__\___/|_|   \__, |
                                                                                        __/ |
                                                                                       |___/ 

*/

VideoNote* VideoNoteFactory::create(const std::string& id) {
	VideoNote *m = new VideoNote(id);
	NotesManager::getInstance().addNewNote(m);
	return m;
}

bool VideoNoteFactory::isXmlValid(QDomElement e) {
	return    MediaNoteFactory::isXmlValid(e)
	       && e.tagName() == "videonote";
}

VideoNote* VideoNoteFactory::fromXml(QDomElement e) {
	if(!isXmlValid(e)) throw BadXmlException();
	
	MediaNote *t = MediaNoteFactory::fromXml(e);
	time_t modif = t->getDateModified();
	VideoNote *n = new VideoNote(*t);
	n->setFilename(t->getFilename());
	n->setDescription(t->getDescription());
	n->setDateModified(modif);

	return n;
}





