#ifndef IMAGENOTE_WIDGET_H
#define IMAGENOTE_WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include "medianote_widget.h"

class ImageNote;


namespace Ui {
class ImageNoteWidget;
}
/*!
 * \brief La classe ImageNoteWidget permet d'afficher une ImageNote.
 * Elle permet d'afficher un fichier image (.png, .jpg, .jpeg).
 */
class ImageNoteWidget : public MediaNoteWidget
{
	Q_OBJECT
public:
	/*!
	 * \brief Construit un ImageNoteWidget qui affiche une ImageNote avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de l'ImageNote associé.
	 * \param n L'ImageNote à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit ImageNoteWidget(ImageNote *n, QWidget *parent = 0);
	/*!
	 * \brief Construit un ImageNoteWidget qui affiche une ImageNote en mode lecture seule.
	 * \param n L'ImageNote à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit ImageNoteWidget(const ImageNote *n, QWidget *parent = 0);
	~ImageNoteWidget();
	/*!
	 * \brief Joue le fichier image, donc ne fait rien.
	 */
	void play() {}
	/*!
	 * \brief Pause le fichier image, donc ne fait rien.
	 */
	void stop() {}
	
private:
	Ui::ImageNoteWidget *ui;
	QGraphicsScene *scene;
	QGraphicsPixmapItem *image;
	
protected:
	/*!
	 * \brief Rafraichit le widget pour afficher le nouveau fichier.
	 * \param s Chemin absolu du nouveau fichier.
	 */
	void onFileChange(QString s);
	/*!
	 * \brief Initialise tous les champs à partir d'une ImageNote.
	 * \param n L'ImageNote à afficher.
	 */
	void setupFields(const ImageNote *n);
	/*!
	 * \brief Rafraichit le widget pour afficher l'image.
	 */
	void resizeEvent(QResizeEvent *);
	/*!
	 * \brief Rafraichit le widget pour afficher l'image.
	 */
	void showEvent(QShowEvent *);	
	
	
};

#endif // IMAGENOTE_WIDGET_H
