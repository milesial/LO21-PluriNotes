#include "deleteversion_command.h"
#include "notes_manager.h"

void DeleteVersionCommand::undo() {
	if(NotesManager::getInstance().isActive(memento->getState()->getId()))
		NotesManager::getInstance().getActiveVersion(memento->getState()->getId())->addVersion(memento, true);
}

void DeleteVersionCommand::redo() {
	if(NotesManager::getInstance().isActive(memento->getState()->getId()))
		NotesManager::getInstance().getActiveVersion(memento->getState()->getId())->removeVersion(memento);
}


