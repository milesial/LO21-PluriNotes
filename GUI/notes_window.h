#ifndef NOTES_WINDOW_H
#define NOTES_WINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include "note.h"
#include "notes_manager.h"
#include "noteslist_widget.h"
#include "tasklist_widget.h"
#include "relationlist_widget.h"

namespace Ui {
class NotesWindow;
}

/*!
 * \brief La classe NotesWindow est la fenetre principale de l'application.
 * Elle affiche et permet de gérer les notes et les relations du NotesManager.
 */
class NotesWindow : public QMainWindow
{
	Q_OBJECT

public:
	/*!
	 * \brief Construit la NotesWindow.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit NotesWindow(QWidget *parent = 0);
	~NotesWindow();
	//! Sauvegarde le contexte de l'application dans le fichier 'context.xml'.
	void saveContext();
	//! Charge le contexte de l'application depuis le fichier 'context.xml'.
	void loadContext();
	//! @return Le chemin absolu du projet ouvert.
	std::string getWorkspace() const;
	


private slots:
	void openArchived(QString id);
	void openActive(QString id);
	void openDeleted(QString id);
	void openNote(QString id);
	void openVersion();
	void deleteVersion();
	void restoreVersion();
	void changeListSelect(int i);
	void changeTabSelect(QString s);
	void createNote();
	void closeTab(int i);
	void closeCurrent();
	void closeAll();
	void enableSave();
	void saveCurrent();
	void saveAll();
	void deleteActive();
	void archiveActive();
	void deleteDeleted();
	void restoreDeleted();
	void restoreArchived();
	void deleteArchived();
	void loadFile();
	void saveFile();
	void undo();
	void redo();
	void updateTabs();
	void updateUndoRedo(int);
	void updateVersionList(QString id);
	void closeProject();
	void openRelationsManager();
	
private:
	Ui::NotesWindow *ui;
	
	QString workspace;          //XML file where the project is saved
	NotesManager& manager;
	NotesListWidget *list;      //list of active/archived/deleted notes on the left
	TaskListWidget *taskList;   //list of active tasks on the bottom left
	RelationListWidget *relationList;  //list of versions of the current note, on the top right
	QUndoStack commands;        //stack of commands for undo/redo
	bool deleteBinOnExit;       //if the bin get cleared on exit without warning
	

};

#endif // NOTES_WINDOW_H
