#include <string>
#include <sstream>
#include <ctime>
#include <iostream>

#include "note.h"

/*
                        _        _____ _        _
                       | |      /  ___| |      (_)
                       | |_ ___ \ `--.| |_ _ __ _ _ __   __ _
                       | __/ _ \ `--. \ __| '__| | '_ \ / _` |
                       | || (_) /\__/ / |_| |  | | | | | (_| |
                        \__\___/\____/ \__|_|  |_|_| |_|\__, |
                                                         __/ |
                                                        |___/

*/
std::string Note::toString() const {
	std::stringstream ss;

	ss << " Note :\n\tID: "
	   << id
	   << "\n\n\tTitle: "
	   << title
	   << "\n\n\tCreated: "
	   << ctime(&dateCreated)
	   << "\n\tModified: "
	   << ctime(&dateModified);

	return ss.str();
}

std::string Article::toString() const {
	std::stringstream ss;

	ss << "Article"
	   << Note::toString()
	   << "\n\tText: "
	   << text;

	return ss.str();
}

std::string Task::toString() const {
	std::stringstream ss;
	
	ss << "Task"
	   << Note::toString()
	   << "\n\tAction: "
	   << action
	   << "\n\n\tDeadline: "
	   << ((deadline <= 0)?"No\n":ctime(&deadline))
	   << "\n\tPriority: ";

	if(priority <= 0)
		ss << "No";
	else
		ss << priority;

	std::string stateStr;
	switch(state) {
		case FINISHED:
			stateStr = "FINISHED";
			break;
		case IN_PROGRESS:
			stateStr = "IN_PROGRESS";
			break;
		case WAITING:
			stateStr = "WAITING";
			break;
		default:
			stateStr = "UNKNOWN";
	}


	ss << "\n\n\tState: "
	   << stateStr;



	return ss.str();
}

std::string MediaNote::toString() const {
	std::stringstream ss;

	ss << "MediaNote"
	   << Note::toString()
	   << "\n\tFilename: "
	   << filename
	   << "\n\n\tDescription: "
	   << description;

	return ss.str();
}

std::string ImageNote::toString() const {
	std::stringstream ss;

	ss << "Image "
	   << MediaNote::toString();

	return ss.str();
}


std::string VideoNote::toString() const {
	std::stringstream ss;

	ss << "Video "
	   << MediaNote::toString();

	return ss.str();
}


std::string AudioNote::toString() const {
	std::stringstream ss;

	ss << "Audio "
	   << MediaNote::toString();

	return ss.str();
}

std::ostream& operator<<(std::ostream& os, const Note& note) {

	os << note.toString();
	return os;
}


NoteWidget* Note::getWidget() const {
	NoteWidget *w = new NoteWidget(this);
	return w;
}

NoteWidget* Note::getWidget() {
	NoteWidget *w = new NoteWidget(this);
	return w;
}

ArticleWidget* Article::getWidget() {
	ArticleWidget *w = new ArticleWidget(this);
	return w;
}

ArticleWidget* Article::getWidget() const {
	ArticleWidget *w = new ArticleWidget(this);
	return w;
}

TaskWidget* Task::getWidget() {
	TaskWidget *w = new TaskWidget(this);
	return w;
}

TaskWidget* Task::getWidget() const {
	TaskWidget *w = new TaskWidget(this);
	return w;
}


NoteWidget* MediaNote::getWidget() {
	return Note::getWidget();
}

NoteWidget* MediaNote::getWidget() const {
	return Note::getWidget();
}


ImageNoteWidget* ImageNote::getWidget() {
	ImageNoteWidget *w = new ImageNoteWidget(this);
	return w;
}

ImageNoteWidget* ImageNote::getWidget() const {
	ImageNoteWidget *w = new ImageNoteWidget(this);
	return w;
}

AudioNoteWidget* AudioNote::getWidget() {
	AudioNoteWidget *w = new AudioNoteWidget(this);
	return w;
}

AudioNoteWidget* AudioNote::getWidget() const {
	AudioNoteWidget *w = new AudioNoteWidget(this);
	return w;
}


VideoNoteWidget* VideoNote::getWidget() {
	VideoNoteWidget *w = new VideoNoteWidget(this);
	return w;
}

VideoNoteWidget* VideoNote::getWidget() const {
	VideoNoteWidget *w = new VideoNoteWidget(this);
	return w;
}


/*
                        ___ ___  _ __  _   _
                       / __/ _ \| '_ \| | | |
                      | (_| (_) | |_) | |_| |
                       \___\___/| .__/ \__, |
                                | |     __/ |
                                |_|    |___/
*/



Note* Note::copy(const std::string& newId) const {
	Note *n = new Note(newId);
	n->setTitle(title);
	return n;
}

Article* Article::copy(const std::string& newId) const {
	Article *a = new Article(newId);
	a->setTitle(title);
	a->setText(text);
	return a;
}

Task* Task::copy(const std::string& newId) const {
	Task *t = new Task(newId);
	t->setTitle(title);
	t->setAction(action);
	t->setPriority(priority);
	t->setDeadline(deadline);
	t->setState(state);
	return t;
}

MediaNote* MediaNote::copy(const std::string& newId) const {
	MediaNote *n = new MediaNote(newId);
	n->setTitle(title);
	n->setDescription(description);
	n->setFilename(filename);
	return n;
}

ImageNote* ImageNote::copy(const std::string& newId) const {
	ImageNote *n = new ImageNote(newId);
	n->setTitle(title);
	n->setDescription(description);
	n->setFilename(filename);
	return n;
}

AudioNote* AudioNote::copy(const std::string& newId) const {
	AudioNote *n = new AudioNote(newId);
	n->setTitle(title);
	n->setDescription(description);
	n->setFilename(filename);
	return n;
}

VideoNote* VideoNote::copy(const std::string& newId) const {
	VideoNote *n = new VideoNote(newId);
	n->setTitle(title);
	n->setDescription(description);
	n->setFilename(filename);
	return n;
}

/*
                       _       __   __          _
                      | |      \ \ / /         | |
                      | |_ ___  \ V / _ __ ___ | |
                      | __/ _ \ /   \| '_ ` _ \| |
                      | || (_) / /^\ \ | | | | | |
                       \__\___/\/   \/_| |_| |_|_|
*/



 QDomDocumentFragment Note::toXml() const {
	
	QDomDocument doc;

	QDomDocumentFragment f = doc.createDocumentFragment();
	QDomElement r = doc.createElement("note");
	
	QDomElement n = doc.createElement("id");
	n.appendChild(doc.createTextNode(QString::fromStdString(id)));
	r.appendChild(n);
	
	n = doc.createElement("title");
	n.appendChild(doc.createTextNode(QString::fromStdString(title)));
	r.appendChild(n);

	n = doc.createElement("dateCreated");
	n.appendChild(doc.createTextNode(QString::number(dateCreated)));
	r.appendChild(n);

	n= doc.createElement("dateModified");
	n.appendChild(doc.createTextNode(QString::number(dateModified)));
	r.appendChild(n);
	
	f.appendChild(r);

	return f;
}

QDomDocumentFragment Article::toXml() const {
	
	QDomDocument doc;
	
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(Note::toXml());
	f.firstChild().toElement().setTagName("article");
	

	QDomElement n;
	n = doc.createElement("text");
	n.appendChild(doc.createTextNode(QString::fromStdString(text)));

	f.firstChild().appendChild(n);
	
	
	return f;
}

QDomDocumentFragment Task::toXml() const {
	QDomDocument doc;
	
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(Note::toXml());
	f.firstChild().toElement().setTagName("task");
	
	QDomElement n;

	n = doc.createElement("action");
	n.appendChild(doc.createTextNode(QString::fromStdString(action)));
	f.firstChild().appendChild(n);

	n = doc.createElement("priority");
	n.appendChild(doc.createTextNode(QString::number(priority)));
	f.firstChild().appendChild(n);

	n = doc.createElement("deadline");
	n.appendChild(doc.createTextNode(QString::number(deadline)));
	f.firstChild().appendChild(n);

	n = doc.createElement("state");
	n.appendChild(doc.createTextNode(QString::number(state)));
	f.firstChild().appendChild(n);


	return f;
}

QDomDocumentFragment MediaNote::toXml() const {
	QDomDocument doc;
	
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(Note::toXml());
	f.firstChild().toElement().setTagName("medianote");

	
	QDomElement n;

	n = doc.createElement("description");
	n.appendChild(doc.createTextNode(QString::fromStdString(description)));
	f.firstChild().appendChild(n);

	n = doc.createElement("filename");
	n.appendChild(doc.createTextNode(QString::fromStdString(filename)));
	f.firstChild().appendChild(n);

	return f;
}

QDomDocumentFragment ImageNote::toXml() const {
	QDomDocument doc;
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(MediaNote::toXml());
	f.firstChild().toElement().setTagName("imagenote");

	return f;
}


QDomDocumentFragment VideoNote::toXml() const {
	QDomDocument doc;
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(MediaNote::toXml());
	f.firstChild().toElement().setTagName("videonote");


	return f;
}


QDomDocumentFragment AudioNote::toXml() const {
	QDomDocument doc;
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	f.appendChild(MediaNote::toXml());
	f.firstChild().toElement().setTagName("audionote");

	return f;
}

