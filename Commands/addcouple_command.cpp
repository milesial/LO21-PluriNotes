#include "addcouple_command.h"
#include "notes_exception.h"

void AddCoupleCommand::undo() {
	try{
		r->removeCouple(c);
	} catch(NotesException& e) {
		
	}
}

void AddCoupleCommand::redo() {
	try{
		r->addCouple(c);
	} catch(NotesException& e) {
		
	}
}
