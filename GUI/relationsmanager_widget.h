#ifndef RELATIONSMANAGER_WIDGET_H
#define RELATIONSMANAGER_WIDGET_H

#include <QUndoStack>
#include <QWidget>
#include "relation.h"

namespace Ui {
class RelationsManagerWidget;
}
/*!
 * \brief La classe RelationsManagerWidget est une fenetre qui permet d'afficher et de gérer toutes les relations du NotesManager.
 */
class RelationsManagerWidget : public QWidget
{
	Q_OBJECT
	
public:
	/*!
	 * \brief Construit la fenetre.
	 * \param stack Le QUndoStack utilisé dans la NotesWindow.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit RelationsManagerWidget(QUndoStack &stack, QWidget *parent = 0);
	~RelationsManagerWidget();
	
private:
	Ui::RelationsManagerWidget *ui;
	QUndoStack& commands;
	Relation* getRelation(QString s);
	void updateList();
	void updateCouples();
	
private slots:
	void changeRelation(QString s);
	void titleChanged(QString s);
	void descChanged();
	void orientedChanged(bool b);
	void createRelation();
	void addNewRelation(Relation*);
	void createCouple();
	void addNewCouple(Couple c);
	void removeRelation();
	void removeCurrentCouple();
	void undo();
	void redo();
	
	
};

#endif // RELATIONSMANAGER_WIDGET_H
