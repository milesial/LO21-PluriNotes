#include "restorearchive_command.h"

#include "notes_manager.h"

void RestoreArchiveCommand::undo() {
	if(NotesManager::getInstance().isActive(id))
		NotesManager::getInstance().setArchived(id);
}

void RestoreArchiveCommand::redo() {
	if(NotesManager::getInstance().isArchived(id))
		NotesManager::getInstance().restoreFromArchive(id);
}
