#include "addrelation_command.h"
#include "notes_manager.h"

void AddRelationCommand::undo() {
	NotesManager::getInstance().getRelationsManager().removeRelation(r->getTitle());
}

void AddRelationCommand::redo() {
	NotesManager::getInstance().getRelationsManager().addRelation(r);
}

