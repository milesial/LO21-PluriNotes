#ifndef RESTOREVERSIONCOMMAND_H
#define RESTOREVERSIONCOMMAND_H

#include <QUndoCommand>
#include "memento.h"
#include "notes_manager.h"
/*!
 * \brief La classe RestoreVersionCommand gère la commande de restauration d'une version d'une note, et son annulation.
 */
class RestoreVersionCommand : public QUndoCommand
{
public:
private:
	Memento* toRestore;
	Memento* prev;
public:
	/*!
	 * \brief Construit une commande qui se charge de restaurer une version d'une Note.
	 * \param m L'état de la Note à restaurer.
	 * \param parent La commande mère, si il y en a une.
	 */
	RestoreVersionCommand(Memento *m, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Restore version of  : "+m->getState()->getId()), parent),
	      toRestore(m),
		  prev(NotesManager::getInstance().save(m->getState()->getId())){}
	/*!
	 * \brief Restaure la Note comme elle était avant la restauration.
	 */
	void undo();
	/*!
	 * \brief Restaure la version, la Note se retrouve donc à l'état de la version donnée.
	 */
	void redo();
};

#endif // RESTOREVERSIONCOMMAND_H
