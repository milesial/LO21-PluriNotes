#ifndef ADDRELATIONCOMMAND_H
#define ADDRELATIONCOMMAND_H

#include <QUndoCommand>
#include "relation.h"
/*!
 * \brief La classe AddRelationCommand gère la commande d'ajout d'une nouvelle relation, et son annulation.
 */
class AddRelationCommand : public QUndoCommand
{
private:
	Relation *r;
public:
	/*!
	 * \brief Construit une commande qui se charge d'ajouter une Relation au NotesManager.
	 * \param r La relation à ajouter.
	 * \param parent La commande mère, si il y en a une.
	 */
	AddRelationCommand(Relation *r, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Create relation "+r->getTitle()), parent), r(r) {}
	/*!
	 * \brief Supprime la Relation du NotesManager.
	 */
	void undo();
	/*!
	 * \brief Ajoute la Relation au NotesManager.
	 */
	void redo();
};

#endif // ADDRELATIONCOMMAND_H
