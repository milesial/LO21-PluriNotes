# VIDEO

+ Charger projet
+ Montrer les types de notes (via liste active) et modifier les champs
+ Montrer la liste des taches, les filtres du bas et le tri par priorité / deadline
+ Montrer le bouton sauvegarder (+ saveAll)
+ Montrer la liste des versions à droite
	+ voir version
	+ supprimer version (+ CTRL-Z)
	+ restaurer version (+ CTRL-Z)
+ Montrer le bouton archiver (+ CTRL-Z)
+ %ontrer la liste des notes archivées (champs non modifiables)
+ Montrer le bouton delete (+ CTRL-Z)
+ Montrer la corbeille (champs non modifiables)

+ Montrer liste  descendants d'une note
+ Montrer tout de la fenetre manager
+ Montrer reference
+ Option clearBin
+ Quitter projet