# Projet PluriNotes

## Build status : ![build status](https://gitlab.utc.fr/milesial/LO21-PluriNotes/badges/master/build.svg)
## Description

Le projet PluriNotes est une application à créer en binôme, nécéssaire pour valider l'UV LO21, elle permet de valider et de mettre en pratique les concepts assimilés en cours et TD. Sa fonction est de gérer des notes manuscrites, images, fichiers audios ou vidéos. Codée en C++, elle utilise le framework Qt pour l'interface graphique. ([Lien vers le sujet complet](http://moodle.utc.fr/pluginfile.php/152794/mod_resource/content/1/Projet_LO21_P2017.pdf))

### Semestre

P17

### Binôme
+ Quentin Molle
+ Alexandre Milesi


***
## Documentation

La documentation est générée automatiquement par Doxygen grâce aux pipelines gitlab. Elle est accessible depuis [la gitlab-page du projet](https://milesial.gitlab.utc.fr/LO21-PluriNotes/).

## UML

![UML](/images/UML_minimal.png "Schéma UML")

## Style du code
Pour avoir un code régulier et propre, respecter les consignes suivantes :


1) Tabulations pour l'indentation, espaces pour l'alignement (changer le mode d'indentation en tabulations dans QtCreator)

2) Pour les boucles et fonctions, mettre les accolades ainsi :

`void foo() {
    // stuff
}`

3) Pour les pointeurs/références, écrire `Type *foo;`

## Conclusion

Beau projet, deadline à 23:59, livrable rendu à 00:03 après avoir fini le projet sur le parking puis dans la rue, après un sprint historique, après plusieurs shutdown totalement inopinés notamment pendant le render de la video puis au moment fatidique d'envoyer le mail à 23:57. Après avoir réalisé que le fichier ressources.qrc (le coquinou) n'était pas dans le zip envoyé et que le programme ne compilait pas, un dernier mail a été envoyé à 00:33, implorant la pitié du chargé de TD.

Au final, une vidéo avec des cuts totalement invisibles, un environnement sonore impeccable et une préparation à toute épreuve, mais il faut bien se le dire, ça claque.

Je tiens tout de même à remercier ce gentil PC, qui malgré son problème de narcolepsie est resté allumé, à 0%, pendant le sprint final, pour finir les 8 minutes de render de la vidéo, avant de tomber dans les ténèbres.

Un remerciement spécial à ThunderBird et à mon mail perso, qui contrairement à la WebMail de l'UTC, ont permis d'envoyer un fichier joint de 20Mo.

~~~ MERCI DE VOTRE ATTENTION, AU REVOIR ~~~
