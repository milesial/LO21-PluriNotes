#include "restorefrombin_command.h"

#include "notes_manager.h"

void RestoreFromBinCommand::undo() {
	if(NotesManager::getInstance().isActive(id))
		NotesManager::getInstance().deleteNote(id);
}

void RestoreFromBinCommand::redo() {
	if(NotesManager::getInstance().isDeleted(id))
		NotesManager::getInstance().restoreFromBin(id);
}
