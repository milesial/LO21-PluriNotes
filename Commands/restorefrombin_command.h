#ifndef RESTOREFROMBINCOMMAND_H
#define RESTOREFROMBINCOMMAND_H

#include <QUndoCommand>
/*!
 * \brief La classe RestoreFromBinCommand gère la commande de restauration d'une note depuis la corbeille, et son annulation.
 */
class RestoreFromBinCommand : public QUndoCommand
{
private:
	std::string id;
public:
	/*!
	 * \brief Construit une commande qui se charge de restaurer une Note depuis la corbeille, et de la passer en Note active.
	 * \param id L'identifiant de la Note à restaurer.
	 * \param parent La commande mère, si il y en a une.
	 */
	RestoreFromBinCommand(const std::string& id, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Restore archive "+id), parent), id(id) {}
	/*!
	 * \brief Envoie la Note dans la corbeille.
	 */
	void undo();
	/*!
	 * \brief Supprime la Note de la corbeille et la passe en Note active.
	 */
	void redo();
};

#endif // RESTOREFROMBINCOMMAND_H
