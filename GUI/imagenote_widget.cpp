#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QFileDialog>

#include "imagenote_widget.h"
#include "ui_imagenote_widget.h"


#include "note.h"

ImageNoteWidget::ImageNoteWidget(ImageNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::ImageNoteWidget)
{
	ui->setupUi(getContainer());
	
	setupFields(n);
}

ImageNoteWidget::ImageNoteWidget(const ImageNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::ImageNoteWidget)
{
	ui->setupUi(getContainer());
	
	
	setupFields(n);
}

void ImageNoteWidget::setupFields(const ImageNote *n) {
	nameFilter = "Images (*.png *.jpg *.jpeg *.bmp *.gif)";
	scene = new QGraphicsScene();
	image = new QGraphicsPixmapItem(QPixmap(QString::fromStdString(n->getFilename())));
	scene->addItem(image);
	
	ui->graphicsView->setScene(scene);
}

ImageNoteWidget::~ImageNoteWidget()
{
	//delete image;
	//delete scene;
	delete ui;
}

void ImageNoteWidget::resizeEvent(QResizeEvent *) {
	ui->graphicsView->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
}

void ImageNoteWidget::showEvent(QShowEvent *) {
	ui->graphicsView->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);	
}

void ImageNoteWidget::onFileChange(QString s) {
	scene = new QGraphicsScene();
	image = new QGraphicsPixmapItem(QPixmap(s));
	scene->addItem(image);
	
	ui->graphicsView->setScene(scene);
}
