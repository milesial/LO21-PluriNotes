#ifndef Note_H
#define Note_H

#include <string>
#include <ctime>
#include <iostream>

#include <QWidget>
#include <QDomElement>
#include "GUI/medianote_widget.h"
#include "GUI/imagenote_widget.h"
#include "GUI/videonote_widget.h"
#include "GUI/audionote_widget.h"
#include "GUI/task_widget.h"
#include "GUI/article_widget.h"


/*
                        _   _       _
                       | \ | |     | |
                       |  \| | ___ | |_ ___
                       | . ` |/ _ \| __/ _ \
                       | |\  | (_) | ||  __/
                       \_| \_/\___/ \__\___|

*/


/*!
 * \brief Une Note représente un ensemble de champs modifiables.
 * Elle est caractérisée par un ID unique non modifiable et un titre.
 * Elle suit aussi automatiquement le moment de dernière modification ainsi que la date de création.
 */
class Note {
friend class NoteFactory;
friend class NotesManager;
friend class Memento;
protected:
	const std::string id; //!< Un label unique qui permet d'identifier la Note et d'y faire référence
	std::string title; //!< Un titre décrivant brièvement la Note
	time_t dateCreated, //!< La date à laquelle la Note a été créée
		   dateModified; //!<La date à laquelle la Note a été modifiée pour la dernière fois
	std::string typeName;

	

    //!
    //! \brief Note Constructeur de Note
    //! \param id nécessaire à la création d'une Note
    //!
	Note(const std::string &id) : id(id), title("Untitled"), typeName("Note") {
		time(&dateCreated);
		updateDate();
	}
	
    //!
    //! \brief Note Constructeur de recopie
    //! \param n Une Note que l'on veut copier
    //!
	Note(const Note &n) : id(n.id),
						  title(n.title),
						  dateCreated(n.dateCreated),
						  dateModified(n.dateModified), 
	                      typeName("Note") {}

	virtual ~Note() {}


	//! Met à jour la date de dernière modification à la date actuelle
	void updateDate() { time(&dateModified); }
	
    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	virtual Note* copy(const std::string& newId) const;
	
    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note
    //! \return le clone créé
    //!
	virtual Note* clone() const { return new Note(*this); }

public:

    //!
    //! \brief getId accesseur
    //! \return id
    //!
	std::string getId() const { return id; }

    //!
    //! \brief getTitle accesseur
    //! \return title
    //!
	std::string getTitle() const { return title; }

    //!
    //! \brief getDateCreated accesseur
    //! \return dateCreated
    //!
	time_t getDateCreated() const { return dateCreated; }

    //!
    //! \brief getDateModified accesseur
    //! \return deteModified
    //!
	time_t getDateModified() const { return dateModified; }

    //!
    //! \brief getTypeName accesseur
    //! \return typeName
    //!
	std::string getTypeName() const { return typeName; }
	
    //!
    //! \brief getWidget permet d'editer des Notes
    //! \return un QWidget représentant et permettant d'éditer la Note
    //!
	virtual NoteWidget* getWidget();
	
    //!
    //! \brief getWidget permet de lire (lecture seule) des Notes
    //! \return un QWidget représentant la Note, mais en mode lecture seulement
    //!
	virtual NoteWidget* getWidget() const;

    //!
    //! \brief setTitle change le titre de la Note
    //! \param title le nouveau titre
    //!
	void setTitle(const std::string& title) {
		this->title = title;
		updateDate();
	}


    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	virtual std::string toString() const;

    //!
    //! \brief toXml copie la Note en un format xml
    //! \return un QDomElement décrivant la Note
    //!
	virtual QDomDocumentFragment toXml() const;
	
    //!
    //! \brief setDateModified modifie la date de derniere modification
    //! \param dateModified la nouvelle date.
    //!
	void setDateModified(time_t dateModified) {
		this->dateModified = dateModified;
	}
private:
	void setDateCreated(time_t dateCreated) {
		this->dateCreated = dateCreated;
	}
	
	
};



/*
                          ___       _   _      _
                         / _ \     | | (_)    | |
                        / /_\ \_ __| |_ _  ___| | ___
                        |  _  | '__| __| |/ __| |/ _ \
                        | | | | |  | |_| | (__| |  __/
                        \_| |_/_|   \__|_|\___|_|\___|
*/



/*!
 * \brief Un Article est une Note contenant un champ de texte
 */

class Article : public Note {
friend class ArticleFactory;
protected:
	std::string text; //!< Le texte de l'article

    //!
    //! \brief Article constructeur d'article en protected car utilisation de factories
    //! \param id de la Note à créer
    //!
	Article(const std::string& id) : Note(id), text("") { typeName = "Article"; }

    //!
    //! \brief Article constructeur de recopie
    //! \param n un article deja existant
    //!
	Article(const Article &n) : Note(n), text(n.text){ typeName = "Article"; }

    //!
    //! \brief Article constructeur à partir du type Note.
    //! \param n une Note
    //!
	Article(const Note &n) : Note(n), text("") { typeName = "Article"; }

    //!
    //! \brief copy Construit un nouvel article avec les mêmes champs que l'article
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return l'article copié
    //!
	Article* copy(const std::string& newId) const;

    //! \brief clone construit une nouvelle Note totalement identique à cet article
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans le nouvel article
    //! \return l'article cloné
	Article* clone() const { return new Article(*this); }

public:

    //!
    //! \brief getText accesseur
    //! \return le Texte
    //!
	std::string getText() const { return text; }

    //!
    //! \brief setText modifie le texte de la Note
    //! \param text le nouveau texte
    //!
	void setText(const std::string& text) {
		this->text = text;
		updateDate();
	}
    //!
    //! \brief getWidget permet d'editer des articles
    //! \return un QWidget représentant et permettant d'éditer l'article
    //!
	ArticleWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des Articles
    //! \return un QWidget représentant l'Article, mais en mode lecture seulement
    //!
	ArticleWidget* getWidget() const;

    //!
    //! \brief toString transforme notre Article en chaine de caracteres
    //! \return La chaine de caracteres créée
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie l'Article en un format xml
    //! \return un QDomElement décrivant l'Article
    //!
	QDomDocumentFragment toXml() const;

};

/*
                         _____         _
                        |_   _|       | |
                          | | __ _ ___| | __
                          | |/ _` / __| |/ /
                          | | (_| \__ \   <
                          \_/\__,_|___/_|\_\
*/



/*!
 * \brief Une Task est une Note avec une priorité optionnelle et une date de fin optionnelle, ainsi qu'une action et un état
 */
class Task : public Note {
friend class TaskFactory;
public:
	//! Représente la progression d'un tâche
	enum TaskState {
		WAITING, //!< En attente
		IN_PROGRESS, //!< En cours
		FINISHED //!< Terminée
	};
protected:
	

	int priority; //!< La priorité de la tâche (0 si aucune priorité)
	time_t deadline; //!< La date de fin de la tâche (0 si aucune date de fin)
	std::string action; //!< L'action à effectuer
	TaskState state; //!< La progression de la tâche

    //!
    //! \brief Task constructeur de task à partir d'un string
    //! \param id necessaire à la creation d'une Note
    //!
	Task(const std::string& id) : Note(id),
								  priority(0),
								  deadline(0),
								  action(""),
								  state(WAITING) {typeName = "Task";}
    //!
    //! \brief Task constructeur de recopie
    //! \param n la tache à copier.
    //!
	Task(const Task &n) : Note(n),
						  priority(n.priority),
						  deadline(n.deadline),
						  action(n.action),
						  state(n.state) {typeName = "Task";}

    //!
    //! \brief Task Construcetru à partir d'une Note générale
    //! \param n la Note qu'on copie sous forme de task
    //!
	Task(const Note &n) : Note(n),
						  priority(0),
						  deadline(0),
						  action(""),
						  state(WAITING) {typeName = "Task";}
    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	Task* copy(const std::string& newId) const;
	
    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note
    //! \return le clone créé
    //!
	Task* clone() const { return new Task(*this); }

public:
	
    //!
    //! \brief getPriority Accesseur
    //! \return Priorité
    //!
	int getPriority() const { return priority; }

    //!
    //! \brief getDeadline Accesseur
    //! \return deadLine
    //!
	time_t getDeadline() const { return deadline; }

    //!
    //! \brief getAction Accesseur
    //! \return action
    //!
	std::string getAction() const { return action; }

    //!
    //! \brief getState Accesseur
    //! \return L'etat actuel
    //!
	TaskState getState() const { return state; }

    //!
    //! \brief hasPriority permet de savoir s'il y a un prioirté
    //! \return vrai s'il y a une priorité
    //!
	bool hasPriority() const { return (priority > 0); }

    //!
    //! \brief hasDeadline permet de savoir si il y a une deadline
    //! \return vrai s'il y a une deadLine
    //!
	bool hasDeadline() const { return (deadline > 0); }
	
    //!
    //! \brief getWidget permet d'editer des tasks
    //! \return un QWidget représentant et permettant d'éditer la task
    //!
	TaskWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des Tasks
    //! \return un QWidget représentant la Task, mais en mode lecture seulement
    //!
	TaskWidget* getWidget() const;

    //!
    //! \brief setPriority change la priorité de notre Task
    //! \param priority nouvelle priorité
    //!
	void setPriority(int priority) { 
		this->priority = priority;
		updateDate();
	}

    //!
    //! \brief setDeadline change la deadLine
    //! \param deadLine nouvelle deadLine
    //!
	void setDeadline(const time_t& deadLine) { 
		this->deadline = deadLine;
		updateDate();
	}

    //!
    //! \brief setAction change l'action
    //! \param action nouvelle action
    //!
	void setAction(const std::string& action) { 
		this->action = action;
		updateDate();
	}

    //!
    //! \brief setState change l'etat
    //! \param state nouvel etat
    //!
	void setState(TaskState state) { 
		this->state = state;
		updateDate();
	}

    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie la Task en un format xml
    //! \return un QDomElement décrivant la Task
    //!
	QDomDocumentFragment toXml() const;

};




/*
                        ___  ___         _ _       _   _       _
                        |  \/  |        | (_)     | \ | |     | |
                        | .  . | ___  __| |_  __ _|  \| | ___ | |_ ___
                        | |\/| |/ _ \/ _` | |/ _` | . ` |/ _ \| __/ _ \
                        | |  | |  __/ (_| | | (_| | |\  | (_) | ||  __/
                        \_|  |_/\___|\__,_|_|\__,_\_| \_/\___/ \__\___|
*/




/*!
 * \brief Une MediaNote est une Note représentant un fichier et une description
 */

class MediaNote : public Note {
friend class MediaNoteFactory;

protected:
	std::string filename, //!< Le chemin absolu du fichier
				description; //!< Une description du fichier

    //!
    //! \brief MediaNote constructeur ne prenant que le champs necessaire à la création
    //! \param id L'id de la MediaNote qui va etre créée
    //!
	MediaNote(const std::string& id) : Note(id),
									   filename(""),
									   description("") {typeName = "Media";}

    //!
    //! \brief MediaNote constructeur de recopie
    //! \param n La Task à recopier
    //!
	MediaNote(const MediaNote &n) : Note(n),
									filename(n.filename),
									description(n.description) {typeName = "Media";}

    //!
    //! \brief MediaNote constructeur de recopie d'une Note générale. Transforme une Note quelconque en MediaNote
    //! \param n La  Note à recopier
    //!
	MediaNote(const Note &n) : Note(n),
							   filename(""),
							   description("") {typeName = "Media";}
public:
	
    //!
    //! \brief getWidget permet d'editer des MediaNotes
    //! \return un QWidget représentant et permettant d'éditer les MediaNotes
    //!
	NoteWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des MediaNotes
    //! \return un QWidget représentant la MediaNote, mais en mode lecture seulement
    //!
	NoteWidget* getWidget() const;
protected:
    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	MediaNote* copy(const std::string& newId) const;

    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note
    //! \return le clone créé
    //!
	MediaNote* clone() const { return new MediaNote(*this); }

public:

    //!
    //! \brief getFilename Accesseur
    //! \return le chemin vers le fichier
    //!
	std::string getFilename() const { return filename; }

    //!
    //! \brief getDescription Accesseur
    //! \return la description
    //!
	std::string getDescription() const {return description; }

    //!
    //! \brief setFilename permet de changer/initialiser le filename
    //! \param filename le nouveau chemin vers fichier
    //!
	void setFilename(const std::string& filename) {
		this->filename = filename;
		updateDate();
	}

    //!
    //! \brief setDescription modifie la description
    //! \param description nouvelle description
    //!
	void setDescription(const std::string& description) {
		this->description = description;
		updateDate();
	}

    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie la MediaNote en un format xml
    //! \return un QDomElement décrivant la MediaNote
    //!
	QDomDocumentFragment toXml() const;

};

/*!
 * \brief Une ImageNote est une MediaNote représentant une image
 */
class ImageNote : public MediaNote {
friend class ImageNoteFactory;
protected:

//!
//! \brief ImageNote constructeur ne prenant en argument que l'ID
//! \param id de la nouvelle Note créée.
//!
	ImageNote(const std::string& id) : MediaNote(id) {typeName = "Image";}

    //!
    //! \brief ImageNote constructeur de recopie
    //! \param n l'ImageNote à recopier
    //!
	ImageNote(const ImageNote &n) : MediaNote(n) {typeName = "Image";}

    //!
    //! \brief ImageNote constructeur de recopie à partir d'une Note quelconque.
    //! \param n la Note à recopier
    //!
	ImageNote(const Note &n) : MediaNote(n) {typeName = "Image";}

    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	ImageNote* copy(const std::string& newId) const;
	
    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note
    //! \return le clone créé
    //!
	ImageNote* clone() const { return new ImageNote(*this); }

public:
	
    //!
    //! \brief getWidget permet d'editer des ImageNotes
    //! \return un QWidget représentant et permettant d'éditer les ImageNote
    //!
	ImageNoteWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des ImageNotes
    //! \return un QWidget représentant l'ImageNote, mais en mode lecture seulement
    //!
	ImageNoteWidget* getWidget() const;
	
    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie l'ImageNote en un format xml
    //! \return un QDomElement décrivant l'ImageNote
    //!
	QDomDocumentFragment toXml() const;

};


/*!
 * \brief Une VideoNote est une Note représentant une vidéo
 */
class VideoNote : public MediaNote {
friend class VideoNoteFactory;
protected:

//!
//! \brief VideoNote constructeur ne prenant qu'un ID en paramètre
//! \param id nouvelle ID pour la nouvelle Note
//!
	VideoNote(const std::string& id) : MediaNote(id) {typeName = "Video";}

    //!
    //! \brief VideoNote Constructeur de recopie
    //! \param n la VideoNote à recopier
    //!
	VideoNote(const VideoNote &n) : MediaNote(n) {typeName = "Video";}

    //!
    //! \brief VideoNote constructeur de recopie d'une Note quelconque
    //! \param n la Note à recopier
    //!
	VideoNote(const Note &n) : MediaNote(n) {typeName = "Video";}
public:
    //!
    //! \brief getWidget permet d'editer des VideoNotes
    //! \return un QWidget représentant et permettant d'éditer la VideoNote
    //!
	VideoNoteWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des VideoNotes
    //! \return un QWidget représentant la VideoNote, mais en mode lecture seulement
    //!
	VideoNoteWidget* getWidget() const;
protected:
    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	VideoNote* copy(const std::string& newId) const;

    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note
    //! \return le clone créé
    //!
	VideoNote* clone() const { return new VideoNote(*this); }

public:
    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie la VifroNote en un format xml
    //! \return un QDomElement décrivant la VideoNote
    //!
	QDomDocumentFragment toXml() const;

};



/*!
 * \brief Une AudioNote est une Note représentant un fichier audio
 */
class AudioNote : public MediaNote {
friend class AudioNoteFactory;
protected:

//!
//! \brief AudioNote Constructeur ne prenant que le champ obligatoire ID
//! \param id l'id de la nouvelle Note
//!
	AudioNote(const std::string& id) : MediaNote(id) {typeName = "Audio";}

    //!
    //! \brief AudioNote Constructeur de recopie
    //! \param n AudioNote à recopier
    //!
	AudioNote(const AudioNote &n) : MediaNote(n) {typeName = "Audio";}

    //!
    //! \brief AudioNote Constructeur de recopier à partir d'une Note quelconque
    //! \param n La Note à recopier
    //!
	AudioNote(const Note &n) : MediaNote(n) {typeName = "Audio";}
public:

    //!
    //! \brief getWidget permet d'editer des AUdioNotes
    //! \return un QWidget représentant et permettant d'éditer l'AudioNote
    //!
	AudioNoteWidget* getWidget();

    //!
    //! \brief getWidget permet de lire (lecture seule) des AudioNotes
    //! \return un QWidget représentant l'AudoiNote, mais en mode lecture seulement
    //!
	AudioNoteWidget* getWidget() const;
protected:
    //!
    //! \brief copy Construit une nouvelle Note avec les mêmes champs que cette Note.
    //! La nouvelle Note est créée avec un nouvel ID, et les dates de création et de modification sont mises à jour.
    //! \param newId le nouvel id
    //! \return cette nouvelle Note
    //!
	AudioNote* copy(const std::string& newId) const;
	
    //!
    //! \brief clone construit une nouvelle Note totalement identique à cette Note.
    //! Les dates de création et de modification, ainsi que l'ID sont copiés dans la nouvelle Note.
    //! \return le clone créé
    //!
	AudioNote* clone() const { return new AudioNote(*this); }

public:
    //!
    //! \brief toString permet de décrire la Note avec un string
    //! \return une chaîne de caractère décrivant la Note
    //!
	std::string toString() const;

    //!
    //! \brief toXml copie l'AudioNote en un format xml
    //! \return un QDomElement décrivant l'AudioNote
    //!
	QDomDocumentFragment toXml() const;

};

std::ostream& operator<<(std::ostream& os, const Note& note);


#endif // Note_H

