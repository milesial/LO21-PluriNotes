#ifndef NOTESMANAGER_H
#define NOTESMANAGER_H


#include <iostream>
#include <QUndoStack>

#include "note.h"
#include "note_factory.h"
#include "relation.h"
#include "memento.h"

class VersionNote;
class Memento;

/*!
 * \brief La classe NotesManager permet de gérer des notes et les relations qui existent entre elles
 */
class NotesManager
{
	/*!
	 * \brief La classe LastVersionIterator permet d'itérer sur les dernières versions d'une liste de VersionNote
	 */
	class LastVersionIterator {
	public:

        //!
        //! \brief LastVersionIterator constructeur
        //! \param pos un iterateur sur tableau
        //!
		LastVersionIterator(std::vector<const VersionNote*>::const_iterator pos) : m_pos(pos) {}

        //!
        //! \brief operator == verifie si les deux iterateur sont egaux
        //! \param it l'iterateur auquel on veut comparer celui de notre classe
        //! \return vrai si egaux, faux sinon
        //!
		bool operator==(const LastVersionIterator& it) { return m_pos == it.m_pos; }

        //!
        //! \brief operator =! verifie si les deux iterateur sont différents
        //! \param it l'iterateur auquel on veut comparer celui de notre classe
        //! \return vrai si différents, faux sinon
        //!
		bool operator!=(const LastVersionIterator& it) { return m_pos != it.m_pos; }
		//etc
        //!
        //! \brief operator ++ pré incremente l'iterateur
        //!
		void operator++() {++m_pos;}

        //!
        //! \brief operator ++ post incrémente k'iterateur
        //!
		void operator++(int) {m_pos++;}
		
        //!
        //! \brief operator * récupère la note associée à la dernière version
        //! \return La Note en question
        //!
		const Note* operator*() {
			return (*m_pos)->getLast()->getState();
		}
	private:
        std::vector<const VersionNote*>::const_iterator m_pos; //!< un iterateur de tableau de VersionNote
	};
	
private:
	
	const QString contextFile = ".context";
	
	NotesManager();
	~NotesManager();
	NotesManager(const NotesManager& nm);
	
	
		
	std::vector<Note*> activeNotes;
	
	std::map<std::string, VersionNote*> activeVersions;
	std::vector<const VersionNote*> archivedNotes;
	std::vector<const VersionNote*> deletedNotes; //corbeille
	
	RelationsManager relations;
	
	std::map<std::string, NoteFactory*> factories;
	
	
public:

    //!
    //! \brief getInstance accesseur.
    //! \return Une reference surl'unique instance de notre NoteManager
    //!
	static NotesManager& getInstance() {
		static NotesManager in;
		return in;
	}
	
    //!
    //! \brief getActiveCount Accesseur
    //! \return le nombre de notes actives
    //!
	int getActiveCount() { return activeNotes.size(); }
	
    //!
    //! \brief getArchivedCount Accesseur
    //! \return Retourne le nombre de notes archivées
    //!
	int getArchivedCount() { return archivedNotes.size(); }
	
    //!
    //! \brief getDeletedCount Accesseur
    //! \return le nombre de notes dans la corbeille
    //!
	int getDeletedCount() { return deletedNotes.size(); }
	
    //!
    //! \brief beginArchived va permettre de visualiser toutes les notes archivées avec endArchived
    //! \return un itérateur sur le début des notes archivées
    //!
	LastVersionIterator beginArchived() {
		return LastVersionIterator(archivedNotes.begin());
	}
	
    //!
    //! \brief endArchived
    //! \return un itérateur sur la fin des notes archivées
    //!
	LastVersionIterator endArchived() {
		return LastVersionIterator(archivedNotes.end());
	}
	
    //!
    //! \brief beginDeleted va permettre de visualiser toutes les notes dans la corbeille avec endDeleted
    //! \return un itérateur sur le début des notes dans la corbeille
    //!
	LastVersionIterator beginDeleted() {
		return LastVersionIterator(deletedNotes.begin());
	}
	
    //!
    //! \brief endDeleted
    //! \return un itérateur sur la fin des notes dans la corbeille
    //!
	LastVersionIterator endDeleted() {
		return LastVersionIterator(deletedNotes.end());
	}
	
    //!
    //! \brief beginActive va permettre de visualiser toutes les notes active avec endActive
    //! \return un itérateur sur le début des notes actives
    //!
	std::vector<Note*>::iterator beginActive() {
		return activeNotes.begin();
	}
	
    //!
    //! \brief endActive
    //! \return un itérateur sur la fin des notes actives
    //!
	std::vector<Note*>::iterator endActive() {
		return activeNotes.end();
	}
	
    //!
    //! \brief isArchived permet de savoir si une note est archivée
    //! \param id l'ID de la Note à tester
    //! \return
    //!
	bool isArchived(const std::string& id);

    //!
    //! \brief isArchived permet de savoir si une note est archivée
    //! \param n La Note à tester
    //! \return
    //!
	bool isArchived(const Note *n) { return isArchived(n->getId()); }
	
    //!
    //! \brief isActive permet de savoir si une note est active
    //! \param id L'ID de la Note à tester
    //! \return
    //!
	bool isActive(const std::string& id);

    //!
    //! \brief isActive permet de savoir si une note est active
    //! \param n La Note à Tester
    //! \return
    //!
	bool isActive(const Note *n) { return isActive(n->getId()); }
	
    //!
    //! \brief isDeleted permet de savoir si une Note est dans la corbeille
    //! \param id L'ID de la Note à tester
    //! \return
    //!
	bool isDeleted(const std::string& id);

    //!
    //! \brief isDeleted permet de savoir si une Note est dans la corbeille
    //! \param n La Note à tester
    //! \return
    //!
	bool isDeleted(const Note *n) {	return isDeleted(n->getId()); }
	
    //!
    //! \brief hasNote vérifie si la note est active, archivée ou dans la corbeille
    //! \param id L'id de la Note a tester
    //! \return vrai si la note est active ou archivée ou dans la corbeille
    //!
	bool hasNote(const std::string& id) { return isDeleted(id) || isArchived(id) || isActive(id); }
	
    //!
    //! \brief getVersion permet d'avoir l'ensemble des versions d'une note
    //! \param id L'Id de la Note à tester
    //! \return les versions de la note
    //!
	const VersionNote* getVersion(const std::string& id);

    //!
    //! \brief getVersion permet d'avoir l'ensemble des versions d'une note
    //! \param n la Note à tester
    //! \return les versions de la note
    //!
	const VersionNote* getVersion(const Note *n) { return getVersion(n->getId()); }
	
    //!
    //! \brief getActiveVersion permet d'avoir la version active d'une note
    //! \param id L'Id de la Note à tester
    //! \return
    //!
	VersionNote* getActiveVersion(const std::string& id);

    //!
    //! \brief getActiveVersion permet d'avoir la version active d'une note
    //! \param n la Note à tester
    //! \return
    //!
	VersionNote* getActiveVersion(const Note *n) { return getActiveVersion(n->getId()); }
	
    //!
    //! \brief deleteNote Jette la note active dans la corbeille
    //! \param id L'id de la note à supprimer
    //! \return un pointeur sur la note supprimée
    //!
	const Note* deleteNote(const std::string& id);

    //!
    //! \brief deleteNote Jette la note active dans la corbeille
    //! \param n la note à supprimer
    //! \return un pointeur sur la note supprimée
    //!
	const Note* deleteNote(const Note *n) { return deleteNote(n->getId()); }
	
    //!
    //! \brief setArchived envoie la note active dans les archives
    //! \param id de la note à archiver
    //! \return un pointeur sur la note archivée
    //!
	const Note* setArchived(const std::string& id);

    //!
    //! \brief setArchived envoie la note active dans les archives
    //! \param n la note à archiver
    //! \return un pointeur sur la note archivée
    //!
	const Note* setArchived(const Note *n) { return setArchived(n->getId()); }
	
    //!
    //! \brief save Sauvegarde la note active dans sa liste de versions
    //! \param id l'id de la Note à sauvegarder
    //! \return le Memento créé
    //!
	Memento* save(const std::string& id);

    //!
    //! \brief save Sauvegarde la note active dans sa liste de versions
    //! \param n la Note à sauvegarder
    //! \return le Memento créé
    //!
	Memento* save(const Note *n) { return save(n->getId()); }
	
    //!
    //! \brief saveAll sauvegarde toutes les notes actives
    //!
	void saveAll();
	
    //!
    //! \brief clear supprime toutes les notes actives, archivées et dans la corbeille, ainsi que leurs versions et les relations et references
    //!
	void clear();
	
    //!
    //! \brief getRelationsManager Accesseur
    //! \return l'objet gérant les relations
    //!
	RelationsManager& getRelationsManager() { return relations; }
	
    //!
    //! \brief addFactory ajoute un type de note à la liste proposée lors de la création d'une note
    //! @param label Le label décrivant la note, qui sera affiché lors de la création de la note
    //! \param factory la nouvelle factory
    //!
	void addFactory(NoteFactory* factory, const std::string& label) { factories[label] = factory; }

    //!
    //! \brief getFactories Accesseur
    //! \return tous les factories créées jusqu'a maintenant
    //!
	const std::map<std::string, NoteFactory*>& getFactories() { return factories; }
	
    //!
    //! \brief deleteFromBin supprime définitivement une note de la corbeille
    //! \param id L'id de la note à supprimer
    //!
	void deleteFromBin(const std::string& id);

    //!
    //! \brief deleteFromBin supprime définitivement une note de la corbeille
    //! \param n la note à supprimer
    //!
	void deleteFromBin(const Note *n) { deleteFromBin(n->getId()); }
	
    //!
    //! \brief deleteFromArchive supprime définitivement une note archivée
    //! \param id L'id de la note à supprimer
    //!
	void deleteFromArchive(const std::string& id);

    //!
    //! \brief deleteFromArchive supprime définitivement une note archivée
    //! \param n la note à supprimer
    //!
	void deleteFromArchive(const Note *n) { deleteFromArchive(n->getId()); }
	
    //!
    //! \brief clearBin supprime définitivement toutes les notes de la corbeille
    //!
	void clearBin();
	
    //!
    //! \brief restoreFromBin restaure une note de la corbeille, qui devient alors active
    //! \param id L'id de la note à restaurer
    //! \return la note restaurée
    //!
	Note* restoreFromBin(const std::string& id);

    //!
    //! \brief restoreFromBin restaure une note de la corbeille, qui devient alors active
    //! \param n la note à restaurer
    //! \return la note restaurée
    //!
	Note* restoreFromBin(const Note *n) { return restoreFromBin(n->getId()); }
	
    //!
    //! \brief restoreFromArchive restaure une note archivée, qui devient alors active
    //! \param id L'id de la note à restaurer
    //! \return la note restaurée
    //!
	Note* restoreFromArchive(const std::string& id);

    //!
    //! \brief restoreFromArchive restaure une note archivée, qui devient alors active
    //! \param n la note à restaurer
    //! \return la note restaurée
    //!
	Note* restoreFromArchive(const Note *n) { return restoreFromArchive(n->getId()); }
	
    //!
    //! \brief restore restaure une note de la corbeille ou archivée, qui devient alors active
    //! \param id L'id de la note à restaurer
    //! \return La note restaurée
    //!
	Note* restore(const std::string& id);

    //!
    //! \brief restore restaure une note de la corbeille ou archivée, qui devient alors active
    //! \param n la note à restaurer
    //! \return La note restaurée
    //!
	Note* restore(const Note *n) { return restore(n->getId()); }
	
    //!
    //! \brief getActiveNote Accesseur
    //! \param id L'id de la note que l'on veut recuperer
    //! \return La note active dont l'id est id
    //!
	Note* getActiveNote(const std::string& id);

    //!
    //! \brief getArchiedNote Accesseur
    //! \param id L'id de la note que l'on veut recuperer
    //! \return La note archivée dont l'id est id
    //!
	const Note* getArchivedNote(const std::string& id);

    //!
    //! \brief getActiveNote Accesseur
    //! \param id L'id de la note que l'on veut recuperer
    //! \return La note dans la corbeille dont l'id est id
    //!
	const Note* getDeletedNote(const std::string& id);
	
    //!
    //! \brief getNote récupère la note, qu'elle soit active, archivée ou dans la corbeille
    //! \param id L'id de la note à recuperer
    //! \return La note recuperer
    //!
	const Note* getNote(const std::string& id);
	
    //!
    //! \brief loadFromFile charge un projet depuis les fichiers de l'utilisateur
    //! \param filename le chemin vers le projet (par defaut plurinotes.xml)
    //!
	void loadFromFile(const std::string& filename="plurinotes.xml");

    //!
    //! \brief saveToFile sauvegarde tout le projet dans les fichiers de l'utilisateur au format xml
    //! \param filename le chemin vers l'endroit ou enregistrer
    //!
	void saveToFile(const std::string& filename="plurinotes.xml");
	
    //!
    //! \brief restoreVersion restaure une note active depuis une certaine version
    //! \param m Le memento dans laquelle il y a la version à restaurer
    //! \return La note une fois restaurer
    //!
	Note* restoreVersion(const Memento* m);
	
    //!
    //! \brief addNewNote ajoute une nouvelle note à la liste des notes suivies
    //! \param n La note à ajouter
    //!
	void addNewNote(Note *n);
	
    //!
    //! \brief isSaved vérifie si la dernière version de la note active correspond à la note elle-même
    //! \param id l'id de la note que l'on veut tester
    //! \return vrai si la dernière version de la note active correspond à la note elle-même, faux sinon
    //!
	bool isSaved(const std::string& id);

    //!
    //! \brief isSaved vérifie si la dernière version de la note active correspond à la note elle-même
    //! \param n la note que l'on veut tester
    //! \return vrai si la dernière version de la note active correspond à la note elle-même, faux sinon
    //!
	bool isSaved(const Note *n) { return isSaved(n->getId()); }
	
    //!
    //! \brief checkReferences vérifie l'ajout ou la suppression de nouvelles références dans le texte de la note
    //! \param id l'id de la note à vérifier
    //!
	void checkReferences(const std::string& id);
};



#endif // NOTESMANAGER_H
