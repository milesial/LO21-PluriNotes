#ifndef REMOVERELATIONCOMMAND_H
#define REMOVERELATIONCOMMAND_H

#include "relation.h"
#include <QUndoCommand>
/*!
 * \brief La classe RemoveRelationCommand gère la commande de suppression d'une relation, et son annulation.
 */
class RemoveRelationCommand: public QUndoCommand
{
public:
private:
	Relation *r;
public:
	/*!
	 * \brief Construit une commande qui se charge de supprimer une Relation du NotesManager.
	 * \param r La Relation à supprimer.
	 * \param parent La commande mère, si il y en a une.
	 */
	RemoveRelationCommand(Relation *r, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Remove relation "+r->getTitle()), parent), r(r) {}
	/*!
	 * \brief Ajoute la Relation au NotesManager.
	 */
	void undo();
	/*!
	 * \brief Supprime la Relation du NotesManager.
	 */
	void redo();
};

#endif // REMOVERELATIONCOMMAND_H
