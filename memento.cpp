#include "memento.h"
#include "notes_exception.h"

void VersionNote::removeVersion(Memento* m){
	if(m->getState()->getId() != id)
		throw NotesException("Can't remove version : bad ID");
    unsigned int i=0;
    while (version[i]->getState()->getDateModified() != m->getState()->getDateModified())
        i++;
    version.erase(version.begin()+i);
}

void VersionNote::addVersion(Memento *m, bool force) {
	if(!(force || (version.size() == 0 || m->getState()->getDateModified() != getLast()->getState()->getDateModified()))) {
		return;
	}
	if(m->getState()->getId() == this->id) {
		if(version.size()==0) {
			version.push_back(m);
			return;
		}
		for(auto it = version.begin(); it != version.end(); it++) {
			if((*it)->getState()->getDateModified() > m->getState()->getDateModified()) {
				version.insert(it, m);
				return;
			}
		}
		version.push_back(m);
		
	} else
		throw NotesException("Can't add version : bad ID");
}

VersionNote::~VersionNote() {
	for(auto it = version.begin(); it != version.end(); it++) {
		delete *it;
	}
}

Memento* VersionNote::getLast() const {
	//std::cout << version.size();
	
	if(version.size() != 0) {
		return version.back();
	}
	
	throw NotesException("This note has no version ! ");
}

QDomDocumentFragment VersionNote::toXml() const {
	QDomDocument doc;
	QDomDocumentFragment f = doc.createDocumentFragment();
	
	QDomElement v = doc.createElement("versionnote");
	v.setAttribute("id", QString::fromStdString(id));
	
	for(auto it = version.rbegin(); it != version.rend(); it++) {
		v.appendChild((*it)->getState()->toXml());
	}
	f.appendChild(v);
	return f;
}
