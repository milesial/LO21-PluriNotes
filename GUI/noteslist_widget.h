#ifndef NOTESLIST_WIDGET_H
#define NOTESLIST_WIDGET_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QDockWidget>

namespace Ui {
class NotesListWidget;
}
/*!
 * \brief La classe NotesListWidget permet d'afficher la liste de toutes les notes du NotesManager.
 * Le widget est séparé en 3 parties : les notes actives, la corbeille et les archives.
 * Si une partie est vide, elle n'est pas affichée.
 */
class NotesListWidget : public QWidget
{
	Q_OBJECT
private slots:
	void activeClicked(QTreeWidgetItem* i);
	void deletedClicked(QTreeWidgetItem* i);
	void archivedClicked(QTreeWidgetItem* i);
signals:
	//! Une note active a été sélectionnée.
	void activeClicked(QString id);
	//! Une note archivée a été sélectionnée.
	void archivedClicked(QString id);
	//! Une note de la corbeille a été sélectionnée.
	void deletedClicked(QString id);
public:
	/*!
	 * \brief Construit le widget d'affichage de la liste des Note.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit NotesListWidget(QWidget *parent = 0);
	~NotesListWidget();
	
public slots:
	//! Force la sélection sur la note avec l'ID donné
	void setSelected(QString id);
	//! Rafraîchit la liste en récupérnt de nouveau toutes les notes du NotesManager
	void update();
	//! Rafraîchit uniquement les titres des notes déjà présentes dans la liste
	void updateTitles();
	
private:
	Ui::NotesListWidget *ui;
};

#endif // NOTESLIST_WIDGET_H
