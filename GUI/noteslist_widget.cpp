#include "noteslist_widget.h"
#include "ui_noteslist_widget.h"
#include "notes_manager.h"
#include "note.h"
#include "memento.h"

#include <QStringList>

NotesListWidget::NotesListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NotesListWidget)
{
	ui->setupUi(this);
	
	update();
	
	connect(ui->active, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(activeClicked(QTreeWidgetItem*)));
	connect(ui->archived, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(archivedClicked(QTreeWidgetItem*)));
	connect(ui->deleted, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(deletedClicked(QTreeWidgetItem*)));
	
}

NotesListWidget::~NotesListWidget()
{
	delete ui;
}

//load again all notes from the manager and recreate the list
void NotesListWidget::update() {
	NotesManager& nm = NotesManager::getInstance();
	
	ui->active->clear();
	for(auto it = nm.beginActive(); it != nm.endActive(); it++) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->active);
		item->setText(0, QString::fromStdString((*it)->getTitle()));
		item->setText(1, QString::fromStdString((*it)->getTypeName()));
		item->setData(0, Qt::UserRole, QString::fromStdString((*it)->getId()));
	}
	
	
	ui->archived->clear();
	for(auto it = nm.beginArchived(); it != nm.endArchived(); it++) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->archived);
		item->setText(0, QString::fromStdString((*it)->getTitle()));
		item->setText(1, QString::fromStdString((*it)->getTypeName()));
		item->setData(0, Qt::UserRole, QString::fromStdString((*it)->getId()));
	}
	
	ui->deleted->clear();
	for(auto it = nm.beginDeleted(); it != nm.endDeleted(); it++) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->deleted);
		item->setText(0, QString::fromStdString((*it)->getTitle()));
		item->setText(1, QString::fromStdString((*it)->getTypeName()));
		item->setData(0, Qt::UserRole, QString::fromStdString((*it)->getId()));
	}
	
	ui->archived->setVisible(ui->archived->topLevelItemCount() != 0);
	ui->deleted->setVisible(ui->deleted->topLevelItemCount() != 0);
	
}

void NotesListWidget::activeClicked(QTreeWidgetItem *i) {
	ui->archived->clearSelection();
	ui->deleted->clearSelection();
	emit activeClicked(i->data(0, Qt::UserRole).toString());
}

void NotesListWidget::archivedClicked(QTreeWidgetItem *i) {
	ui->active->clearSelection();
	ui->deleted->clearSelection();
	emit archivedClicked(i->data(0, Qt::UserRole).toString());
}

void NotesListWidget::deletedClicked(QTreeWidgetItem *i) {
	ui->archived->clearSelection();
	ui->active->clearSelection();
	emit deletedClicked(i->data(0, Qt::UserRole).toString());
}


void NotesListWidget::setSelected(QString id) {
	ui->archived->clearSelection();
	ui->active->clearSelection();
	ui->deleted->clearSelection();
	
	
	for(int i = 0; i < ui->active->topLevelItemCount(); i++) {
		if(ui->active->topLevelItem(i)->data(0, Qt::UserRole).toString() == id) {
			ui->active->setFocus();
			ui->active->setCurrentItem(ui->active->topLevelItem(i));
			emit activeClicked(id);
			return;
		}
	}
	
	for(int i = 0; i < ui->archived->topLevelItemCount(); i++) {
		if(ui->archived->topLevelItem(i)->data(0, Qt::UserRole).toString() == id) {
			ui->archived->setFocus();
			ui->archived->setCurrentItem(ui->archived->topLevelItem(i));
			emit archivedClicked(id);
			return;
		}
	}
	
	for(int i = 0; i < ui->deleted->topLevelItemCount(); i++) {
		if(ui->deleted->topLevelItem(i)->data(0, Qt::UserRole).toString() == id) {
			ui->deleted->setFocus();
			ui->deleted->setCurrentItem(ui->deleted->topLevelItem(i));
			emit deletedClicked(id);
			return;
		}
	}

	
	
}

void NotesListWidget::updateTitles() {
	for(int i = 0; i < ui->active->topLevelItemCount(); i++) {
		ui->active->topLevelItem(i)->setText(0, QString::fromStdString(
		                                     NotesManager::getInstance().getActiveNote(
		                                         ui->active->topLevelItem(i)->data(0, Qt::UserRole)
		                                             .toString().toStdString())->getTitle()));
		
	}
}
