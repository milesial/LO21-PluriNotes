#ifndef ADDCOUPLECOMMAND_H
#define ADDCOUPLECOMMAND_H

#include <QUndoCommand>
#include "relation.h"
/*!
 * \brief La clase AddCoupleCommand gère la commande d'ajout d'un couple dans une relation, et son annulation.
 */
class AddCoupleCommand : public QUndoCommand
{
private:
	Relation *r;
	Couple c;
public:
	/*!
	 * \brief Construit une commande qui se charge d'ajouter un Couple à une Relation donnée.
	 * \param r La Relation dans laquelle le couple est ajouté.
	 * \param c Le Couple à ajouter dans la Relation.
	 * \param parent La commande mère, si il y en a une.
	 */
	AddCoupleCommand(Relation *r, Couple c, QUndoCommand *parent = 0)
		: QUndoCommand(QString::fromStdString("Add couple to relation "+r->getTitle()), parent), r(r), c(c) {}
	/*!
	 * \brief Supprime le Couple de la Relation.
	 */
	void undo();
	/*!
	 * \brief Ajoute le Couple à la Relation.
	 */
	void redo();
};

#endif // ADDCOUPLECOMMAND_H
