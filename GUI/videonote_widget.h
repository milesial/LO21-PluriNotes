#ifndef VIDEONOTE_WIDGET_H
#define VIDEONOTE_WIDGET_H

#include <QWidget>
#include <QMediaPlayer>
#include <QGraphicsVideoItem>
#include <QMediaPlaylist>

#include "medianote_widget.h"

class VideoNote;

namespace Ui {
class VideoNoteWidget;
}
/*!
 * \brief La classe VideoNoteWidget est un QWidget qui affiche une VideoNote.
 * En plus des fonctionnalités d'un MediaNoteWidget, il permet de lire une vidéo (.mp4, .avi, .mov, .mkv, .mpa).
 */
class VideoNoteWidget : public MediaNoteWidget
{
	Q_OBJECT
	
	
public:
	/*!
	 * \brief Construit un VideoNoteWidget qui affiche une VideoNote avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de la VideoNote associé.
	 * \param n La VideoNote à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit VideoNoteWidget(VideoNote *n, QWidget *parent = 0);
	
	/*!
	 * \brief Construit un VideoNoteWidget qui affiche une VideoNote en mode lecture seule.
	 * \param n La VideoNote à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit VideoNoteWidget(const VideoNote *n, QWidget *parent = 0);
	~VideoNoteWidget();
	//! Joue la vidéo
	void play();
	//! Pause la vidéo
	void stop();
	
protected:
	/*!
	 * \brief Initialise tous les champs à partir d'une VideoNote.
	 * \param n La VideoNote à afficher.
	 */
	void setupFields(const VideoNote *n);
	/*!
	 * \brief Rafraichit le widget pour afficher la video.
	 */
	void resizeEvent(QResizeEvent *);
	
	/*!
	 * \brief Rafraichit le widget pour afficher la video.
	 */
	void showEvent(QShowEvent *);
	/*!
	 * \brief Rafraichit le widget pour afficher le nouveau fichier.
	 * \param s Chemin absolu du nouveau fichier.
	 */
	void onFileChange(QString s);
	
	
private:
	Ui::VideoNoteWidget *ui;
	QMediaPlayer *player;
	QVideoWidget *videoWidget;
	
private slots:
	void setSlider(qint32 i);
	void setTime(int i);
	void playStop();
};

#endif // VIDEONOTE_WIDGET_H
