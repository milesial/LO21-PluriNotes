#include "archivenote_command.h"

#include "notes_manager.h"

void ArchiveNoteCommand::undo() {
	if(NotesManager::getInstance().isArchived(id))
		NotesManager::getInstance().restoreFromArchive(id);
}

void ArchiveNoteCommand::redo() {
	if(NotesManager::getInstance().isActive(id))
		NotesManager::getInstance().setArchived(id);
}
