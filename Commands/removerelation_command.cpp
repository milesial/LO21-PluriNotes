#include "removerelation_command.h"


#include "notes_manager.h"

void RemoveRelationCommand::redo() {
	NotesManager::getInstance().getRelationsManager().removeRelation(r->getTitle());
}

void RemoveRelationCommand::undo() {
	NotesManager::getInstance().getRelationsManager().addRelation(r);
}

