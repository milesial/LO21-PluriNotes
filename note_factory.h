#ifndef NOTE_FACTORY_H
#define NOTE_FACTORY_H

#include <QDomElement>

#include "note.h"


/*!
 * \brief Une NoteFactory permet de créer des notes en les ajoutant à la liste des notes gérées par le NotesManager
 */
class NoteFactory {
friend class NotesManager;
public:

    //!
    //! \brief create crée une Note et l'ajoute au NotesManager
    //! \param id necessaire à la création de la Note
    //! \return la Note créée
    //!
	virtual Note* create(const std::string& id) = 0;
	virtual ~NoteFactory() {}

protected:

    //!
    //! \brief isXmlValid vérifie si un élément XML représente une note
    //! \param e l'element à tester
    //! \return vrai si est valide
    //!
	virtual bool isXmlValid(QDomElement e);

//!
//! \brief fromXml construit une note depuis un élément XML
//! \param e L'element xml dont on eut recuperer la note
//! \return la note en question
//!
	virtual Note* fromXml(QDomElement e);
};

/*!
 * \brief Une ArticleFactory permet de créer des Articles en les ajoutant à la liste des notes gérées par le NotesManager
 * elle herite de NoteFactory
 */
class ArticleFactory : public NoteFactory {
public:

    //!
    //! \brief create crée un Article et l'ajoute au NotesManager
    //! \param id necessaire à la création de l'Article
    //! \return l'Article créé
    //!
	Article* create(const std::string& id);

protected:

	bool isXmlValid(QDomElement e);

	Article* fromXml(QDomElement e);
};

/*!
 * \brief Une TaskFactory permet de créer des tasks en les ajoutant à la liste des notes gérées par le NotesManager.
 * elle herite de NoteFactory
 */
class TaskFactory : public NoteFactory{
public:

    //!
    //! \brief create crée une Task l'ajoute au NotesManager
    //! \param id necessaire à la création de la Task
    //! \return la Task créée
    //!
	Task* create(const std::string& id);
	
protected:

	bool isXmlValid(QDomElement e);

	Task* fromXml(QDomElement e);
};

/*!
 * \brief Une MediaNoteFactory permet de créer des medianotes en les ajoutant à la liste des notes gérées par le NotesManager
 * elle herite de NoteFactory
 */
class MediaNoteFactory : public NoteFactory {
public:	
	bool isXmlValid(QDomElement e);
	MediaNote* fromXml(QDomElement e);
};

/*!
 * \brief Une AudioNoteFactory permet de créer des audionotes en les ajoutant à la liste des notes gérées par le NotesManager
 * elle herite de MediaNoteFactory
 */
class AudioNoteFactory : public MediaNoteFactory {
public:

    //!
    //! \brief create crée une AudioNote l'ajoute au NotesManager
    //! \param id necessaire à la création de l'AudioNote
    //! \return l'AudioNote créée
    //!
	AudioNote* create(const std::string& id);
	
protected:
	bool isXmlValid(QDomElement e);
	AudioNote* fromXml(QDomElement e);
};

/*!
 * \brief Une VideoNoteFactory permet de créer des videonotes en les ajoutant à la liste des notes gérées par le NotesManager
 * elle herite de MediaNoteFactory
 */
class VideoNoteFactory : public MediaNoteFactory {
public:

    //!
    //! \brief create crée une VideoNote l'ajoute au NotesManager
    //! \param id necessaire à la création de la VideoNote
    //! \return la VideoNote créée
    //!
	VideoNote* create(const std::string& id);
	
protected:
	bool isXmlValid(QDomElement e);
	VideoNote* fromXml(QDomElement e);
};

/*!
 * \brief Une ImageNoteFactory permet de créer des ImageNotes en les ajoutant à la liste des notes gérées par le NotesManager
 * elle herite de MediaNoteFactory
 */
class ImageNoteFactory : public MediaNoteFactory {
public:

    //!
    //! \brief create crée une ImageNote l'ajoute au NotesManager
    //! \param id necessaire à la création de l'ImageNote
    //! \return l'ImageNote créée
    //!
	ImageNote* create(const std::string& id);
	
protected:
	bool isXmlValid(QDomElement e);
	ImageNote* fromXml(QDomElement e);
};





/*
                          __  . .* ,
                        ~#@#%(" .,$ @
                        ."^ ';"
                       ..
                      ;. :                                   . .
                      ;==:                     ,,   ,.@#(&*.;'
                      ;. :                   .;#$% & ^^&
                      ;==:                   &  ......
                      ;. :                   ,,;      :
                      ;==:  ._______.       ;  ;      :
                      ;. :  ;    ###:__.    ;  ;      :
_____________________.'  `._;       :  :__.' .'        `.__________________

                       ~ ~ VOUS QUITTEZ L'USINE DE NOTES ~ ~
*/


#endif // NOTE_FACTORY_H

