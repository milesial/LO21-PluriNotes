#ifndef RESTOREARCHIVECOMMAND_H
#define RESTOREARCHIVECOMMAND_H

#include <QUndoCommand>
/*!
 * \brief La classe RestoreArchiveCommand gère la commande de restauration d'une note depuis les archives, et son annulation.
 */
class RestoreArchiveCommand : public QUndoCommand
{
private:
	std::string id;
public:
	/*!
	 * \brief Construit une commande qui se charge de restaurer une Note archivée, et la passer en Note active.
	 * \param id L'identifiant de la Note à restaurer.
	 * \param parent La commande mère, si il y en a une.
	 */
	RestoreArchiveCommand(const std::string& id, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Restore archive "+id), parent), id(id) {}
	/*!
	 * \brief Passe la Note en Note archivée.
	 */
	void undo();
	/*!
	 * \brief Restaure la Note et la passe en Note active.
	 */
	void redo();
};

#endif // RESTOREARCHIVECOMMAND_H
