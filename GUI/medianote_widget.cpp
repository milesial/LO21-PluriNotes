#include <QFileDialog>

#include "medianote_widget.h"
#include "ui_medianote_widget.h"

#include "note.h"

MediaNoteWidget::MediaNoteWidget(MediaNote *mn, QWidget *parent) :
    NoteWidget(mn, parent),
    ui(new Ui::MediaNoteWidget),
    playing(false)
{
	ui->setupUi(NoteWidget::getContainer());
	
	setupFields(mn);

	connect(ui->filenameButton, SIGNAL(clicked(bool)), this, SLOT(chooseFile()));
	connect(ui->descriptionEdit, SIGNAL(textChanged()), this, SLOT(setMediaNoteDescription()));
	
}
MediaNoteWidget::MediaNoteWidget(const MediaNote *mn, QWidget *parent) :
    NoteWidget(mn, parent),
    ui(new Ui::MediaNoteWidget),
    playing(false)
{
	ui->setupUi(NoteWidget::getContainer());
	
	setupFields(mn);

	ui->filenameButton->setDisabled(true);
	ui->descriptionEdit->setReadOnly(true);
}

void MediaNoteWidget::setupFields(const MediaNote *mn) {
	ui->descriptionEdit->setText(QString::fromStdString(mn->getDescription()));
	ui->filename->setText(QString::fromStdString(mn->getFilename()));
}

MediaNoteWidget::~MediaNoteWidget()
{
	delete ui;
}

QWidget* MediaNoteWidget::getContainer() {
	return ui->cont;
}

void MediaNoteWidget::setMediaNoteDescription() {
	static_cast<MediaNote*>(note)->setDescription(ui->descriptionEdit->toPlainText().toStdString());
	emit noteModified(ui->descriptionEdit->toPlainText());
}

void MediaNoteWidget::chooseFile() {
	QFileDialog dialog(this);
	if(nameFilter != "")
		dialog.setNameFilter(nameFilter);
	dialog.setFileMode(QFileDialog::ExistingFile);
	if(dialog.exec()) {
		QString s = dialog.selectedFiles().at(0);
		ui->filename->setText(s);
		static_cast<MediaNote*>(note)->setFilename(s.toStdString());
		onFileChange(s);
	}
	emit noteModified("");
}

