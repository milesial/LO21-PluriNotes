#ifndef TASKLIST_WIDGET_H
#define TASKLIST_WIDGET_H

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class TaskListWidget;
}
/*!
 * \brief La classe TaskListWidget permet d'afficher de façon claire les Task actives du NotesManager.
 * La liste peut trier les Task selon leur priorité, deadline ou titre.
 * Elle permet aussi de filtrer les notes en fonction de leur état.
 * Ces états sont représentés par un code couleur.
 */
class TaskListWidget : public QWidget
{
	Q_OBJECT
	
public:
	/*!
	 * \brief Construit le widget affichant les task du NotesManager.
	 * \param parent
	 */
	explicit TaskListWidget(QWidget *parent = 0);
	~TaskListWidget();
private:
	Ui::TaskListWidget *ui;
signals:
	//! Signal émis losqu'une tâche est cliquée, passe l'ID de la tâche en paramètre
	void taskClicked(QString id);
private slots:
	void itemPressed(QTreeWidgetItem* i);
	
public slots:
	//! Rafraîchit la liste en récupérant de nouveau toutes les taches actives du NotesManager
	void update();
};

#endif // TASKLIST_WIDGET_H
