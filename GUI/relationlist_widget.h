#ifndef RELATIONLIST_WIDGET_H
#define RELATIONLIST_WIDGET_H
#include "notes_manager.h"
#include <QWidget>
#include <QtGui>
#include <QTreeWidgetItem>

namespace Ui {
class RelationListWidget;
}
/*!
 * \brief La classe RelationListWidget permet d'afficher sour forme d'arbre les descendants et les ascendants de la note active.
 * \details Si un clic est effectué sur une note de l'arbre, la note s'active.
 * Les relations sont sur fond cyan dans l'arbre.
 * L'arbre s'arrete lorsqu'il detecte un cycle ou s'il n'y a plus de descendant/acsendant.
 */
class RelationListWidget : public QWidget
{
    Q_OBJECT

public:

    /*!
     * \brief Ajoute une racine nommé name à notre arbre.
     * \param name
     * \return Le noeud créé
     */
    QTreeWidgetItem* addRoot(std::string name);

    /*!
     * \brief Ajoute un fils a la racine parent .
     * \param parent La racine local ou va être ajouté le fils.
     * \param id Le string qui sera ajouté.
     * \return Le Noeud créé
     */
    QTreeWidgetItem* addChild(QTreeWidgetItem *parent, std::string id);

    /*!
     * \brief Construit le widget permettant de visualiser les acsendants/descendants d'une note.
     * \param parent
     */
    explicit RelationListWidget(QWidget *parent = 0);
    ~RelationListWidget();

    //!
    //! \brief getTree Accesseur de la classe
    //! \return QtreeWidget de notre classe
    //!
    QTreeWidget* getTree() const;
	/*!
	 * \brief Supprime tous les éléments de l'arbre
	 */
	void clear() const;
private:
    Ui::RelationListWidget *ui;
    void affichDesc(std::vector<std::string> pere, QTreeWidgetItem *ici, std::string aAffich, Relation* rel);
    void affichAcs(std::vector<std::string> pere, QTreeWidgetItem* ici, std::string aAffich, Relation* rel);
public slots:

    /*!
     * \brief Permet de raffraichir l'arborescence et réaffichant tous les ascendants/descendants de la note dont l'id est passé en argument.
     * \param id L'id de la note dont on affiche les acsendants/descendants.
     */
    void update(QString id);
signals:

    //! Signal émis losqu'une note des relation est cliquée.
    //! \param id L'ID passé a travers le signal
    void noteClicked(QString id);
private slots:
    void itemPressed(QTreeWidgetItem* i);
};

#endif // RELATIONLIST_WIDGET_H
