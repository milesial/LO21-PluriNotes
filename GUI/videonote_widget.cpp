#include "videonote_widget.h"
#include "ui_videonote_widget.h"

#include "note.h"

VideoNoteWidget::VideoNoteWidget(VideoNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::VideoNoteWidget)
{
	ui->setupUi(getContainer());
	
	setupFields(n);
}

VideoNoteWidget::VideoNoteWidget(const VideoNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::VideoNoteWidget)
{
	ui->setupUi(getContainer());
	
	setupFields(n);
}


VideoNoteWidget::~VideoNoteWidget()
{
	delete ui;
}

void VideoNoteWidget::resizeEvent(QResizeEvent *) {
	videoWidget->setGeometry(getContainer()->rect());
}

void VideoNoteWidget::showEvent(QShowEvent *) {
	videoWidget->setGeometry(getContainer()->rect());
}

void VideoNoteWidget::setupFields(const VideoNote *n) {
	
	nameFilter = "Videos (*.avi *.mov *.mpa *.mp4 *.mkv)";
	
	player = new QMediaPlayer();
	videoWidget = new QVideoWidget(ui->cont);
	
	player->setVideoOutput(videoWidget);
	player->setMedia(QUrl::fromLocalFile(QString::fromStdString(n->getFilename())));
	videoWidget->setAspectRatioMode(Qt::AspectRatioMode::KeepAspectRatio);
	
	videoWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		
	
	connect(ui->timeSlider, SIGNAL(sliderMoved(int)), this, SLOT(setTime(int)));
//	connect(player, SIGNAL(positionChanged(qint32)), this, SLOT(setSlider(qint32)));
	connect(ui->playButton, SIGNAL(clicked(bool)), this, SLOT(playStop()));
	
	videoWidget->show();
	player->play();
	player->pause();
	
}

void VideoNoteWidget::setSlider(qint32 i) {
	ui->timeSlider->setValue((int)(i*1000/player->duration()));
}

void VideoNoteWidget::setTime(int i) {
	player->setPosition(i*player->duration() / 1000);
}

void VideoNoteWidget::playStop() {
	if(playing)
		stop();
	else
		play();
	
}

void VideoNoteWidget::play() {
	playing = true;
	player->play();
	ui->playButton->setText("Pause");
}

void VideoNoteWidget::stop() {
	playing = false;
	player->pause();
	ui->playButton->setText("Play");
}


void VideoNoteWidget::onFileChange(QString s) {
	stop();
	ui->timeSlider->setValue(0);
	player->setMedia(QUrl::fromLocalFile(s));
}
