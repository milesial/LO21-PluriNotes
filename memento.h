#ifndef MEMENTO_H
#define MEMENTO_H
#include "note.h"
#include "notes_exception.h"

/*!
 * \brief La classe Memento permet de sauvegarder l'état d'une note à un instant donné.
 * \details Pour ce faire, a chaque sauvegarde de l'utilisateur, un objet Memento est créé.
 * On stocke, dans cet objet, un pointeur sur la version de la note sauvegardé en clonant la note telle qu'elle est au moment de la sauvegarde.
 */
class Memento
{
    const Note* state; //!<La version de la note
public:

    /*!
     * \brief Construit l'objet memento en clonant la note passée en argument.
     * \param s le pointeur sur la note à sauvegarder.
     */
    Memento(const Note* s): state(s->clone()) {}

    /*!
     * \brief Detruit lobjet memento en détruisant juste le pointeur.
     */
	~Memento() { delete state; }

    //!
    //! \brief getState Accesseur de classe
    //! \return la version stocké dans notre memento
    //!
    const Note* getState() const {return state;}
};

/*!
 * \brief La classe VersionNote regroupe l'ensemble des états précédents d'une note.
 * Elle stocke donc une liste de memento et l'id de la note dont il est question.
 */
class VersionNote{
    const std::string id;
    std::vector<Memento*> version;
public:

    /*!
     * \brief Construit un VersionNote.
     * \param id L'id dont la VersionNote va stocker les états précédents.
     */
    VersionNote(const std::string& id): id(id) {}

    /*!
     * \brief Detruit un VersionNote en rendant la memoire allouée aux pointeurs sur memento.
     */
	~VersionNote();

    /*!
     * \brief Ajoute un memento à un VersionNote en alouant de la memoire pour son pointeur.
     * Si l'id du memento ne correspond pas a celui du VersionNote alors envoie une Exception.
     * \param m Le pointeur sur le memento à ajouter.
     * \param force Si faux, n'ajoute pas la version si sa date de modification correspond à celle de la dernière version.
     */
    void addVersion(Memento* m, bool force=false);

    /*!
     * \brief Enleve un memento à un VersionNote.
     * Si l'id du memento ne correspond pas a celui du VersionNote alors envoie une Exception.
     * \param m Le pointeur sur le memento à enlever.
     */
    void removeVersion(Memento* m);

    //!
    //! \brief getId Accesseur
    //! \return l'id de la note dont les mementos sont les états antérieurs.
    //!
    const std::string getId() const {return id;}

    //!
    //! \brief getList Accesseur.
    //! \return une référence sur le tableau ou sont contenus tou nos pointeurs sur memento .
    //!
    const std::vector<Memento*>& getList() const {return version;}
	
    //!
    //! \brief getLast Accesseur
    //! \return la dernière version de la liste.
    //!
	Memento* getLast() const;

    //!
    //! \brief toXml crée un copie de notre VersionNote en .xml
    //! \return l'element xml créé
    //!
	QDomDocumentFragment toXml() const;

    //!
    //! \brief getSize Accesseur
    //! \return le nombre de versions sauvegardées.
    //!
	int getSize() const { return version.size();}
};

#endif // MEMENTO_H
