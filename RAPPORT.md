# Application PluriNotes

+ MOLLE Quentin
+ MILESI Alexandre


![screen appli](images/appli.png)


L'application PluriNotes est une application développée en C++ avec le framwork Qt. Elle permet de gérer des notes et leur versions, des relations entre ces notes.



# Architecture

## UML (simplifié)
![UML](images/UML_minimal.png)

## Introduction

L'architecture de l'application a été conçues pour respecter au maximum les points suivant :

+ pouvoir ajouter des fonctionnalités sans changer le code existant
+ séparer l'interface graphique de l'application logique
+ utiliser les fonctionnalités de Qt que lorsque c'est nécessaire

Le premier point sera étudié en détail dans la deuxième partie. Pour respecter le deuxième point, l'interface graphique de chaque type de note n'est pas intégré dans la classe elle-même, mais des classes héritant de `QWidget` sont chargées d'afficher le type de note associé. La fenêtre principale est séparées du singleton central. Concernant le troisième point, cela permet d'avoir un code plus réutilisable dans d'autres situations, où Qt n'est pas forcément utilisé. Par exemple, lorsqu'une chaîne de caractères n'a pas besoin d'être utilisé par des objets Qt, un utilise un objet `string` de la STL au lieu d'un `QString`. Ce dernier point pose un problème cependant : l'abondance des conversions entre `QString` et `std::string`, au prix de la réutilisabilité. Il n'est pas adapté par exemple d'utiliser un `QString` dans la classe `Note`, qui n'a rien a voir avec `Qt` et qui peut être utilisée autre part facilement.

Etudions maintenant les principaux points d'architecture de l'application.


## Types de notes

Chaque type de note (article, tache, ...) est représenté par une classe héritant de la classe générale `Note`. Cette classe regroupe donc les attributs qui définissent la note : identifiant, titre, date de création, date de modification, ainsi que les accesseurs correspondants. De plus, la classe contient un attribut `typeName`, qui permet à l'interface graphique d'afficher le texte correspondant au type de note (ex: "Article"). Un note contient une méthode `toString`, et une méthode `toXml`, qui permettent d'obtenir la description de la note en texte ou en XML. La méthode `getWidget` permet d'obtenir un widget affichant la note (en mode lecture seuelement si la note est constante). Pour finir, la méthode protégée `clone` permet d'obtenir un nouveau pointeur sur une note identique, et la méthode protégée `copy` permet d'obtenir un nouveau pointeur sur une note identique mais avec un nouvel identifiant. La classe `Note` est amie avec `NotesManager` et `Memento`, qui doivent utiliser la méthode `clone`. De plus, chaque type de note est ami avec la *factory* associée.

### UML :

![UML](images/notes.png)


## Création de notes

Chaque note créée doit avoir un identifiant unique. Toutes les notes créées doivent être ajoutées au `NotesManager`, et celui-ci doit gérer la destruction des notes. C'est pourquoi le constructeur et le destructeur de `Note` sont privés. Pour construire des notes, le design pattern *Abstract Factory* est utilisé. A chaque type de note (`Article`, `Task`, ...) correspond une *factory* (`ArticleFactory`, `TaskFactory`,..), dérivant de la classe abstraite `NoteFactory`. La méthode `create` d'une factory permet de créer la note du bon type, et de l'ajouter au `NotesManager` (celui-ci vérifie si l'identifiant est déjà utilisé ou non).
Une *factory* possède aussi des méthodes permettant de convertir un élément XML en note. Ces méthodes sont protégées, car elles sont uniquement utiles au `NotesManager` (qui est donc un ami des *factories*) lors du chargement d'un fichier XML.



Ce design pattern est pratique à utiliser, car la méthode `create` renvoie directement un pointeur vers le type de note désiré, que l'utilisateur peut modifier directement sans être obligé d'effectuer un `static_cast`. Ce pattern simplifie aussi énormément l'ajout de nouvelles fonctionnalités, comme on le verra plus tard.


De plus, la classe de base `NoteFactory` est abstraite, il est donc impossible de créer des notes *de base*.

### Exemple d'utilisation :
```C++
ImageNote *note = ImageNoteFactory().create("id");
```


### UML :


![UML](images/factories.png)


## Affichage de notes

Pour afficher les notes, chaque note est associée à une classe dérivée de `NoteWidget`, qui est un `QWidget`. Un widget de notes s'occupe donc d'afficher le type de note associé. Il génèrent aussi les signaux nécéssaires. Un `NoteWidget` peut afficher une note normale, ou alors une note constante. Dans ce dernier cas, les champs sont non-modifiables. Si la note est modifiable, à chaque fois qu'un champ est modifié, la note associée est modifiée. La méthode virtuelle `getWidget` des notes permet de récupérer la bonne spécialisation de `NoteWidget`.

### Utilisation :
```C++
Article *art = ArticleFactory().create("id");

ArticleWidget *w = art->getWidget();
w->show(); //champs modifiables

const Article *artC = art;
artC->getWidget()->show; //mode lecture seulement

```


### UML :


![UML](images/notewidgets.png)

## Exceptions

Pour gérer les différentes exceptions, la classe `NoteException` dérive de l'exception de la blibliothèque standard (STL). Des exceptions plus scpécialisés permettent de faciliter les cas d'exceptions les plus communs, comme `IdTakenException`, `BadXmlException`, `IdNotFoundException`. Ces exceptions s'utilisent exctement comme les exceptions standard : la méthode `what` permet d'obtenir le message.


### UML :


![UML](images/exceptions.png)

## Gestion des relations

Une `Relation` est simplement une liste de `Couple`, décrite par un titre et une description. Un booléen permet de choisir si la relation est orientée ou non. Un couple est constitué de l'identifiant d'une note X, de celui d'une note Y et d'un label décrivant le couple. La classe relation permet d'effectuer les actions de bases, comme ajouter ou supprimer un couple, ou modifier les attributs. De plus, une méthode `getOccurences` retourne une liste de tous les couples dans lesquels une certaine note apparaît. `removeOccurences` supprime tout ces couples de la relation. 

Les méthodes statiques `fromXml` de `Relation` et `Couple` permettent de charger respectivement une relation et un couple depuis un élément XML. A l'inverse, `toXml` enregistre la relation ou le couple dans un objet XML.

La classe `RelationsManager` gère une liste de relations, ainsi que la relation préexistente des références. Elle permet aussi de charger un élément XML ou de sauvegarder les relations en XML.

### Utilisation :

```C++
Relation *rel = new Relation("relation 1");
rel->addCouple(Couple(note1, note2));

relationManager->addRelation(rel);
```


### UML :

![UML](images/relations.png)

## Gestion des versions

A chaque fois qu'une note est sauvegardée, une nouvelle version de cette note est créée. Le design pattern *Memento* est utilisé pour gérer ces versions. Ce pattern est composé de deux classes. Un `Memento` est un objet qui encapsule l'état d'une note à un moment donné. Son constructeur effectue une copie de la note passée en paramètre via la méthode `clone` et stocke cette copie. La classe `VersionNote` est le *caretaker* du design pattern : elle gère une liste de `Memento` pour une note donnée. Elle possède un champ permettant d'obtenir facilement l'identifiant des notes stockées. Il est possible d'obtenir la liste des états précédents, ou bien la dernière version ajoutée. Une version peut être ajoutée (en vérifiant si une version similaire existe déjà, ou non) ou supprimée. On peut aussi sauvegarder l'ensemble des versions en XML.


### Utilisation :


```C++
versionNote->addVersion(new Memento(note));
```


### UML :

![UML](images/memento.png)





## Fonctions annuler et rétablir

La classe `QUndoCommand` est utilisée pour gérer l'annulation de commandes (Ctrl+Z) et la restauration (Ctrl+Y). Pour chaque commande annulable, une classe dérivée est créée. Les méthodes `undo` et `redo` de la commande sont implémentées pour appeler les méthodes du `NotesManager` correspondantes. 
Les commandes annulables sont :

+ Ajout d'un couple à une relation (`AddCoupleCommand`)
+ Suppression d'un couple d'une relation (`RemoveCoupleCommand`)
+ Ajout d'une nouvelle relation (`AddRelationCommand`)
+ Suppression d'une relation (`RemoveRelationCommand`)
+ Envoi d'une note à la corbeille (`DeleteNoteCommand`)
+ Restauration d'une note depuis la corbeille (`RestoreFromBinCommand`)
+ Envoi d'une note vers les archives (`ArchiveNoteCommand`)
+ Restauration d'une note depuis les archives (`RestoreArchiveCommand`)
+ Restauration d'une version d'une note (`RestoreVersionCommand`)
+ Suppression d'une version d'une note (`DeleteVersionCommand`)


Pour gérer ces commandes, la `NotesWindow` possède un attribut de type `QUndoStack` qui gère la pile des commandes à annuler ou restaurer. 


### UML :


![UML](images/commandes.png)

## Manager de notes

Le noyau de l'application repose sur un *Singleton* : `NotesManager`. Il gère les notes actives, archivée et celles dans la corbeille, les relations et les références via un objet `RelationsManager`, ainsi que toutes les versions des notes. Il se charge d'enregistrer l'ensemble du projet dans un fichier en format XML, et à l'inverse de charger un projet depuis un fichier XML. Il permet de sauvegarder l'état d'une ou de toutes les notes et de restaurer un état d'une version. Il sert à envoyer une note active vers la corbeille ou les archives et inversement.


La classe interne `LastVersionIterator` permet d'itérer à travers la dernière version de chaque note active, archivée ou dans la corbeille (méthode `beginActive`, `endActive`, ...).

![UML](images/notesmanager.png)


## Fenêtre principale

La classe `NotesWindow` fournit une interface graphique au `NotesManager`. Elle est composée en haut à gauche d'une liste des notes actives, archivées et dans la corbeille et d'une liste des tâches actives en bas à gauche. A droite, on peut trouver la liste des versions de la note ouverte et la liste des descendants et ascendants dans les différentes relations (cette partie est rétractable). Au centre se trouve les onglets avec les différentes notes ouvertes.

## Fenêtre des relations

La classe `RelationsManagerWidget` est la vue secondaire qui permet de gérer les relations : ajout et suppression de couples et de relations.s

## Ajout des références

Un couple (x,y) doit être ajouté aux références si le texte `\ref{idy}` (avec `idy` l'identifiant d'une note active ou archivée) apparaît dans un champ quelconque d'une note. Pour effectuer cela, le `NotesManager` cherche, à chaque fois qu'une note est sauvegardée, dans la chaîne de caractères retournée par `toString` de la note, le pattern correspondant grâce à aux fonctions d'expressions régulières de Qt, et met à jour les descendants de cette note dans les références. Si l'utilisateur décide d'implémenter un nouveau type de note, il suffit qu'il implémente correctement la fonction `toString` et la recherche de références se fera sans souçis.


## Suppression de notes

Lorqu'une note active est supprimée et qu'elle n'est référencé par aucune autre note, elle en envoyée dans la corbeille. Sinon, elle est archivée. Les couples dans les relations et les références ne sont pas supprimé quand une note est dans la corbeille ou archivée. Cependant, il est impossible de créer de nouvelle relations ou références impliuant une note dans la corbeille (possible pour les notes archivée). Lorqu'une note est supprimée pour de bon, depuis la corbeille ou les archives, tous les couples mentionant la note sont supprimés dans les relations, de même pour les références. Une note archivée étant référencée par une autre note ne peut pas être supprimée définitivement.

## Supression des archives à la suppression de la dernière référence

Lorque la dernière référence vers une note archivée est supprimée, l'application propose de supprimer définitivement cette note. La vérification se fait à chaque fois qu'une note est sauvegardée ou supprimée : l'application cherche la liste des archives n'étant pas référencées avant la modification/supression, puis recherche la même liste après la modification/suppression. On propose alors à l'utilisateur de supprimer toutes les notes qui sont dans la deuxième liste et pas dans la première.

## Sauvegarde du contexte

Le contexte de l'application représente deux variables : le chemin du projet ouvert et le booléen qui détermine si la corbeille doit être vidée automatiquement à la sortie de l'application. A la destruction de l'objet `NotesWindow`, ces deux variables sont enregistrée en format XML dans le fichier `context.xml` dans le même répertoire que l'exécutable de l'application. A la création d'une `NotesWindow`, le fichier est lu et les variables `workspace` et `deleteBinOnExit` sont initialisées.




# Evolution

Comme dit plus haut, l'architecture a été conçue pour pouvoir faciliter au maximum l'ajout de fonctionnalités.

## Ajout de types de notes

Il est relativement facile d'ajouter un type de note et de faire en sorte qu'il soit totalement intégré dans l'application. La première étape est évidemment de créer une classe dérivant de `Note`, et d'implémenter toutes les méthodes nécéssaires. Il faut ensuite construire une *factory* dérivant de `NoteFactory`, qui construit cette note et l'ajoute au `NotesManager` via la méthode `addNewNote`. La note est ainsi prise en charge automatiquement par le manager et les relations, car seuls des pointeurs vers les notes sont utilisés. Pour enregistrer la note en XML, aucun problème ne se pose si la méthode a bien été implémenté. Cependant, au moment du chargement des notes, le manager ne sait pas avec quel *factory* construire les notes qu'il rencontre dans le fichier XML. Il parcourt donc une liste de *factories*, vérifiant à chaque fois si la note XML est validée par une certaine *factory*. Si oui, la note est construite avec la méthode `fromXml` de la bonne *factory*. De base, la liste de *factory* ne contient que celles déjà implémentées. Pour en ajouter une nouvelle, il faut passer par la fonction `addFactory`. La *factory* a besoin d'être ajoutée une seule fois, au début du programme. Cette factory, et le label associé, permet aussi d'ajouter automatiquement ce type de note à la liste des propositions lors de la création d'une nouvelle note. Pour finir, pour afficher correctement cette nouvelle note, il faut implémenter un widget dérivant de `NoteWidget` qui se charge d'afficher cette note (widget qui est retourné par la méthode `getWidget` de la note). 



Résumons les étapes :
+ Ajout de la classe fille de `Note`
+ Ajout de la factory fille de `NoteFactory`
+ Ajout du widget fils de `NoteWidget` (seulement si on compte afficher la note)
+ Au début du programme, ajouter la factory au NotesManager (seulement si on compte charger un fichier XML ou créer une note via l'interface graphique)

## Spécialisation de la fenêtre

La classe `NotesWindow` est facilement spécialisable, l'utilisateur peut donc ajouter des éléments graphiques et des boutons qui exécutent de nouvelles fonctionnalités. Les commandes sont aussi extensibles : elles dérivent de `QUndoCommand` et peuvent donc être ajoutées au `QUndoStack`.

## Spécialisation de relations et de couples

Les couples et relations sont de même spécialisables. Ils seront pris en charge de le même manière par le manager.

## Spécialisation des widgets

La classe `NotesWindow` possède une méthode `getContainer` qui retourne le widget central vide. Il est donc facile d'ajouter un nouvel élément d'interface graphique.

## Spécialisation en général

En général, l'utilisateur est libre de spécialiser les classes qu'il veut, car l'ensemble de l'application utilise des pointeurs sur les objets.


# Informations supplémentaires

## Instructions de compilation

Avant de compiler, s'assurer que les packages suivants sont bien installés :
+ `qt5-default`
+ `qtmultimedia5-dev`

Pour compiler, simplement exécuter :
+ `qmake`
+ `make`

## Liens utiles

Documentation : https://milesial.gitlab.utc.fr/LO21-PluriNotes/index.html