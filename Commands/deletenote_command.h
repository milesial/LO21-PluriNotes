#ifndef DELETENOTECOMMAND_H
#define DELETENOTECOMMAND_H

#include <QUndoCommand>
/*!
 * \brief La classe DeleteNoteCommand gère la commande d'envoie d'une note active vers la corbeille, et son annulation.
 */
class DeleteNoteCommand : public QUndoCommand
{
private:
	std::string id;
public:
	/*!
	 * \brief Construit une commande qui se charge d'envoyer une Note active vers la corbeille.
	 * \param id L'identifiant de la Note active à envoyer à la corbeille.
	 * \param parent La commande mère, si il y en a une.
	 */
	DeleteNoteCommand(const std::string& id, QUndoCommand *parent = 0)
	    : QUndoCommand(QString::fromStdString("Delete note : "+id), parent), id(id) {}
	/*!
	 * \brief Sort la Note de la corbeille et la passe en Note active.
	 */
	void undo();
	/*!
	 * \brief Envoie la Note dans la corbeille.
	 */
	void redo();
};

#endif // DELETENOTECOMMAND_H
