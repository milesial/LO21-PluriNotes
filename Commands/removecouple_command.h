#ifndef REMOVECOUPLECOMMAND_H
#define REMOVECOUPLECOMMAND_H

#include "relation.h"
#include <QUndoCommand>
/*!
 * \brief La classe RemoveCoupleCommand gère la commande de suppression d'un couple d'une relation, et son annulation.
 */
class RemoveCoupleCommand : public QUndoCommand
{
public:
private:
	Relation *r;
	Couple c;
public:
	/*!
	 * \brief Construit une commande qui se charge de supprimer un Couple d'une Relation.
	 * \param r La relation dans laquelle se trouve le Couple à supprimer.
	 * \param c Le couple à supprimer.
	 * \param parent La commande mère, si il y en a une.
	 */
	RemoveCoupleCommand(Relation *r, Couple c, QUndoCommand *parent = 0)
		: QUndoCommand(QString::fromStdString("Remove couple from relation "+r->getTitle()), parent), r(r), c(c) {}
	/*!
	 * \brief Ajoute le Couple à la Relation.
	 */
	void undo();
	/*!
	 * \brief Supprime le Couple de la Relation.
	 */
	void redo();
};

#endif // REMOVECOUPLECOMMAND_H
