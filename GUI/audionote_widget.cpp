#include "audionote_widget.h"
#include "ui_audionote_widget.h"

#include "note.h"

AudioNoteWidget::AudioNoteWidget(AudioNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::AudioNoteWidget)
{
	ui->setupUi(getContainer());
	
	setupFields(n);
}


AudioNoteWidget::AudioNoteWidget(const AudioNote *n, QWidget *parent) :
    MediaNoteWidget(n, parent),
    ui(new Ui::AudioNoteWidget)
{
	ui->setupUi(getContainer());
	
	setupFields(n);
}


AudioNoteWidget::~AudioNoteWidget()
{
	delete ui;
}

void AudioNoteWidget::setupFields(const AudioNote *n) {
	
	nameFilter = "Audio (*.wav *.mp3)";
	
	player = new QMediaPlayer();
	
	player->setMedia(QUrl::fromLocalFile(QString::fromStdString(n->getFilename())));
	
	connect(ui->timeSlider, SIGNAL(sliderMoved(int)), this, SLOT(setTime(int)));
//	connect(player, SIGNAL(positionChanged(qint32)), this, SLOT(setSlider(qint32)));
	connect(ui->playButton, SIGNAL(clicked(bool)), this, SLOT(playStop()));

	player->play();
	player->pause();
	
}


void AudioNoteWidget::playStop() {
	if(playing)
		stop();
	else
		play();
}

void AudioNoteWidget::play() {
	playing = true;
	player->play();
	ui->playButton->setText("Pause");
}

void AudioNoteWidget::stop() {
	playing = false;
	player->pause();
	ui->playButton->setText("Play");
}


void AudioNoteWidget::onFileChange(QString s) {
	stop();
	ui->timeSlider->setValue(0);
	player->setMedia(QUrl::fromLocalFile(s));
}

void AudioNoteWidget::setSlider(qint32 i) {
	ui->timeSlider->setValue((int)(i*1000/player->duration()));
}

void AudioNoteWidget::setTime(int i) {
	player->setPosition(i*player->duration() / 1000);
}
