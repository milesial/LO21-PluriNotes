#ifndef ARTICLE_WIDGET_H
#define ARTICLE_WIDGET_H

#include "note_widget.h"

namespace Ui {
class ArticleWidget;
}

class Article;
/*!
 * \brief La classe ArticleWidget est un QWidget qui affiche le contenu d'une Task.
 */
class ArticleWidget : public NoteWidget {
	Q_OBJECT
private slots:
	void boldSelection();
	void underlineSelection();
	void italicSelection();
	void strikeSelection();
	void setFontSizeSelection(int size);
	void alignRight();
	void alignCenter();
	void alignLeft();
	void setArticleText();

protected:
	void setupFields(const Article* article);
	
public:
	/*!
	 * \brief Construit un ArticleWidget qui affiche un Article avec tous les champs modifiables.
	 *  La modification de ces champs entraine la modification de l'Article associé.
	 * \param article L'article à afficher et à associer.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit ArticleWidget(Article *article, QWidget *parent = 0);
	/*!
	 * \brief Construit un ArticleWidget qui affiche un Article en mode lecture seule.
	 * \param article L'article à afficher.
	 * \param parent Le QWidget parent, si il existe.
	 */
	explicit ArticleWidget(const Article *article, QWidget *parent = 0);
	~ArticleWidget();

private:
	Ui::ArticleWidget *ui;

};

#endif // ARTICLE_WIDGET_H
