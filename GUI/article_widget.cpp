#include <QToolButton>

#include "article_widget.h"
#include "ui_article_widget.h"

#include "note.h"


void setSquare(QWidget *w, int size=-1) {
	if(size == -1)
		size = w->height();
	w->setMaximumWidth (size);
	w->setMinimumWidth (size);
	w->setMaximumHeight(size);
	w->setMinimumHeight(size);
}

ArticleWidget::ArticleWidget(Article *article, QWidget *parent) :
	NoteWidget(article, parent),
	ui(new Ui::ArticleWidget) {

	ui->setupUi(getContainer());

	setupFields(article);
	
	//connections
	connect(ui->boldButton, SIGNAL(clicked(bool)), this, SLOT(boldSelection()));
	connect(ui->underlineButton, SIGNAL(clicked(bool)), this, SLOT(underlineSelection()));
	connect(ui->italicButton, SIGNAL(clicked(bool)), this, SLOT(italicSelection()));
	connect(ui->strikeButton, SIGNAL(clicked(bool)), this, SLOT(strikeSelection()));
	connect(ui->fontSpin, SIGNAL(valueChanged(int)), this, SLOT(setFontSizeSelection(int)));
	connect(ui->alignRightButton, SIGNAL(clicked(bool)), this, SLOT(alignRight()));
	connect(ui->alignCenterButton, SIGNAL(clicked(bool)), this, SLOT(alignCenter()));
	connect(ui->alignLeftButton, SIGNAL(clicked(bool)), this, SLOT(alignLeft()));
	
	connect(ui->textEdit, SIGNAL(textChanged()), this, SLOT(setArticleText()));
	
}


ArticleWidget::ArticleWidget(const Article *article, QWidget *parent) :
	NoteWidget(article, parent),
	ui(new Ui::ArticleWidget) {

	ui->setupUi(getContainer());
	
	setupFields(article);
	
	ui->textEdit->setReadOnly(true);
	QList<QToolButton*> l = this->findChildren<QToolButton*>();
	foreach (QToolButton *bu, l) {
		bu->setDisabled(true);
	}
	ui->fontSpin->setDisabled(true);
	ui->label->setDisabled(true);

}

void ArticleWidget::setupFields(const Article *article) {
	ui->textEdit->setHtml(QString::fromStdString(article->getText()));

	//tools buttons square
	setSquare(ui->italicButton);
	setSquare(ui->boldButton);
	setSquare(ui->strikeButton);
	setSquare(ui->underlineButton);
	setSquare(ui->alignCenterButton);
	setSquare(ui->alignLeftButton);
	setSquare(ui->alignRightButton);
}

ArticleWidget::~ArticleWidget()
{
	delete ui;
}

void ArticleWidget::setArticleText() {
	static_cast<Article*>(note)->setText(ui->textEdit->toHtml().toStdString());
	emit noteModified(ui->textEdit->toPlainText());
}


void ArticleWidget::boldSelection() {
	QTextCharFormat format;
	QTextCursor curs = ui->textEdit->textCursor();

	if(!curs.charFormat().font().bold())
		format.setFontWeight(QFont::Bold);
	else
		format.setFontWeight(QFont::Normal);

	curs.mergeCharFormat(format);
}

void ArticleWidget::underlineSelection() {
	QTextCharFormat format;
	QTextCursor curs = ui->textEdit->textCursor();

	if(!curs.charFormat().font().underline())
		format.setUnderlineStyle(QTextCharFormat::SingleUnderline);
	else
		format.setUnderlineStyle(QTextCharFormat::NoUnderline);

	curs.mergeCharFormat(format);
}


void ArticleWidget::italicSelection() {
	QTextCharFormat format;
	QTextCursor curs = ui->textEdit->textCursor();

	format.setFontItalic(!curs.charFormat().font().italic());

	curs.mergeCharFormat(format);
}

void ArticleWidget::strikeSelection() {
	QTextCharFormat format;
	QTextCursor curs = ui->textEdit->textCursor();

	format.setFontStrikeOut(!curs.charFormat().font().strikeOut());

	curs.mergeCharFormat(format);

}

void ArticleWidget::setFontSizeSelection(int size) {
	QTextCharFormat format;
	QTextCursor curs = ui->textEdit->textCursor();

	format.setFontPointSize(size);

	curs.mergeCharFormat(format);


	ui->textEdit->setFontPointSize(size);
}

void ArticleWidget::alignRight() {
	QTextBlockFormat b = ui->textEdit->textCursor().blockFormat();
	b.setAlignment(Qt::AlignRight);
	ui->textEdit->textCursor().mergeBlockFormat(b);
}

void ArticleWidget::alignCenter() {
	QTextBlockFormat b = ui->textEdit->textCursor().blockFormat();
	b.setAlignment(Qt::AlignCenter);
	ui->textEdit->textCursor().mergeBlockFormat(b);
}

void ArticleWidget::alignLeft() {
	QTextBlockFormat b = ui->textEdit->textCursor().blockFormat();
	b.setAlignment(Qt::AlignLeft);
	ui->textEdit->textCursor().mergeBlockFormat(b);
}

