#include "removecouple_command.h"
#include "notes_exception.h"

void RemoveCoupleCommand::undo() {
	try{
		r->addCouple(c);
	} catch(NotesException& e) {
		
	}
}

void RemoveCoupleCommand::redo() {
	try{
		r->removeCouple(c);
	} catch(NotesException& e) {
		
	}
}
